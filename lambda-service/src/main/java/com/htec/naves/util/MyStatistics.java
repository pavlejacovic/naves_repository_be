package com.htec.naves.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.naves.dto.StatisticsForFunctionDTO;
import com.htec.naves.entity.StatisticsEntity;
import com.htec.naves.exception.InvalidDateExcpetion;
import com.htec.naves.repository.FunctionExecutionRepository;

/**
 * Class that contains method that returns statistics based on query params
 * 
 * @author Pavle Jacovic
 *
 */
@Service
public class MyStatistics {

	@Autowired
	private FunctionExecutionRepository functionExecutionRepository;

	public List<StatisticsEntity> getAllStatisticsForSubscribedUser(Long userId) {

		List<Object[]> statisticsForUserObject = functionExecutionRepository
				.findDailyStatisticsForSubscribedUser(userId);

		List<StatisticsEntity> statisticsForUser = new ArrayList<>();

		for (Object[] objects : statisticsForUserObject) {
			StatisticsEntity statisticsFromObject = new StatisticsEntity();
			statisticsFromObject.setDate(objects[0].toString());
			statisticsFromObject.setFunctionName(objects[1].toString());
			statisticsFromObject.setAverageExecutionDuration(Double.parseDouble(objects[2].toString()));
			statisticsFromObject.setMaxExecutionDuration(Integer.parseInt(objects[3].toString()));
			statisticsFromObject.setMinExecutionDuration(Integer.parseInt(objects[4].toString()));
			statisticsFromObject.setNumberOfExecutions(Integer.parseInt(objects[5].toString()));
			statisticsFromObject.setNumberOfExecutionsByOtherUsers(Integer.parseInt(objects[6].toString()));

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date today = new Date();
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(today);
			calendar.add(Calendar.DAY_OF_MONTH, -7);
			Date daysAgo = calendar.getTime();

			try {
				Date executionDate = simpleDateFormat.parse(statisticsFromObject.getDate());
				if (executionDate.after(daysAgo)) {
					statisticsForUser.add(statisticsFromObject);
				}
			} catch (ParseException e) {
				throw new InvalidDateExcpetion("Could not load statistics for users");
			}

		}

		return statisticsForUser;
	}

	public List<StatisticsForFunctionDTO> getFunctionStatisticsForUser(Long functionId) {

		List<Object[]> statisticsForFunctionObjects = functionExecutionRepository.findStatisticsForFunction(functionId);

		List<StatisticsForFunctionDTO> statisticsForFunction = new ArrayList<>();

		for (Object[] objects : statisticsForFunctionObjects) {
			StatisticsForFunctionDTO statisticsForFunctionDTO = new StatisticsForFunctionDTO();
			statisticsForFunctionDTO.setDate(objects[0].toString());
			statisticsForFunctionDTO.setFunctionName(objects[1].toString());
			statisticsForFunctionDTO.setNumberOfExecutions(Integer.parseInt(objects[2].toString()));

			statisticsForFunction.add(statisticsForFunctionDTO);

		}

		return statisticsForFunction;
	}

}
