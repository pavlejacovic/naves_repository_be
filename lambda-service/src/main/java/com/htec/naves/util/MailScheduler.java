package com.htec.naves.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.htec.naves.service.EmailSenderService;

/**
 * Class that sends mail each time Scheduled function triggers. Function
 * triggers every Sunday at 9 a.m. cron=0 0 9 * * SUN
 * 
 * For testing purposes use cron= 1 * * * * ? . This triggers function each
 * minute
 * 
 * @author Pavle Jacovic
 *
 */
@Component
public class MailScheduler {

	@Autowired
	private EmailSenderService emailSenderService;

	@Scheduled(cron = "0 0 9 * * SUN")
	public void sendMail() {
		emailSenderService.sendStatistics();
	}
}
