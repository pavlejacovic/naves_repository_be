package com.htec.naves.util;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.htec.naves.exception.CommandLineException;

@Service
public class CommandLineExecutor {

	/**
	 * executes command in windows terminal using set of arguments
	 * 
	 * @param args  set of arguments
	 * @param email currently logged user
	 */
	public void executeCommandLine(List<String> args, String email) {
		try {
			Process dockerCommand;
			ProcessBuilder builder = new ProcessBuilder(args.toArray(new String[args.size()]));
			builder.redirectErrorStream(true);
			builder.redirectError(ProcessBuilder.Redirect.INHERIT);
			dockerCommand = builder.start();
			dockerCommand.waitFor();

		} catch (IOException | InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new CommandLineException(email, "Error in command execution", e);
		}
	}

}
