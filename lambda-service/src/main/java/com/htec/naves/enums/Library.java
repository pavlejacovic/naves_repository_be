package com.htec.naves.enums;

import lombok.Getter;

/**
 * 
 * Names of all supported languages
 * 
 * @author Uros
 *
 */
@Getter
public enum Library {

	JAVA("navesLib.jar"),CS("NavesLib.1.0.0.nupkg");

	private String libraryName;

	Library(String libraryName) {
		this.libraryName = libraryName;
	}
}
