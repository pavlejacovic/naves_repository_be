package com.htec.naves.enums;

public enum TypeOfFile {

	JAR("jar"), ZIP("zip");

	private final String executableFileValue;

	TypeOfFile(String string) {
		this.executableFileValue = string;
	}

	@Override
	public String toString() {
		return executableFileValue.toLowerCase();
	}

}
