package com.htec.naves.enums;

public enum TypesOfExecutables {

	JAR("jar"), CS("cs"), PY("py");

	private final String executableFileValue;

	TypesOfExecutables(String string) {
		this.executableFileValue = string;
	}

	@Override
	public String toString() {
		return executableFileValue.toLowerCase();
	}

}
