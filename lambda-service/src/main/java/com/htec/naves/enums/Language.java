package com.htec.naves.enums;

import lombok.Getter;

/**
 * 
 * Names of all supported languages
 * 
 * @author Uros
 * @author Ognjen
 * 
 */
@Getter
public enum Language {

	JAVA("java"), CSHARP("csharp");

	private String libraryName;

	Language(String libraryName) {
		this.libraryName = libraryName;
	}
}