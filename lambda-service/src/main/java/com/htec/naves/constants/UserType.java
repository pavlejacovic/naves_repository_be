package com.htec.naves.constants;

public class UserType {

	private UserType() {

	}

	public static final Long ADMIN = Long.valueOf(2);
	public static final Long CLIENT = Long.valueOf(1);
}
