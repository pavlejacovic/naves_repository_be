package com.htec.naves.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommandLineException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final String user;

	public CommandLineException(String user, String message) {
		super(message);
		this.user = user;
	}

	public CommandLineException(String user, String message, Throwable cause) {
		super(message, cause);
		this.user = user;
	}

}
