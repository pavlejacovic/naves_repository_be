package com.htec.naves.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Thrown when an IOException related to the unzipping file occurs.
 * 
 * @author Ognjen Pejcic
 *
 */
@Getter
@Setter
public class UnzippingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final String user;

	public UnzippingException(String user, String message) {
		super(message);
		this.user = user;
	}

	public UnzippingException(String user, String message, Throwable cause) {
		super(message, cause);
		this.user = user;
	}
}
