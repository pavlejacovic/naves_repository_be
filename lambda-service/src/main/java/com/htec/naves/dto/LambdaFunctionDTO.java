package com.htec.naves.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import com.htec.naves.enums.Language;
import com.htec.naves.enums.Trigger;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LambdaFunctionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long functionId;

	@NotNull(message = "Function name should not be null")
	@Size(max = 50, message = "Maximum function name allowed is 50 characters")
	@Pattern(regexp = "^[a-z0-9-]+$", message = "Function name can only contain lowercase characters(a-z), numbers(0-9) and dash(-)")
	private String functionName;

	private String functionLink;

	@NotNull(message = "Public access should not be null")
	private Boolean publicAccess;

	@NotNull(message = "Trigger should not be null")
	private Trigger trigger;

	@NotNull(message = "Executable file should not be null")
	private MultipartFile archiveFile;

	private RegisteredUserDTO owner;

	@NotNull(message = "You must select a language")
	private Language language;

	private List<Long> projectIds = new ArrayList<>();

	private String prefix;
	private String suffix;

}
