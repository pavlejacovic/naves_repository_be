package com.htec.naves.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private RegisteredUserDTO owner;
	private String projectName;
}
