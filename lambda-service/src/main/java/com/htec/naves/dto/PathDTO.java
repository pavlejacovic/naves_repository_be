package com.htec.naves.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PathDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotBlank
	@NotNull(message = "Path should not be blank")
	private String path;
}
