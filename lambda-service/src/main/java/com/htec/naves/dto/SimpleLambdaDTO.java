package com.htec.naves.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.htec.naves.enums.Trigger;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimpleLambdaDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long functionId;
	private String functionName;
	private String functionLink;
	private Boolean publicAccess;
	private Trigger trigger;
	private RegisteredUserDTO owner;
	private List<ProjectDTO> projects = new ArrayList<>();

}
