package com.htec.naves.dto;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class InputParametersDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String inputParameters;
	private List<File> inputFiles;

}
