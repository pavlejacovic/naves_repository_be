package com.htec.naves.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import com.htec.naves.enums.Language;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LambdaFunctionExecutionUpdateDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long functionId;

	@NotNull(message = "Executable file should not be null")
	private MultipartFile archiveFile;

	private RegisteredUserDTO owner;

	@NotNull(message = "You must select a language")
	private Language language;

}
