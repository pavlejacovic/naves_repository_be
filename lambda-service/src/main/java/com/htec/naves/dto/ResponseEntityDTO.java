package com.htec.naves.dto;

import org.springframework.util.MultiValueMap;

import lombok.Data;

@Data
public class ResponseEntityDTO {

	private int status;
	private MultiValueMap<String, String> headers;
	private Object body;
}
