package com.htec.naves.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsForFunctionDTO {

	String date;
	String functionName;
	int numberOfExecutions;
}
