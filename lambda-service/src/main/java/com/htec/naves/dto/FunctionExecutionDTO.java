package com.htec.naves.dto;

import com.htec.naves.entity.LambdaFunctionEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FunctionExecutionDTO {

	private Long executionId;
	private LambdaFunctionEntity functionId;
	private String executionTimestamp;
	private Long executionDuration;
	private Boolean source;

}
