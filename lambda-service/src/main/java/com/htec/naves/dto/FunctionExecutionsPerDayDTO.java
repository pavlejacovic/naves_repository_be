package com.htec.naves.dto;

import com.htec.naves.entity.LambdaFunctionEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FunctionExecutionsPerDayDTO {

	private Long executionPerDayId;
	private LambdaFunctionEntity functionId;
	private String date;
	private int numberOfExecutions;
}
