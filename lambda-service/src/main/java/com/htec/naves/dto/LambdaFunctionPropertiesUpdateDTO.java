package com.htec.naves.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.htec.naves.enums.Trigger;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LambdaFunctionPropertiesUpdateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long functionId;

	@NotNull(message = "Function name should not be null")
	@Size(max = 50, message = "Maximum function name allowed is 50 characters")
	@Pattern(regexp = "^[a-z0-9-]+$", message = "Function name can only contain lowercase characters(a-z), numbers(0-9) and dash(-)")
	private String functionName;

	@NotNull(message = "Public access should not be null")
	private Boolean publicAccess;

	@NotNull(message = "Trigger should not be null")
	private Trigger trigger;

	private RegisteredUserDTO owner;

	private List<Long> projectIds = new ArrayList<>();

	private String successMessage;

	private String prefix;
	private String suffix;

}
