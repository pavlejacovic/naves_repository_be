package com.htec.naves.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.htec.naves.repository.LambdaFunctionRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Service containing methods for admin.
 * 
 * @author Jovan Popovic
 * @author Mia Pecelj
 * 
 *
 */
@Service
@Transactional
@Slf4j
@Configuration
public class AdminService {
	@Autowired
	private LambdaFunctionRepository lambdaRepository;
	@Autowired
	private AdminPermissionService adminPermissionUtil;

	/**
	 * Returns number of all functions that are in db.
	 * 
	 * @param token token containing user email
	 * @return
	 */
	public Long getNumberOfFunctions(String token) {
		adminPermissionUtil.checkAdminPermission(token);
		return lambdaRepository.count();
	}

}
