package com.htec.naves.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import com.htec.naves.exception.FileSystemException;

@Service
@EnableScheduling
public class ContainerCleanupService {

	@Value("${lambda.path}")
	private String lambdaPath;

	@Scheduled(fixedDelay = 60000)
	public void deleteFilesScheduledTask() throws IOException {
		findFiles(lambdaPath);
	}

	public void findFiles(String filePath) throws IOException {

		try (Stream<Path> pathStream = Files.list(Paths.get(filePath))) {
			List<File> files = pathStream.map(Path::toFile).collect(Collectors.toList());
			for (File file : files) {
				if (isFileOld(file)) {
					deleteFile(file);
				}
			}
		} catch (IOException e) {
			throw new FileSystemException("Error");
		}

	}

	public void deleteFile(File file) {
		FileSystemUtils.deleteRecursively(file);
	}

	public boolean isFileOld(File file) {
		LocalDateTime fileDate = Instant.ofEpochMilli(file.lastModified()).atZone(ZoneId.systemDefault())
				.toLocalDateTime();
		LocalDateTime oldDate = LocalDateTime.now().minusMinutes(10);
		return fileDate.isBefore(oldDate);
	}
}
