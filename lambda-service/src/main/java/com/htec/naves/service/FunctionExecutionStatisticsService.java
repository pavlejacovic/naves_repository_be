package com.htec.naves.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.htec.naves.dto.StatisticsForFunctionDTO;
import com.htec.naves.entity.FunctionExecutionEntity;
import com.htec.naves.entity.LambdaFunctionEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.exception.CommandLineException;
import com.htec.naves.exception.InvalidDateExcpetion;
import com.htec.naves.exception.PermissionException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.repository.FunctionExecutionRepository;
import com.htec.naves.repository.LambdaFunctionRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.JwtUtil;
import com.htec.naves.util.MyStatistics;

/**
 * Class that contains methods for previewing statistics of naves function
 * execution
 * 
 * @author Pavle Jacovic
 */
@Service
@Transactional
public class FunctionExecutionStatisticsService {

	@Autowired
	private JwtUtil jwtUtil;
	@Autowired
	private RegisteredUserRepository registeredUserRepository;
	@Autowired
	private FunctionExecutionRepository statisticsRepository;
	@Autowired
	private LambdaFunctionRepository lambdaFunctionRepository;
	@Autowired
	private MyStatistics myStatistics;

	/**
	 * Returns all executions for desired function in the last X days
	 * 
	 * @param token
	 * @param functionId
	 * @return
	 */
	public List<StatisticsForFunctionDTO> getStatisticsForFunction(String token, Long functionId, int days) {
		String email = jwtUtil.extractUsername(token);
		RegisteredUserEntity existingUser = getExistingUser(email);
		Optional<LambdaFunctionEntity> lambdaFunctionEntity = lambdaFunctionRepository.findByFunctionId(functionId);
		List<StatisticsForFunctionDTO> allExecutionsOfMyFunctionInTheLastXDays = new ArrayList<>();

		if (lambdaFunctionEntity.isEmpty()) {
			throw new ResourceNotFoundException(email, "Function does not exist");
		}

		if (lambdaFunctionEntity.get().getOwner().getUserId().equals(existingUser.getUserId())) {
			throw new PermissionException(email, "You do not have permission for this operation");
		}

		Date today = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(today);
		calendar.add(Calendar.DAY_OF_MONTH, -days);
		Date daysAgo = calendar.getTime();

		List<StatisticsForFunctionDTO> statistics = myStatistics.getFunctionStatisticsForUser(functionId);

		for (StatisticsForFunctionDTO statisticsEntity : statistics) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date executionDate = simpleDateFormat.parse(statisticsEntity.getDate());
				if (executionDate.after(daysAgo)) {
					allExecutionsOfMyFunctionInTheLastXDays.add(statisticsEntity);
				}
			} catch (ParseException e) {
				throw new InvalidDateExcpetion("Could not load statistics for the last " + days + " days");
			}
		}

		return allExecutionsOfMyFunctionInTheLastXDays;

	}

	public void calculateAndSetFunctionExecutionInformation(String containerName, String functionName, String email) {

		LambdaFunctionEntity lambdaFunctionEntity = getLambdaFunctionByName(functionName);
		boolean executionSource = false;
		if (email.equals(lambdaFunctionEntity.getOwner().getEmail())) {
			executionSource = true;
		}
		List<String> args = new ArrayList<>();
		args.add(0, containerName);
		args.add(0, "--format={{.State.StartedAt}}");
		args.add(0, "inspect");
		args.add(0, "docker");
		String startTimeStamp = executeCommandLineAndReturnResponse(args, email);
		args.clear();
		args.add(0, containerName);
		args.add(0, "--format={{.State.FinishedAt}}");
		args.add(0, "inspect");
		args.add(0, "docker");
		String endTimeStamp = executeCommandLineAndReturnResponse(args, email);

		Instant timeStart = Instant.parse(startTimeStamp);
		Instant timeEnd = Instant.parse(endTimeStamp);

		Date dateStart = Date.from(timeStart);
		Date dateEnd = Date.from(timeEnd);

		Long executionDuration = dateEnd.getTime() - dateStart.getTime();
		FunctionExecutionEntity functionExecution = new FunctionExecutionEntity();
		functionExecution.setLambdaFunctionEntity(lambdaFunctionEntity);
		functionExecution.setExecutionTimestamp(startTimeStamp);
		functionExecution.setExecutionDuration(executionDuration);
		functionExecution.setSource(executionSource);

		statisticsRepository.save(functionExecution);
	}

	/**
	 * executes command in windows terminal using set of arguments and return one
	 * line as a response
	 * 
	 * @param args  set of arguments
	 * @param email currently logged user
	 */
	private String executeCommandLineAndReturnResponse(List<String> args, String email) {
		try {
			Process dockerCommand;
			ProcessBuilder builder = new ProcessBuilder(args.toArray(new String[args.size()]));
			builder.redirectErrorStream(true);
			builder.redirectError(ProcessBuilder.Redirect.INHERIT);
			dockerCommand = builder.start();
			dockerCommand.waitFor();

			String result = "";
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(dockerCommand.getInputStream()))) {
				String line = reader.readLine();
				while (line != null) {
					result = line;
					line = reader.readLine();
				}
			}
			return result;

		} catch (IOException | InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new CommandLineException(email, "Error while updating function", e);
		}
	}

	private RegisteredUserEntity getExistingUser(String email) {
		Optional<RegisteredUserEntity> existingUserOptional = registeredUserRepository.findByEmail(email);
		if (!existingUserOptional.isPresent()) {
			throw new ResourceNotFoundException("User does not exist");
		}
		return existingUserOptional.get();
	}

	private LambdaFunctionEntity getLambdaFunctionByName(String name) {
		Optional<LambdaFunctionEntity> lambdaFunctionEntityOptional = lambdaFunctionRepository.findByFunctionName(name);
		if (!lambdaFunctionEntityOptional.isPresent()) {
			throw new ResourceNotFoundException("Function does not exist");
		}
		return lambdaFunctionEntityOptional.get();
	}

}
