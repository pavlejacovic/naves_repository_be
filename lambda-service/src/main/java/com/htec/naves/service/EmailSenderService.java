package com.htec.naves.service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.google.common.io.Files;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.entity.StatisticsEntity;
import com.htec.naves.exception.EmailSendingException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.MyStatistics;

/**
 * Service that is responsible for sending statistics regarding function
 * executions to users
 * 
 * @author Pavle Jacovic
 *
 */
@Service
public class EmailSenderService {

	@Autowired
	private RegisteredUserRepository registeredUserRepository;
	@Autowired
	private JavaMailSender javaMailSender;
	@Autowired
	private MyStatistics myStatistics;

	private static final String HTMLCONST1 = "<td>";
	private static final String HTMLCONST2 = "</td>\r\n";

	public void sendStatistics() {
		List<RegisteredUserEntity> userEntities = registeredUserRepository.findBySubscribed(true);
		for (RegisteredUserEntity registeredUserEntity : userEntities) {
			sendEmailWithStatistics(registeredUserEntity);
		}
	}

	private void sendEmailWithStatistics(RegisteredUserEntity registeredUserEntity) {

		List<StatisticsEntity> statisticsData = myStatistics
				.getAllStatisticsForSubscribedUser(registeredUserEntity.getUserId());

		if (!statisticsData.isEmpty()) {

			try {
				MimeMessage message = javaMailSender.createMimeMessage();

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message);
				mimeMessageHelper.setFrom("navesteam26@gmail.com", "Naves");

				mimeMessageHelper.setTo(registeredUserEntity.getEmail());

				String subject = "Your weekly statistics";

				String htmlString = "";
				StringBuilder dataString = new StringBuilder();

				htmlString = loadHTMLFile();

				for (StatisticsEntity statistics : statisticsData) {
					dataString.append("<tr bgcolor=\"#ffffff\" align=\"left\"\r\n"
							+ "style=\"padding: 20px 30px 40px 30px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: 400; line-height: 25px;\">\r\n"
							+ HTMLCONST1
							+ statistics.getFunctionName().substring(0, statistics.getFunctionName().lastIndexOf('-'))
							+ HTMLCONST2 + HTMLCONST1 + statistics.getDate() + HTMLCONST2 + HTMLCONST1
							+ statistics.getNumberOfExecutions() + HTMLCONST2 + HTMLCONST1
							+ statistics.getNumberOfExecutionsByOtherUsers() + HTMLCONST2 + HTMLCONST1
							+ statistics.getMinExecutionDuration() + HTMLCONST2 + HTMLCONST1
							+ statistics.getMaxExecutionDuration() + HTMLCONST2 + "</tr>");
				}

				htmlString = htmlString.replace("$content", dataString);

				mimeMessageHelper.setSubject(subject);
				mimeMessageHelper.setText(htmlString, true);
				javaMailSender.send(message);
			} catch (MessagingException | UnsupportedEncodingException e) {
				throw new EmailSendingException("Error in sending email with statistics");
			}
		}
	}

	private String loadHTMLFile() {
		try {
			return Files.asCharSource(new File("." + File.separator + "src" + File.separator + "main" + File.separator
					+ "resources" + File.separator + "functionStatistics.html"), StandardCharsets.UTF_8).read();
		} catch (IOException e) {
			throw new ResourceNotFoundException("Statistics HTML does not exist");
		}
	}

}
