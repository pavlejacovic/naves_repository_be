package com.htec.naves.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.MyUser;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private RegisteredUserRepository registeredUserRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		Optional<RegisteredUserEntity> user = registeredUserRepository.findByEmail(email);

		if (user.isEmpty()) {
			throw new UsernameNotFoundException("User " + email + " does not exist");
		}
		return new MyUser(new User(user.get().getEmail(), "***", new ArrayList<>()), 1);

	}

}