package com.htec.naves.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.enums.Library;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.InvalidArgumentException;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class LibraryService {

	@Value("${library.path}")
	private String libraryPath;

	/**
	 * Creates directories where user libraries will be placed
	 */
	@PostConstruct
	public void init() {
		try {
			Files.createDirectories(Paths.get(libraryPath));
			// Create directory for each supported language
			for (Library language : Library.values()) {
				Files.createDirectories(Paths.get(libraryPath + File.separator + language.name()));
			}
		} catch (IOException e) {
			log.warn("Could not create a folder that stores libraries!");
			throw new FileSystemException("Could not create a folder that stores libraries!");
		}
	}

	/**
	 * Downloads library for specific programming language
	 * 
	 * @param email    email of user that requested this action
	 * 
	 * @param language programming language for which library is being downloaded
	 *
	 * @return requested library as a resource
	 * 
	 * @throws FileSystemException      when encountering a problem with file system
	 *                                  or if requested library not present
	 * @throws InvalidArgumentException when user specified language that is not
	 *                                  supported
	 */
	public Resource downloadLibrary(String email, Library language) {
		try {
			Path path = Paths
					.get(libraryPath + File.separator + language.name() + File.separator + language.getLibraryName())
					.normalize();
			Resource resource = new UrlResource(path.toUri());
			if (!resource.exists() && !resource.isReadable()) {
				throw new FileSystemException(email, "Library for language " + language + " does not exist");
			}
			return resource;
		} catch (IOException e) {
			log.info("Error with downloading library for " + language + " language. \n" + e.getMessage());
			throw new FileSystemException(email, "Error with downloading library for " + language + " language", e);
		}
	}
}
