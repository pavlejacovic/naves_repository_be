package com.htec.naves.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.htec.naves.dto.LambdaFunctionDTO;
import com.htec.naves.dto.LambdaFunctionExecutionUpdateDTO;
import com.htec.naves.dto.LambdaFunctionPropertiesUpdateDTO;
import com.htec.naves.dto.SimpleLambdaDTO;
import com.htec.naves.entity.LambdaFunctionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.enums.Trigger;
import com.htec.naves.enums.TypeOfFile;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.exception.PermissionException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.mapper.LambdaDTOToSimpleLambdaDTOMapper;
import com.htec.naves.mapper.MyLambdaFunctionDTOMapper;
import com.htec.naves.mapper.UpdatePropertiesEntityDTOMapper;
import com.htec.naves.repository.LambdaFunctionRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.CommandLineExecutor;
import com.htec.naves.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Service for generating docker images and executing docker containers
 * 
 * 
 * @author Pavle Jacovic
 * @author Mia Pecelj
 * @author Jovan Popovic
 * @author Ognjen Pejcic
 * @author Uros Stokovic
 */
@Service
@Transactional
@Slf4j
public class LambdaService {

	@Value("${lambda.path}")
	private String lambdaPath;

	@Value("${lambda.link}")
	private String lambdaLink;

	@Autowired
	private CommandLineExecutor commandLineExecutor;

	public static final String VALID_SUFFIX = "\\.[a-zA-Z0-9]+";

	public static final String DOCKER = "docker";
	public static final String IMAGE = "image";
	public static final String DOCKERFILE = "Dockerfile";
	public static final String ZIP = "zip";
	public static final String JAR = "jar";

	public static final String FUNCTION_CREATION_ERROR = "Error while creating function";
	public static final String FUNCTION_ALREADY_EXISTS = "Function already exists";
	public static final String INVALID_TYPE_OF_FILE_PROVIDED = "Invalid type of file for requested language";
	public static final String USER_DOES_NOT_EXIS = "User does not exist";
	public static final String FUNCTION_DOES_NOT_EXIST = "Function does not exist";
	public static final String FUNCTION_UPDATING_ERROR = "Error while updating function";
	public static final String PROPERTIES_SUCCESSFULLY_UPDATED = "Successfully updated function's properties";
	public static final String NO_PERMISSION = "You do not have permission for this operation";
	public static final String WRONG_SUFFIX_FORMAT = "Suffix does not have correct format";

	@Autowired
	private JwtUtil jwtUtil;
	@Autowired
	private RegisteredUserRepository registeredUserRepository;
	@Autowired
	private LambdaFunctionRepository lambdaFunctionRepository;
	@Autowired
	private MyLambdaFunctionDTOMapper lambdaFunctionDTOMapper;
	@Autowired
	private UpdatePropertiesEntityDTOMapper updatePropertiesDtoMapper;
	@Autowired
	private UnzippingService unzippingService;
	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private LambdaDTOToSimpleLambdaDTOMapper lambdaSimpleMapper;

	@PostConstruct
	public void init() throws IOException {
		Files.createDirectories(Paths.get(lambdaPath));
	}

	public String getLambdaLink() {
		return lambdaLink;
	}

	public void setLambdaLink(String lambdaLink) {
		this.lambdaLink = lambdaLink;
	}

	/**
	 * This function makes docker image from user provided executable file and
	 * Dockerfile with custom parameters for java functions
	 * 
	 * @param lambdaFunctionDTO data transfer object that contains lambda function
	 *                          properties such as archiveFile, name, trigger,
	 *                          accessibility(public or private)
	 * @param imageName         name of the image based on the project name
	 * @param token             user's token from the authorization header
	 */
	public void makeJavaFunction(LambdaFunctionDTO lambdaFunctionDTO, String token) {

		String email = jwtUtil.extractUsername(token);

		if (!checkFileExtension(TypeOfFile.JAR.toString(), lambdaFunctionDTO.getArchiveFile())) {
			throw new InvalidArgumentException(email, INVALID_TYPE_OF_FILE_PROVIDED);
		}

		if (lambdaFunctionDTO.getTrigger() != Trigger.GET && lambdaFunctionDTO.getTrigger() != Trigger.POST) {
			lambdaFunctionDTO.setSuffix(checkSuffix(email, lambdaFunctionDTO.getSuffix()));
			lambdaFunctionDTO.setPrefix(checkPrefix(lambdaFunctionDTO.getPrefix()));
		} else {
			lambdaFunctionDTO.setPrefix("");
			lambdaFunctionDTO.setSuffix("");
		}

		String imageNameWithUser = generateImageName(email, lambdaFunctionDTO.getFunctionName());
		lambdaFunctionDTO.setFunctionName(imageNameWithUser);
		String lambdaFunctionDestinationPath = prepareLambdaStorageForJavaFunctions(lambdaFunctionDTO.getArchiveFile(),
				email, imageNameWithUser, false);

		createImageInDocker(imageNameWithUser, lambdaFunctionDestinationPath, email);

		insertLambdaFunctionInDatabase(lambdaFunctionDTO, email);
	}

	/**
	 * Checks if the owner of the project and lambda are the same person when adding
	 * trigger to project.
	 * 
	 * @param lambdaFunctionDTO Contains information about lambda function
	 * @param email             email of the function owner
	 * @return
	 */
	private LambdaFunctionDTO checkOwner(LambdaFunctionDTO lambdaFunctionDTO, String email) {

		List<Long> toRemove = new ArrayList<>();

		for (Long projectId : lambdaFunctionDTO.getProjectIds()) {
			Optional<ProjectEntity> project = projectRepository.findById(projectId);

			if (project.isEmpty() || !email.equals(project.get().getOwner().getEmail())) {
				toRemove.add(projectId);
			}
		}

		lambdaFunctionDTO.getProjectIds().removeAll(toRemove);
		return lambdaFunctionDTO;
	}

	/**
	 * This function makes docker image from user provided zip file and dockerfile
	 * with custom parameters for C# functions
	 * 
	 * @param lambdaFunctionDTO data transfer object that contains lambda function
	 *                          properties such as archiveFile, name, trigger,
	 *                          accessibility(public or private)
	 * @param imageName         name of the image based on the project name
	 * @param token             user's token from the authorization header
	 */
	public void makeImageFromDll(LambdaFunctionDTO lambdaFunctionDTO, String token) {

		String email = jwtUtil.extractUsername(token);

		if (!checkFileExtension(TypeOfFile.ZIP.toString(), lambdaFunctionDTO.getArchiveFile())) {
			throw new InvalidArgumentException(email, INVALID_TYPE_OF_FILE_PROVIDED);
		}

		if (lambdaFunctionDTO.getTrigger() != Trigger.GET && lambdaFunctionDTO.getTrigger() != Trigger.POST) {
			lambdaFunctionDTO.setSuffix(checkSuffix(email, lambdaFunctionDTO.getSuffix()));
			lambdaFunctionDTO.setPrefix(checkPrefix(lambdaFunctionDTO.getPrefix()));
		} else {
			lambdaFunctionDTO.setPrefix("");
			lambdaFunctionDTO.setSuffix("");
		}

		String imageNameWithUser = generateImageName(email, lambdaFunctionDTO.getFunctionName());
		lambdaFunctionDTO.setFunctionName(imageNameWithUser);
		String lambdaFunctionDestinationPath = prepareLambdaStorageForCSharpFunctions(
				lambdaFunctionDTO.getArchiveFile(), email, imageNameWithUser, false);

		createImageInDocker(imageNameWithUser, lambdaFunctionDestinationPath, email);

		insertLambdaFunctionInDatabase(lambdaFunctionDTO, email);
	}

	/**
	 * This function generates docker image
	 * 
	 * @param imageName                     name of image in docker
	 * @param lambdaFunctionDTO             data transfer object that contains
	 *                                      lambda function properties such as
	 *                                      archiveFile, name, trigger,
	 *                                      accessibility(public or private)
	 * @param lambdaFunctionDestinationPath path where files for lambda function
	 *                                      will be temporary stored
	 * @param email                         user's email
	 * 
	 */
	private void createImageInDocker(String imageName, String lambdaFunctionDestinationPath, String email) {

		List<String> args = new ArrayList<>();
		args.add(0, lambdaFunctionDestinationPath);
		args.add(0, imageName);
		args.add(0, "-t");
		args.add(0, "build");
		args.add(0, DOCKER);
		if (Objects.equals(System.getProperty("os.name"), "Linux")) {
			args.add(0, "sudo");
		}
		commandLineExecutor.executeCommandLine(args, email);
		args.clear();

		try {
			FileSystemUtils.deleteRecursively(Paths.get(lambdaFunctionDestinationPath));
		} catch (IOException e) {
			throw new FileSystemException(email, FUNCTION_CREATION_ERROR, e);
		}
	}

	/**
	 * This function saves lambda function into database
	 * 
	 * @param lambdaFunctionDTO data transfer object that contains lambda function
	 *                          properties such as archiveFile, name, trigger,
	 *                          accessibility(public or private)
	 * @param email             user's email
	 * 
	 */
	private void insertLambdaFunctionInDatabase(LambdaFunctionDTO lambdaFunctionDTO, String email) {
		LambdaFunctionEntity lambdaFunctionEntity = lambdaFunctionDTOMapper.toEntity(lambdaFunctionDTO);
		RegisteredUserEntity lambdaFunctionOwner = getExistingUser(email);
		lambdaFunctionEntity.setOwner(lambdaFunctionOwner);
		if (!isTrigger(lambdaFunctionDTO.getTrigger())) {
			lambdaFunctionEntity.setFunctionLink(UUID.randomUUID().toString());
		} else {
			lambdaFunctionEntity.setFunctionLink(null);
		}
		lambdaFunctionRepository.save(lambdaFunctionEntity);
	}

	/**
	 * Gets all lambda functions of logged user
	 * 
	 * @param token user's token from the authorization header
	 * @return list of user's functions
	 */
	public List<SimpleLambdaDTO> getMyFunctions(String token) {

		String email = jwtUtil.extractUsername(token);
		RegisteredUserEntity existingUser = getExistingUser(email);
		return lambdaFunctionRepository.findByOwner(existingUser).stream().map(element -> {
			LambdaFunctionDTO lambdaFuntionDTO = lambdaFunctionDTOMapper.toDTO(element);
			lambdaFuntionDTO.setFunctionName(lambdaFuntionDTO.getFunctionName().substring(0,
					lambdaFuntionDTO.getFunctionName().lastIndexOf('-')));
			lambdaFuntionDTO.setFunctionLink(lambdaFuntionDTO.getFunctionLink() == null ? null
					: lambdaLink + lambdaFuntionDTO.getFunctionLink());
			return lambdaSimpleMapper.toSimpleDto(lambdaFuntionDTO);
		}).collect(Collectors.toList());

	}

	/**
	 * Gets all lambda functions of logged user by page
	 * 
	 * @param token contains information about loggedUser
	 * @param page  contains information about functions per page
	 * @return
	 */
	public List<SimpleLambdaDTO> getMyFunctions(String token, Pageable page) {
		String email = jwtUtil.extractUsername(token);
		RegisteredUserEntity existingUser = getExistingUser(email);

		return lambdaFunctionRepository.findAllByOwner(existingUser, page).stream().map(element -> {
			LambdaFunctionDTO lambdaFuntionDTO = lambdaFunctionDTOMapper.toDTO(element);
			lambdaFuntionDTO.setFunctionName(lambdaFuntionDTO.getFunctionName().substring(0,
					lambdaFuntionDTO.getFunctionName().lastIndexOf('-')));
			lambdaFuntionDTO.setFunctionLink(lambdaFuntionDTO.getFunctionLink() == null ? null
					: lambdaLink + lambdaFuntionDTO.getFunctionLink());
			return lambdaSimpleMapper.toSimpleDto(lambdaFuntionDTO);
		}).collect(Collectors.toList());
	}

	/**
	 * Generates new link for the requested function if the user who requested this
	 * is owner of that function
	 * 
	 * @param token      user's token from the authorization header received during
	 *                   login
	 * @param functionId id of function for which we generate new link
	 * @return Newly generated link
	 * 
	 * @throws ResourceNotFoundException when there is no function with specified id
	 * @throws PermissionException       when user that requested this operation is
	 *                                   not an owner of the function
	 */
	public String generateNewFunctionLink(String token, Long functionId) {
		LambdaFunctionEntity lambdaFunction = getFunctionIfOwned(functionId, token);
		if (isTrigger(lambdaFunction.getTrigger())) {
			throw new InvalidArgumentException(jwtUtil.extractUsername(token), "Trigger function does not have a link");
		}
		lambdaFunction.setFunctionLink(UUID.randomUUID().toString());
		lambdaFunctionRepository.save(lambdaFunction);
		return lambdaLink + lambdaFunction.getFunctionLink();
	}

	/**
	 * Deletes function for the given id
	 * 
	 * @param functionId
	 * @param token
	 */
	public void deleteFunction(long functionId, String token) {

		LambdaFunctionEntity lambda = getFunctionIfOwned(functionId, token);
		String email = jwtUtil.extractUsername(token);

		List<String> args = new ArrayList<>();
		args.add(0, "-f");
		args.add(0, lambda.getFunctionName());
		args.add(0, "rmi");
		args.add(0, IMAGE);
		args.add(0, DOCKER);
		commandLineExecutor.executeCommandLine(args, email);

		lambdaFunctionRepository.delete(lambda);
	}

	/**
	 * Checks if function is owned by user and return it if the function exists
	 * 
	 * @param functionId
	 * @param token
	 * @return
	 */
	private LambdaFunctionEntity getFunctionIfOwned(long functionId, String token) {
		String email = jwtUtil.extractUsername(token);
		RegisteredUserEntity existingUser = getExistingUser(email);
		Optional<LambdaFunctionEntity> lambdaFunction = lambdaFunctionRepository.findById(functionId);

		if (lambdaFunction.isEmpty()) {
			log.info("Lambda function with id: " + functionId + " does not exist. \n");
			throw new ResourceNotFoundException(email, FUNCTION_DOES_NOT_EXIST);
		}
		if (!lambdaFunction.get().getOwner().getUserId().equals(existingUser.getUserId())) {
			log.info("User: " + existingUser.getUserId() + "doesn't have permission for this operation.");
			throw new PermissionException(email, NO_PERMISSION);
		}

		return lambdaFunction.get();

	}

	/**
	 * Creates docker image name
	 * 
	 * @param email
	 * @param functionName
	 * @return
	 */
	private String generateImageName(String email, String functionName) {
		RegisteredUserEntity existingUser = getExistingUser(email);
		return functionName + "-" + existingUser.getUserId();
	}

	/**
	 * system. This function also creates custom docker file based on the template
	 * and user parameters for java functions.
	 * 
	 * @param archiveFile       executable jar file that user has uploaded
	 * @param email             user email
	 * @param imageNameWithUser image name with user id
	 * @return Docker file destination path
	 */
	private String prepareLambdaStorageForJavaFunctions(MultipartFile jarFile, String email, String imageNameWithUser,
			boolean updating) {
		String lambdaFunctionDestinationPath = lambdaPath + File.separator + email + File.separator + imageNameWithUser;
		File sourceFile = new File(DOCKERFILE);

		if (lambdaFunctionRepository.existsByFunctionName(imageNameWithUser) && !updating) {
			throw new ResourceAlreadyExistsException(email, FUNCTION_ALREADY_EXISTS);
		}

		try {
			Files.createDirectories(Paths.get(lambdaFunctionDestinationPath));
		} catch (IOException e1) {
			throw new FileSystemException(email, FUNCTION_CREATION_ERROR, e1);
		}

		try (FileWriter myWriter = new FileWriter(lambdaFunctionDestinationPath + File.separator + DOCKERFILE);) {
			String content = new String(Files.readAllBytes(Paths.get(sourceFile.getPath())));

			content = content.replace("&baseImage", "openjdk:16-jdk-alpine");
			content = content.replace("&secondAction", "ADD");
			content = content.replace("$actionProperties", "$jarFileName $imageName");
			content = content.replace("$entrypoint", "\"java\",\"-jar\",\"$imageName\"");
			content = content.replace("$jarFileName", jarFile.getOriginalFilename());
			content = content.replace("$imageName", imageNameWithUser);

			myWriter.write(content);
			Files.copy(jarFile.getInputStream(),
					Paths.get(lambdaFunctionDestinationPath + File.separator + jarFile.getOriginalFilename()));
			return lambdaFunctionDestinationPath;
		} catch (IOException e) {
			throw new FileSystemException(email, FUNCTION_CREATION_ERROR, e);
		}
	}

	/**
	 * This function creates directory for image inside user file system. This
	 * function also creates custom docker file based on the template and user
	 * parameters for C# functions.
	 * 
	 * @param archiveFile       zip file that user has uploaded which contains all
	 *                          required files for function execution
	 * @param email             user email
	 * @param imageNameWithUser image name with user id
	 * @return Docker file destination path
	 */
	private String prepareLambdaStorageForCSharpFunctions(MultipartFile archiveFile, String email,
			String imageNameWithUser, boolean updating) {
		String lambdaFunctionDestinationPath = lambdaPath + File.separator + email + File.separator + imageNameWithUser;
		File sourceFile = new File(DOCKERFILE);

		if (lambdaFunctionRepository.existsByFunctionName(imageNameWithUser) && !updating) {
			throw new ResourceAlreadyExistsException(email, FUNCTION_ALREADY_EXISTS);
		}
		try {
			Files.createDirectories(Paths.get(lambdaFunctionDestinationPath + File.separator + "zip"));
		} catch (IOException e1) {
			throw new FileSystemException(email, FUNCTION_CREATION_ERROR, e1);
		}

		unzippingService.unzipFile(archiveFile, Paths.get(lambdaFunctionDestinationPath + File.separator + "zip"),
				email);

		String executableFileName = FilenameUtils.removeExtension(archiveFile.getOriginalFilename()) + ".dll";

		try (FileWriter myWriter = new FileWriter(lambdaFunctionDestinationPath + File.separator + DOCKERFILE);) {
			String content = new String(Files.readAllBytes(Paths.get(sourceFile.getPath())));

			content = content.replace("&baseImage", "mcr.microsoft.com/dotnet/runtime:5.0");
			content = content.replace("&secondAction", "COPY");
			content = content.replace("$actionProperties ", "\\zip .");
			content = content.replace("$entrypoint", "\"dotnet\", \"$dllFileName\"");
			content = content.replace("$dllFileName", executableFileName);

			myWriter.write(content);
			Files.copy(archiveFile.getInputStream(),
					Paths.get(lambdaFunctionDestinationPath + File.separator + archiveFile.getOriginalFilename()));
			return lambdaFunctionDestinationPath;
		} catch (IOException e) {
			throw new FileSystemException(email, FUNCTION_CREATION_ERROR, e);
		}
	}

	/**
	 * updateFunctionProperties updates lambda function properties, but not jar
	 * file. It changes information about function in docker, file system and
	 * database
	 * 
	 * @param token      contains information about loggedUser
	 * @param lambdaDto  contains information with which certain lambda function is
	 *                   updated
	 * @param functionId serves as a parameter to find lambda function in database
	 * @return
	 */
	public LambdaFunctionPropertiesUpdateDTO updateFunctionProperties(String token,
			LambdaFunctionPropertiesUpdateDTO lambdaDto, Long functionId) {

		String email = jwtUtil.extractUsername(token);
		RegisteredUserEntity existingUser = getExistingUser(email);
		LambdaFunctionEntity existingLambdaFunction = getFunctionIfOwned(functionId, token);

		boolean exists = lambdaFunctionRepository
				.existsByFunctionName(lambdaDto.getFunctionName() + "-" + existingUser.getUserId());

		if (exists && !(lambdaDto.getFunctionName() + "-" + existingUser.getUserId())
				.equals(existingLambdaFunction.getFunctionName())) {
			log.info("Function with name: " + existingLambdaFunction.getFunctionName() + " already exists.");
			throw new ResourceAlreadyExistsException(email, FUNCTION_ALREADY_EXISTS);
		}

		String imageToBeRemoved = existingLambdaFunction.getFunctionName();
		String newName = "";

		newName = lambdaDto.getFunctionName() + "-" + existingUser.getUserId();
		existingLambdaFunction.setFunctionName(newName);
		existingLambdaFunction.setPublicAccess(lambdaDto.getPublicAccess());
		existingLambdaFunction.setTrigger(lambdaDto.getTrigger());

		if (lambdaDto.getTrigger() != Trigger.GET && lambdaDto.getTrigger() != Trigger.POST) {
			existingLambdaFunction.setSuffix(checkSuffix(email, lambdaDto.getSuffix()));
			existingLambdaFunction.setPrefix(checkPrefix(lambdaDto.getPrefix()));
		} else {
			existingLambdaFunction.setPrefix("");
			existingLambdaFunction.setSuffix("");
		}

		existingLambdaFunction.getProjects().clear();

		LambdaFunctionDTO functionDTO = checkOwner(updatePropertiesDtoMapper.dtoToDto(lambdaDto), email);

		if (existingLambdaFunction.getFunctionLink() == null
				&& (!isTrigger(lambdaDto.getTrigger()))) {
			existingLambdaFunction.setFunctionLink(UUID.randomUUID().toString());
		} else if (isTrigger(lambdaDto.getTrigger())) {
			existingLambdaFunction.setFunctionLink(null);
		}

		if (functionDTO.getTrigger() != Trigger.GET && functionDTO.getTrigger() != Trigger.POST) {
			for (Long projectId : functionDTO.getProjectIds()) {
				ProjectEntity project = new ProjectEntity();
				project.setId(projectId);
				existingLambdaFunction.addProject(project);

			}
		}
		if (!(lambdaDto.getFunctionName() + "-" + existingUser.getUserId()).equals(imageToBeRemoved)) {

			List<String> args = new ArrayList<>();
			args.add(0, newName + ":latest");
			args.add(0, imageToBeRemoved + ":latest");
			args.add(0, "tag");
			args.add(0, IMAGE);
			args.add(0, DOCKER);
			commandLineExecutor.executeCommandLine(args, email);
			args.clear();

			args.add(0, "-f");
			args.add(0, imageToBeRemoved);
			args.add(0, "rmi");
			args.add(0, DOCKER);
			commandLineExecutor.executeCommandLine(args, email);

		}

		lambdaFunctionRepository.save(existingLambdaFunction);

		LambdaFunctionPropertiesUpdateDTO updatedFunctionDTO = updatePropertiesDtoMapper.toDto(existingLambdaFunction);
		updatedFunctionDTO.setSuccessMessage(PROPERTIES_SUCCESSFULLY_UPDATED);

		return updatedFunctionDTO;
	}

	public String checkSuffix(String email, String suffix) {
		if (suffix == null) {
			return "";
		}
		suffix = suffix.trim();
		if (suffix.length() != 0 && !suffix.matches(VALID_SUFFIX)) {
			throw new InvalidArgumentException(email, WRONG_SUFFIX_FORMAT);
		}
		return suffix;
	}

	public String checkPrefix(String prefix) {
		if (prefix == null) {
			return "";
		}
		prefix = prefix.trim();
		prefix = prefix.replace("\\", "/");
		prefix = prefix.replaceAll("/+", "/");
		return Paths.get(prefix).normalize().toString();
	}

	/**
	 * updateFunctionExecution updates lambda function execution file and docker
	 * image
	 * 
	 * @param token        contains information about loggedUser
	 * @param executionDTO contains information about jar with which certain lambda
	 *                     function image is updated
	 * @param functionId   serves as a parameter to find lambda function in database
	 * @return
	 */
	public String updateFunctionExecutionJava(String token, LambdaFunctionExecutionUpdateDTO executionDTO,
			Long functionId) {
		String email = jwtUtil.extractUsername(token);

		LambdaFunctionEntity existingLambdaFunction = getFunctionIfOwned(functionId, token);

		if (!checkFileExtension(TypeOfFile.JAR.toString(), executionDTO.getArchiveFile())) {
			log.info("User: " + email + " provided invalid type of executable file");
			throw new InvalidArgumentException(email, INVALID_TYPE_OF_FILE_PROVIDED);
		}

		String imageNameWithUser = existingLambdaFunction.getFunctionName();
		String lambdaFunctionDestinationPath = prepareLambdaStorageForJavaFunctions(executionDTO.getArchiveFile(),
				email, imageNameWithUser, true);

		createImageInDocker(imageNameWithUser, lambdaFunctionDestinationPath, email);

		List<String> args = new ArrayList<>();
		args.clear();
		args.add(0, "-f");
		args.add(0, "prune");
		args.add(0, IMAGE);
		args.add(0, DOCKER);
		commandLineExecutor.executeCommandLine(args, email);

		return "Successfully updated executable file for function - " + existingLambdaFunction.getFunctionName()
				.substring(0, existingLambdaFunction.getFunctionName().length() - 2);

	}

	public String updateFunctionExecutionDotnet(String token, LambdaFunctionExecutionUpdateDTO executionDTO,
			Long functionId) {
		String email = jwtUtil.extractUsername(token);

		LambdaFunctionEntity existingLambdaFunction = getFunctionIfOwned(functionId, token);

		if (!checkFileExtension(TypeOfFile.ZIP.toString(), executionDTO.getArchiveFile())) {
			throw new InvalidArgumentException(email, INVALID_TYPE_OF_FILE_PROVIDED);
		}

		String imageNameWithUser = existingLambdaFunction.getFunctionName();
		String lambdaFunctionDestinationPath = prepareLambdaStorageForCSharpFunctions(executionDTO.getArchiveFile(),
				email, imageNameWithUser, true);

		createImageInDocker(imageNameWithUser, lambdaFunctionDestinationPath, email);

		List<String> args = new ArrayList<>();
		args.clear();
		args.add(0, "-f");
		args.add(0, "prune");
		args.add(0, IMAGE);
		args.add(0, DOCKER);
		commandLineExecutor.executeCommandLine(args, email);

		return "Successfully updated executable file for function - " + existingLambdaFunction.getFunctionName()
				.substring(0, existingLambdaFunction.getFunctionName().length() - 2);
	}

	/**
	 * check if file has valid extension
	 * 
	 * @param extension      determines type of file
	 * @param executableFile
	 * @return
	 */
	private boolean checkFileExtension(String extension, MultipartFile executableFile) {
		return FilenameUtils.getExtension(executableFile.getOriginalFilename()).equals(extension);
	}

	/**
	 * checks if function is trigger or not
	 * 
	 * @param functionType type of function
	 * @return true if function is trigger and false if not
	 */
	private boolean isTrigger(Trigger functionType) {
		return functionType != Trigger.GET && functionType != Trigger.POST;
	}

	private RegisteredUserEntity getExistingUser(String email) {
		Optional<RegisteredUserEntity> existingUserOptional = registeredUserRepository.findByEmail(email);
		if (!existingUserOptional.isPresent()) {
			throw new ResourceNotFoundException(USER_DOES_NOT_EXIS);
		}
		return existingUserOptional.get();
	}

}
