package com.htec.naves.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.htec.naves.exception.UnzippingException;

@Service
public class UnzippingService {

	/**
	 * This function extracts all files from zip file
	 * 
	 * @param zipFile   archive file that user has uploaded
	 * 
	 * @param imageName path in system where all the files will be extracted
	 * @param token     user email
	 */
	public void unzipFile(MultipartFile zipFile, Path path, String email) {

		try {
			ZipInputStream inputStream = new ZipInputStream(zipFile.getInputStream());
			for (ZipEntry entry; (entry = inputStream.getNextEntry()) != null;) {
				Path resolvedPath = path.resolve(entry.getName());
				if (!entry.isDirectory()) {
					Files.createDirectories(resolvedPath.getParent());
					Files.copy(inputStream, resolvedPath);
				} else {
					Files.createDirectories(resolvedPath);
				}

			}
		} catch (IOException e) {
			throw new UnzippingException(email, "Error while unzipping file.", e);
		}
	}
}
