package com.htec.naves.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.tomcat.util.buf.StringUtils;
import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import com.htec.naves.dto.ResponseEntityDTO;
import com.htec.naves.entity.LambdaFunctionEntity;
import com.htec.naves.enums.Trigger;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.FunctionExecutionException;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.exception.PermissionException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.repository.LambdaFunctionRepository;
import com.htec.naves.util.CommandLineExecutor;
import com.htec.naves.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Service for generating docker images and executing docker containers
 * 
 * 
 * @author Pavle Jacovic
 * @author Mia Pecelj
 * @author Jovan Popovic
 * @author Ognjen Pejcic
 * @author Uros Stokovic
 * @author Ivan Kukrkic
 */
@Service
@Slf4j
public class ExecutionService {

	@Value("${lambda.path}")
	private String lambdaPath;

	@Autowired
	private CommandLineExecutor commandLineExecutor;

	private static final String ERROR = "Error";
	private static final String DOCKER = "docker";
	private static final String INPUT = "Input";
	private static final String OUTPUT = "Output";
	private static final String FUNCTION_EXECUTION_ERROR = "Error while executing function";
	private static final String INVALID_FUNCTION_OUTPUT_FORMAT = "Invalid function output format";
	private static final String INVALID_LINK = "Function link is invalid";
	private static final String NO_PERMISSION = "You don't have permission for this operation";
	private static final String CONTAINER_DIRECTORY_ALREADY_EXISTS = "Container directory already exist";
	private static final String INVALID_ARGUMENT_FORMAT = "Invalid argument format";
	private static final String FUNCTION_EXECUTION_TIMEOUT = "Function execution timeout";

	@Autowired
	private JwtUtil jwtUtil;
	@Autowired
	private LambdaFunctionRepository lambdaFunctionRepository;
	@Autowired
	private FunctionExecutionStatisticsService statisticsService;

	/**
	 * This function makes Input and Output directories for the container from where
	 * he is going to read Input files, and where he is going to write a result of
	 * the execution of a container. This method calls executeImage method which
	 * runs the container and returns the result read from the output directory.
	 * 
	 * @param functionLink generated uuid which represents the link to the function
	 * @param token        user token used for extracting an email
	 * @param contentType  format in which the body parameters are passed
	 * @param parameterMap map of all function parameters
	 * @param fileMap      map of all files passed as function input
	 * @param bodyString   string representation of the json passed as function
	 *                     input
	 * @return ResponseEntityDTO containing response status, headers and body
	 * @throws IOException                    in case there was an error deleting
	 *                                        the temporary directory
	 * @throws PermissionException            in case user doesn't have permission
	 *                                        to execute the function
	 * @throws ResourceNotFoundException      in case function link is invalid
	 * @throws ResourceAlreadyExistsException in case there was a conflict when
	 *                                        creating a container
	 * @throws FileSystemException            in case there was an IOException while
	 *                                        creating temporary directories or
	 *                                        reading from output file
	 * @throws InvalidArgumentException       in case provided arguments are in an
	 *                                        invalid format
	 */
	public ResponseEntityDTO runContainerFromImage(String functionLink, String token, String contentType,
			Map<String, String[]> parameterMap, Map<String, MultipartFile> fileMap, String bodyString, Trigger method) {

		String email = "";
		File outputFolder;
		File inputFolder;
		ResponseEntityDTO response = null;

		if (token != null) {
			email = jwtUtil.extractUsername(token);
		}
		String containerName = UUID.randomUUID().toString();

		Optional<LambdaFunctionEntity> function = lambdaFunctionRepository.findByFunctionLink(functionLink);

		if (function.isEmpty() || !function.get().getTrigger().equals(method)) {
			log.info("Function link is invalid.");
			throw new ResourceNotFoundException(email, INVALID_LINK);
		}

		String functionName = function.get().getFunctionName();
		String owner = function.get().getOwner().getEmail();

		if (Boolean.FALSE.equals(function.get().getPublicAccess()) && !email.equals(owner)) {
			log.info("User: " + owner + "doesn't have permission for this operation");
			throw new PermissionException(email, NO_PERMISSION);
		}

		File containerFolder = new File(getContainerPath(containerName));

		if (Files.exists(Paths.get(containerFolder.getPath()))) {
			log.info("Container directory: " + Paths.get(containerFolder.getPath()) + "already exists");
			throw new ResourceAlreadyExistsException(email, CONTAINER_DIRECTORY_ALREADY_EXISTS);
		}

		try {
			Files.createDirectories(Paths.get(containerFolder.getPath()));

			inputFolder = new File(containerFolder.getPath() + File.separator + INPUT);
			Files.createDirectories(Paths.get(inputFolder.getPath()));

			outputFolder = new File(containerFolder.getPath() + File.separator + OUTPUT);
			Files.createDirectories(Paths.get(outputFolder.getPath()));

			executeImage(functionName, email, contentType, parameterMap, fileMap, bodyString, inputFolder,
					containerFolder, containerName);

			response = prepareResponse(email, containerName);
		} catch (IOException | InterruptedException e) {
			log.info("Error while executing function. \n" + e.getMessage());
			Thread.currentThread().interrupt();
			throw new FileSystemException(email, FUNCTION_EXECUTION_ERROR, e);
		} catch (JSONException | ParseException e) {
			log.info("Invalid argument format \n" + e.getMessage());
			throw new InvalidArgumentException(email, INVALID_ARGUMENT_FORMAT, e);
		}

		return response;
	}

	private void executeImage(String functionName, String mail, String contentType, Map<String, String[]> parameterMap,
			Map<String, MultipartFile> fileMap, String bodyString, File inputFolder, File containerFolder,
			String containerName) throws IOException, JSONException, ParseException, InterruptedException {
		Process dockerCommand;
		ProcessBuilder builder;
		List<String> args = new LinkedList<>();

		if (contentType != null && contentType.contains("multipart/form-data")) {
			makeJSON(parameterMap, fileMap, inputFolder);
		} else {
			makeJSON(parameterMap, bodyString, inputFolder);
		}

		args.add(0, functionName);
		args.add(0, containerFolder.getAbsolutePath() + ":/myfolder");
		args.add(0, "-v");
		args.add(0, containerName);
		args.add(0, "--name");
		args.add(0, "run");
		args.add(0, DOCKER);

		builder = new ProcessBuilder(args.toArray(new String[args.size()]));
		builder.redirectErrorStream(true);
		builder.redirectError(ProcessBuilder.Redirect.INHERIT);

		dockerCommand = builder.start();
		if (!dockerCommand.waitFor(30, TimeUnit.SECONDS)) {
			args.clear();
			args.add(0, containerName);
			args.add(0, "kill");
			args.add(0, "container");
			args.add(0, DOCKER);
			commandLineExecutor.executeCommandLine(args, mail);
			builder.start();

			log.info("Function execution timed out.\n Function name: " + functionName);
			throw new FunctionExecutionException(mail, FUNCTION_EXECUTION_TIMEOUT);
		}

		statisticsService.calculateAndSetFunctionExecutionInformation(containerName, functionName, mail);

		args.clear();
		args.add(0, "-f");
		args.add(0, "prune");
		args.add(0, "container");
		args.add(0, DOCKER);
		commandLineExecutor.executeCommandLine(args, mail);
		builder.start();

		args.clear();
		args.add(0, "-f");
		args.add(0, "prune");
		args.add(0, "image");
		args.add(0, DOCKER);
		commandLineExecutor.executeCommandLine(args, mail);
		builder.start();

	}

	private JSONObject makeJSON(Map<String, String[]> parameterMap, Map<String, MultipartFile> fileMap, File inputPath)
			throws IOException {
		JSONObject input = new JSONObject();
		JSONObject parameters = new JSONObject();
		JSONObject body = new JSONObject();
		for (Entry<String, String[]> entry : parameterMap.entrySet()) {
			parameters.put(entry.getKey(), StringUtils.join(Arrays.asList(entry.getValue()), ' '));
		}

		for (Entry<String, MultipartFile> entry : fileMap.entrySet()) {
			MultipartFile file = entry.getValue();
			body.put(entry.getKey(), file.getOriginalFilename());
			Files.copy(file.getInputStream(),
					Paths.get(inputPath.getAbsolutePath() + File.separator + file.getOriginalFilename()));
		}
		input.put("requestParams", parameters);
		input.put("requestBody", body);

		File file = new File(inputPath.getAbsolutePath() + File.separator + "input.json");
		try (Writer output = new BufferedWriter(new FileWriter(file));) {
			output.write(input.toString());
		} catch (IOException e) {
			throw new FileSystemException(ERROR);
		}
		return input;
	}

	private JSONObject makeJSON(Map<String, String[]> parameterMap, String bodyString, File inputPath)
			throws IOException, JSONException, ParseException {
		JSONObject input = new JSONObject();
		JSONObject parameters = new JSONObject();
		JSONObject body = new JSONObject();
		for (Entry<String, String[]> entry : parameterMap.entrySet()) {
			parameters.put(entry.getKey(), entry.getValue());
		}
		if (bodyString != null) {
			JSONParser parser = new JSONParser(bodyString);
			body = new JSONObject(parser.parseObject());
		}

		input.put("requestParams", parameters);
		input.put("requestBody", body);

		File file = new File(inputPath.getAbsolutePath() + File.separator + "input.json");
		try (Writer output = new BufferedWriter(new FileWriter(file));) {
			output.write(input.toString());
		} catch (IOException e) {
			throw new FileSystemException(ERROR);
		}

		return input;
	}

	private ResponseEntityDTO prepareResponse(String mail, String containerName) {
		try {
			ResponseEntityDTO responseDTO = new ResponseEntityDTO();
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			responseDTO.setHeaders(headers);
			responseDTO.setStatus(502);
			File file = new File(
					getContainerPath(containerName) + File.separator + OUTPUT + File.separator + "output.json");
			if (file.exists()) {
				FileReader fr = new FileReader(file);
				JSONParser parser = new JSONParser(fr);
				JSONObject outputJson = new JSONObject(parser.parseObject());
				responseDTO.setStatus(outputJson.getJSONObject("status").getInt("code"));
				Iterator<String> keys = outputJson.getJSONObject("headers").keys();

				while (keys.hasNext()) {
					String key = keys.next();
					headers.add(key, outputJson.getJSONObject("headers").getString(key));
				}
				responseDTO.setHeaders(headers);
				JSONObject body = outputJson.getJSONObject("body");
				if (body.isEmpty()) {
					return responseDTO;
				}
				if (body.getString("type").equals("file")) {
					Path out = Paths.get(getContainerPath(containerName) + File.separator + OUTPUT + File.separator
							+ body.get("content"));
					Resource resource = new UrlResource(out.toUri());
					responseDTO.setBody(resource);
				} else {
					responseDTO
							.setBody(outputJson.getJSONObject("body").get("content").toString().replace("\\\"", "\""));
				}
			}
			return responseDTO;

		} catch (IOException | JSONException | ParseException e) {
			log.info("Invalid function output format. \n" + e.getMessage());
			throw new FunctionExecutionException(mail, INVALID_FUNCTION_OUTPUT_FORMAT, e);
		}
	}

	private String getContainerPath(String containerName) {
		return lambdaPath + File.separator + "container_" + containerName;
	}

}
