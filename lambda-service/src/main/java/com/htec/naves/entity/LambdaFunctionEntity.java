package com.htec.naves.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.htec.naves.enums.Trigger;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "lambda_function")
@Table(name = "lambda_function")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class LambdaFunctionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "function_id")
	private Long functionId;

	@Column(name = "function_name")
	private String functionName;

	@Column(name = "function_link")
	private String functionLink;

	@Column(name = "public")
	private Boolean publicAccess;

	@Enumerated(EnumType.STRING)
	@Column(name = "function_trigger")
	private Trigger trigger;

	private String prefix;
	private String suffix;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id", referencedColumnName = "userId")
	private RegisteredUserEntity owner;

	@ManyToMany(cascade = { CascadeType.PERSIST })
	@JoinTable(name = "lambda_project", joinColumns = @JoinColumn(name = "function_id"), inverseJoinColumns = @JoinColumn(name = "project_id"))
	private Set<ProjectEntity> projects = new HashSet<>();

	@OneToMany(mappedBy = "lambdaFunctionEntity", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<FunctionExecutionEntity> executions = new ArrayList<>();

	public void addProject(ProjectEntity project) {
		projects.add(project);
		project.getLambdas().add(this);
	}

	public void removeProject(ProjectEntity project) {
		projects.remove(project);
		project.getLambdas().remove(this);
	}

	@Override
	public boolean equals(Object object) {
		if (this == object)
			return true;
		if (!(object instanceof LambdaFunctionEntity))
			return false;
		return functionId != null && functionId.equals(((LambdaFunctionEntity) object).getFunctionId());
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}

}
