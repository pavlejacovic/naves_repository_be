package com.htec.naves.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "project")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProjectEntity {
	@Id
	private Long id;
	@ManyToMany(mappedBy = "projects")
	private Set<LambdaFunctionEntity> lambdas = new HashSet<>();

	@ManyToOne(fetch = FetchType.EAGER)
	private RegisteredUserEntity owner;
	@Column(name = "project_name")
	private String projectName;

	@Override
	public boolean equals(Object object) {
		if (this == object)
			return true;
		if (object == null || getClass() != object.getClass())
			return false;
		ProjectEntity project = (ProjectEntity) object;
		return Objects.equals(id, project.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

}
