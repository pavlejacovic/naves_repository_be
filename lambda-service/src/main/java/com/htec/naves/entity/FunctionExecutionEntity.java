package com.htec.naves.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "execution")
@Table(name = "execution")
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class FunctionExecutionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "execution_id")
	private Long executionId;

	@ManyToOne
	@JoinColumn(name = "lambda_function")
	private LambdaFunctionEntity lambdaFunctionEntity;

	@Column(name = "execution_timestamp")
	private String executionTimestamp;

	@Column(name = "execution_duration")
	private Long executionDuration;

	@Column(name = "execution_source")
	private Boolean source;

}
