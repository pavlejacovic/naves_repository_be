package com.htec.naves.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsEntity {

	private String date;
	private String functionName;
	private Double averageExecutionDuration;
	private Integer maxExecutionDuration;
	private Integer minExecutionDuration;
	private Integer numberOfExecutions;
	private Integer numberOfExecutionsByOtherUsers;

}
