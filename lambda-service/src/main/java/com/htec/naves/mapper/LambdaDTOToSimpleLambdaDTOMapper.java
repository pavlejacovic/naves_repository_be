package com.htec.naves.mapper;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.htec.naves.dto.LambdaFunctionDTO;
import com.htec.naves.dto.SimpleLambdaDTO;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.repository.ProjectRepository;

@Component
@Transactional
public class LambdaDTOToSimpleLambdaDTOMapper {

	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectEntityDTOMapper projectMapper;

	public SimpleLambdaDTO toSimpleDto(LambdaFunctionDTO lambda) {
		SimpleLambdaDTO simpleLambda = new SimpleLambdaDTO();
		simpleLambda.setFunctionLink(lambda.getFunctionLink());
		simpleLambda.setFunctionName(lambda.getFunctionName());
		simpleLambda.setOwner(lambda.getOwner());
		simpleLambda.setPublicAccess(lambda.getPublicAccess());
		simpleLambda.setTrigger(lambda.getTrigger());
		simpleLambda.setFunctionId(lambda.getFunctionId());
		for (Long projectId : lambda.getProjectIds()) {
			Optional<ProjectEntity> project = projectRepository.findById(projectId);
			if (project.isPresent()) {
				simpleLambda.getProjects().add(projectMapper.toDto(project.get()));
			}
		}
		return simpleLambda;

	}
}
