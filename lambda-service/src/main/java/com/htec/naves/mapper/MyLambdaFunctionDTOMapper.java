package com.htec.naves.mapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.htec.naves.dto.LambdaFunctionDTO;
import com.htec.naves.entity.LambdaFunctionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.repository.ProjectRepository;

@Component
@Transactional
public class MyLambdaFunctionDTOMapper {
	@Autowired
	private RegisteredUserEntityDTOMapper registeredUserMapper;
	@Autowired
	private ProjectRepository projectRepository;

	public LambdaFunctionDTO toDTO(LambdaFunctionEntity functionEntity) {
		LambdaFunctionDTO functionDto = new LambdaFunctionDTO();
		functionDto.setFunctionId(functionEntity.getFunctionId());
		functionDto.setFunctionName(functionEntity.getFunctionName());
		functionDto.setOwner(registeredUserMapper.toDTO(functionEntity.getOwner()));
		functionDto.setPublicAccess(functionEntity.getPublicAccess());
		functionDto.setTrigger(functionEntity.getTrigger());
		functionDto.setFunctionLink(functionEntity.getFunctionLink());
		functionDto.setPrefix(functionEntity.getPrefix());
		functionDto.setSuffix(functionEntity.getSuffix());
		List<Long> projects = functionEntity.getProjects().stream().map(ProjectEntity::getId)
				.collect(Collectors.toList());
		functionDto.setProjectIds(projects);
		return functionDto;
	}

	public LambdaFunctionEntity toEntity(LambdaFunctionDTO functionDTO) {
		LambdaFunctionEntity functionEntity = new LambdaFunctionEntity();
		functionEntity.setFunctionId(functionDTO.getFunctionId());
		functionEntity.setFunctionName(functionDTO.getFunctionName());
		functionEntity.setOwner(registeredUserMapper.toEntity(functionDTO.getOwner()));
		functionEntity.setPublicAccess(functionDTO.getPublicAccess());
		functionEntity.setTrigger(functionDTO.getTrigger());
		functionEntity.setFunctionLink(functionDTO.getFunctionLink());
		functionEntity.setPrefix(functionDTO.getPrefix());
		functionEntity.setSuffix(functionDTO.getSuffix());
		for (Long projectId : functionDTO.getProjectIds()) {
			Optional<ProjectEntity> existingProject = projectRepository.findById(projectId);
			if (existingProject.isPresent()) {
				functionEntity.addProject(existingProject.get());
			} else {
				ProjectEntity project = new ProjectEntity();
				project.setId(projectId);
				functionEntity.addProject(project);
			}
		}
		return functionEntity;

	}
}
