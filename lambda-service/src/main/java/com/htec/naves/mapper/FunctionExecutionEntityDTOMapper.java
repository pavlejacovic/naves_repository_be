package com.htec.naves.mapper;

import org.mapstruct.Mapper;

import com.htec.naves.dto.FunctionExecutionDTO;
import com.htec.naves.entity.FunctionExecutionEntity;

@Mapper(componentModel = "spring")
public interface FunctionExecutionEntityDTOMapper {

	public FunctionExecutionDTO toDto(FunctionExecutionEntity entity);

	public FunctionExecutionEntity toEntity(FunctionExecutionDTO dto);
}
