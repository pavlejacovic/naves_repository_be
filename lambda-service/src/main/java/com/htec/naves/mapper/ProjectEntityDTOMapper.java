package com.htec.naves.mapper;

import org.mapstruct.Mapper;

import com.htec.naves.dto.ProjectDTO;
import com.htec.naves.entity.ProjectEntity;

@Mapper(componentModel = "spring")
public interface ProjectEntityDTOMapper {
	public ProjectDTO toDto(ProjectEntity entity);

	public ProjectEntity toEntity(ProjectDTO dto);
}
