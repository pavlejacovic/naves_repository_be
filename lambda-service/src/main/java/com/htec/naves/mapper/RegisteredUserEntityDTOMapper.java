package com.htec.naves.mapper;

import org.mapstruct.Mapper;

import com.htec.naves.dto.RegisteredUserDTO;
import com.htec.naves.entity.RegisteredUserEntity;

@Mapper(componentModel = "spring")
public interface RegisteredUserEntityDTOMapper {
	RegisteredUserDTO toDTO(RegisteredUserEntity entity);

	RegisteredUserEntity toEntity(RegisteredUserDTO project);
}
