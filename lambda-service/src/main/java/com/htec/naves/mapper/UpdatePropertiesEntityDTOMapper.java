package com.htec.naves.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htec.naves.dto.LambdaFunctionDTO;
import com.htec.naves.dto.LambdaFunctionPropertiesUpdateDTO;
import com.htec.naves.entity.LambdaFunctionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.repository.ProjectRepository;

@Component
public class UpdatePropertiesEntityDTOMapper {
	@Autowired
	RegisteredUserEntityDTOMapper userMapper;
	@Autowired
	ProjectRepository projectRepository;

	public LambdaFunctionPropertiesUpdateDTO toDto(LambdaFunctionEntity entity) {
		LambdaFunctionPropertiesUpdateDTO dto = new LambdaFunctionPropertiesUpdateDTO();
		dto.setFunctionId(entity.getFunctionId());
		dto.setFunctionName(entity.getFunctionName());
		dto.setOwner(userMapper.toDTO(entity.getOwner()));
		dto.setPublicAccess(entity.getPublicAccess());
		dto.setTrigger(entity.getTrigger());
		for (ProjectEntity project : entity.getProjects()) {
			dto.getProjectIds().add(project.getId());
		}
		return dto;
	}

	public LambdaFunctionDTO dtoToDto(LambdaFunctionPropertiesUpdateDTO updateDTO) {
		LambdaFunctionDTO dto = new LambdaFunctionDTO();
		dto.setFunctionId(updateDTO.getFunctionId());
		dto.setFunctionName(updateDTO.getFunctionName());
		dto.setOwner(updateDTO.getOwner());
		dto.setPublicAccess(updateDTO.getPublicAccess());
		dto.setTrigger(updateDTO.getTrigger());
		for (Long projectId : updateDTO.getProjectIds()) {
			dto.getProjectIds().add(projectId);
		}
		return dto;
	}

	public LambdaFunctionEntity toEntity(LambdaFunctionPropertiesUpdateDTO dto) {
		LambdaFunctionEntity entity = new LambdaFunctionEntity();
		entity.setFunctionId(dto.getFunctionId());
		entity.setFunctionName(dto.getFunctionName());
		entity.setOwner(userMapper.toEntity(dto.getOwner()));
		entity.setPublicAccess(dto.getPublicAccess());
		entity.setTrigger(dto.getTrigger());
		entity.getProjects().clear();
		for (Long projectId : dto.getProjectIds()) {
			ProjectEntity project = new ProjectEntity();
			project.setId(projectId);
			entity.addProject(project);
		}
		return entity;
	}

}
