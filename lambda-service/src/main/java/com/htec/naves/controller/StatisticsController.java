package com.htec.naves.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.service.FunctionExecutionStatisticsService;
import com.htec.naves.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Api calls for function execution statistics
 * 
 * @author Pavle Jacovic
 *
 */
@RestController
@Slf4j
public class StatisticsController {

	@Autowired
	private FunctionExecutionStatisticsService statisticsService;
	@Autowired
	private JwtUtil jwtUtil;

	/**
	 * Gets statistics for desired function that contains information about each
	 * execution in the last X days
	 * 
	 * @param token
	 * @param functionId
	 * @return
	 */
	@GetMapping("/statistics/{function_id}/{days}")
	public ResponseEntity<Object> getStatisticsForFunction(@RequestHeader(name = "Authorization") String token,
			@PathVariable("function_id") Long functionId, @PathVariable("days") int days) {

		log.info("User " + jwtUtil.extractUsername(token) + " requested to see statistics for his function with id: "
				+ functionId);
		return ResponseEntity.status(HttpStatus.OK)
				.body(statisticsService.getStatisticsForFunction(token, functionId, days));
	}

}
