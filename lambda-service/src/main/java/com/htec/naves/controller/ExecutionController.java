package com.htec.naves.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartRequest;

import com.htec.naves.dto.ResponseEntityDTO;
import com.htec.naves.enums.Trigger;
import com.htec.naves.service.ExecutionService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class ExecutionController {

	@Autowired
	private ExecutionService executionService;

	/**
	 * Executes the targeted function with the provided parameters if the content
	 * type is form-data.
	 * 
	 * @param link    uuid which represents the link to the target function
	 * @param token   token received during login if the user is logged in
	 * @param request HttpServletRequest
	 * @return HTTP response with a status, headers and response for the function
	 *         caller provided by the function
	 * @throws IOException
	 */
	@PostMapping(value = "/api/{link}", consumes = "multipart/form-data")
	public ResponseEntity<Object> executeFunction(@PathVariable String link,
			@RequestHeader(name = "Authorization", required = false) String token, HttpServletRequest request)
			throws IOException {
		MultipartRequest multipartRequest = (MultipartRequest) request;
		log.info("Exexuting POST function with multipart/form-data. Executing lambda on link: " + link);
		ResponseEntityDTO response = executionService.runContainerFromImage(link, token, request.getContentType(),
				request.getParameterMap(), multipartRequest.getFileMap(), null, Trigger.POST);
		HttpHeaders httpHeaders = new HttpHeaders(response.getHeaders());
		return ResponseEntity.status(response.getStatus()).headers(httpHeaders).body(response.getBody());
	}

	/**
	 * Executes the targeted function with the provided parameters if the content
	 * type is not form-data.
	 * 
	 * @param link             uuid which represents the link to the target function
	 * @param token            token received during login if the user is logged in
	 * @param contentType      type of content in the request body
	 * @param allRequestParams map of all request params
	 * @param body             body of the http request
	 * @return HTTP response with a status, headers and response for the function
	 *         caller provided by the function
	 * @throws IOException
	 */
	@PostMapping(value = "/api/{link}")
	public ResponseEntity<Object> executeFunction(@PathVariable String link,
			@RequestHeader(name = "Authorization", required = false) String token,
			@RequestHeader(name = "Content-Type", required = false) String contentType,
			@RequestParam Map<String, String[]> allRequestParams, @RequestBody String body) throws IOException {
		log.info("Exexuting POST function without files.Executing lambda on link: " + link);
		ResponseEntityDTO response = executionService.runContainerFromImage(link, token, contentType, allRequestParams,
				null, body, Trigger.POST);
		HttpHeaders httpHeaders = new HttpHeaders(response.getHeaders());
		return ResponseEntity.status(response.getStatus()).headers(httpHeaders).body(response.getBody());
	}

	/**
	 * Executes the targeted function with the provided parameters if the content
	 * type is not form-data.
	 * 
	 * @param link             uuid which represents the link to the target function
	 * @param token            token received during login if the user is logged in
	 * @param allRequestParams map of all request params
	 * @return HTTP response with a status, headers and response for the function
	 *         caller provided by the function
	 * @throws IOException
	 */
	@GetMapping(value = "/api/{link}")
	public ResponseEntity<Object> executeFunction(@PathVariable String link,
			@RequestHeader(name = "Authorization", required = false) String token,
			@RequestParam Map<String, String[]> allRequestParams) throws IOException {
		log.info("Executing GET function. Executing lambda on link: " + link);
		ResponseEntityDTO response = executionService.runContainerFromImage(link, token, "", allRequestParams, null,
				null, Trigger.GET);
		HttpHeaders httpHeaders = new HttpHeaders(response.getHeaders());
		return ResponseEntity.status(response.getStatus()).headers(httpHeaders).body(response.getBody());
	}
}
