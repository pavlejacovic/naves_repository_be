package com.htec.naves.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.dto.LambdaFunctionDTO;
import com.htec.naves.dto.LambdaFunctionExecutionUpdateDTO;
import com.htec.naves.dto.LambdaFunctionPropertiesUpdateDTO;
import com.htec.naves.service.LambdaService;
import com.htec.naves.util.JwtUtil;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.extern.slf4j.Slf4j;

/**
 * Controller for lambdas api calls
 * 
 * 
 * @author Pavle Jacovic
 * @author Mia Pecelj
 * @author Jovan Popovic
 * @author Ognjen Pejcic
 */
@RestController
@Slf4j
public class LambdaController {

	@Autowired
	private LambdaService lambdaService;
	@Autowired
	private JwtUtil jwtUtil;

	/**
	 * Creates a function with the provided attributes and jar file.
	 * 
	 * @param lambdaDto function information and jar file
	 * @param token     user's token from the authorization header
	 * @return HTTP response with a status and a message for the client
	 */
	@PostMapping("/")
	public ResponseEntity<Object> makeImage(@RequestHeader(name = "Authorization") String token,
			@Valid @ModelAttribute LambdaFunctionDTO lambdaDto, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			log.info("Error in creating function");
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach(error -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error in creating function " + errors);
		}

		switch (lambdaDto.getLanguage()) {
		case CSHARP:
			lambdaService.makeImageFromDll(lambdaDto, token);
			break;
		case JAVA:
			lambdaService.makeJavaFunction(lambdaDto, token);
			break;
		default:
			log.info("makeImage function -> User " + jwtUtil.extractUsername(token)
					+ "tried to create lambda unsuccessfully with name: " + lambdaDto.getFunctionName());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Unsupported language");
		}

		log.info("makeImage function -> User " + jwtUtil.extractUsername(token) + " created lambda "
				+ lambdaDto.getFunctionName());
		return ResponseEntity.status(HttpStatus.CREATED).body("Successfully created lambda");
	}

	/**
	 * Get all lambda functions of logged user whose info is stored in token
	 * 
	 * @param token contains information about loged user and expiration time
	 * @return
	 */
	@GetMapping("/functions")
	public ResponseEntity<Object> getMyFunctions(@RequestHeader(name = "Authorization") String token) {

		log.info("getMyFunctions function -> User " + jwtUtil.extractUsername(token)
				+ " requested to see all of his functions ");
		return ResponseEntity.status(HttpStatus.OK).body(lambdaService.getMyFunctions(token));
	}

	/**
	 * Get all lambda functions of logged user by page whose info is stored in token
	 * 
	 * @param token contains information about logged user and expiration time
	 * @param page  parameter serves as a mechanism for splitting results into pages
	 * @return
	 */
	@GetMapping("/functions/page")
	public ResponseEntity<Object> getMyFunctionPagable(@RequestHeader(name = "Authorization") String token,
			Pageable page) {

		log.info("getMyFunctionPagable function -> User " + jwtUtil.extractUsername(token)
				+ " requested to see all of his functions divided by page");
		return ResponseEntity.status(HttpStatus.OK).body(lambdaService.getMyFunctions(token, page));
	}

	/**
	 * updateFunctionProperties updates all properties of the function in database
	 * but not the execution file (docker image)
	 * 
	 * @param token
	 * @param executionDTO
	 * @param functionId
	 * @param bindingResult
	 * @return
	 */
	@PutMapping("/functions/properties/{function_id}")
	public ResponseEntity<Object> updateFunctionProperties(@RequestHeader(name = "Authorization") String token,
			@PathVariable("function_id") Long functionId,
			@Valid @RequestBody LambdaFunctionPropertiesUpdateDTO propetriesDTO, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			log.info("Error in updating function properties");
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach(error -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("Error in updating function properties " + errors);
		}
		log.info("updateFunctionProperties function -> User " + jwtUtil.extractUsername(token) + "updated function "
				+ propetriesDTO.toString());
		return ResponseEntity.status(HttpStatus.OK)
				.body(lambdaService.updateFunctionProperties(token, propetriesDTO, functionId));
	}

	/**
	 * updateFunctionExecution updates jar file of the function (docker image)
	 * 
	 * @param token
	 * @param executionDTO
	 * @param functionId
	 * @param bindingResult
	 * @return
	 */
	@PutMapping("/functions/execution/{function_id}")
	public ResponseEntity<Object> updateFunctionExecution(@RequestHeader(name = "Authorization") String token,
			@PathVariable("function_id") Long functionId,
			@Valid @RequestBody LambdaFunctionExecutionUpdateDTO executionDTO, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			log.info("Error in updating function's execution");
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach(error -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("Error in updating function's execution " + errors);
		}
		log.info("updateFunctionExecution function -> User " + jwtUtil.extractUsername(token)
				+ "updated function's execution " + executionDTO.toString());

		switch (executionDTO.getLanguage()) {
		case CSHARP:
			return ResponseEntity.status(HttpStatus.OK)
					.body(lambdaService.updateFunctionExecutionDotnet(token, executionDTO, functionId));
		case JAVA:
			return ResponseEntity.status(HttpStatus.OK)
					.body(lambdaService.updateFunctionExecutionJava(token, executionDTO, functionId));
		default:
			log.info("updateFunctionExecution function -> User " + jwtUtil.extractUsername(token)
					+ "tried to update lambda unsuccessfully");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Unsupported language");
		}
	}

	/**
	 * Deletes user function
	 * 
	 * @param token      user's token from the authorization header received during
	 *                   login
	 * @param functionId id of function for which we generate new link
	 * @return ResponseEntity with status 200 and body that contains successful
	 *         execution message
	 */
	@DeleteMapping("/functions/{functionId}")
	public ResponseEntity<Object> deleteFunction(@PathVariable(name = "functionId") long functionId,
			@RequestHeader(name = "Authorization") String token) {

		log.info("deleteFunction function -> User " + jwtUtil.extractUsername(token)
				+ " requested to delete his function with id: " + functionId);
		lambdaService.deleteFunction(functionId, token);
		return ResponseEntity.status(HttpStatus.OK).body("Successfully deleted function with id: " + functionId);

	}

	/**
	 * Generates new link for the requested function if the user who requested this
	 * operation is owner of that function
	 * 
	 * @param token      user's token from the authorization header received during
	 *                   login
	 * @param functionId id of function for which we generate new link
	 * @return ResponseEntity with status 200 and body that contains a newly
	 *         generated link if operation was successful
	 */
	@PatchMapping("/functions/{functionId}/link")
	public ResponseEntity<Object> generateNewFunctionLink(@RequestHeader(name = "Authorization") String token,
			@PathVariable(name = "functionId") Long functionId) {
		log.info("generateNewFunctionLink function -> User " + jwtUtil.extractUsername(token)
				+ " requested to generate new link for function with id: " + functionId);
		String newLink = lambdaService.generateNewFunctionLink(token, functionId);
		log.info("generateNewFunctionLink function -> User " + jwtUtil.extractUsername(token)
				+ " has successfully generated new link for his function with id: " + functionId);
		return ResponseEntity.status(HttpStatus.OK).body(newLink);
	}
}
