package com.htec.naves.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.enums.Library;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.service.LibraryService;
import com.htec.naves.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class LibraryController {

	@Autowired
	private LibraryService libraryService;

	@Autowired
	private JwtUtil jwtUtil;

	/**
	 * Downloads library for specific programming language
	 * 
	 * @param token    user's token from the authorization header received during
	 *                 login
	 * @param language programming language for which library is being downloaded
	 * 
	 * @return Status 200 and library for specified programming language if
	 *         requested operation was successful
	 *
	 * @throws FileSystemException      when encountering a problem with file system
	 *                                  or if requested library not present
	 * @throws InvalidArgumentException when user specified language that is not
	 *                                  supported
	 *
	 */
	@GetMapping("/library")
	public ResponseEntity<Object> downloadLibrary(@RequestHeader(name = "Authorization") String token,
			@RequestParam("lang") Library language) {
		String email = jwtUtil.extractUsername(token);
		log.info("User " + email + " requested to download library for " + language + " language");
		Resource resource = libraryService.downloadLibrary(email, language);
		return ResponseEntity.status(HttpStatus.OK)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}
}
