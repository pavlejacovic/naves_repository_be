package com.htec.naves.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.naves.entity.RegisteredUserEntity;

@Repository
@Transactional
public interface RegisteredUserRepository extends JpaRepository<RegisteredUserEntity, Long> {

	Optional<RegisteredUserEntity> findByEmail(String email);

	List<RegisteredUserEntity> findBySubscribed(Boolean subscribed);
}