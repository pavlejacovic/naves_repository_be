package com.htec.naves.repository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.naves.entity.LambdaFunctionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;

@Repository
@Transactional
public interface LambdaFunctionRepository extends JpaRepository<LambdaFunctionEntity, Long> {

	public Optional<LambdaFunctionEntity> findByFunctionId(Long functionId);

	public List<LambdaFunctionEntity> findByOwner(RegisteredUserEntity owner);

	public Optional<LambdaFunctionEntity> findByFunctionName(String functionName);

	Optional<LambdaFunctionEntity> findByFunctionLink(String link);

	public List<LambdaFunctionEntity> findAllByOwner(RegisteredUserEntity owner, Pageable page);

	public boolean existsByFunctionName(String functionName);

	public List<LambdaFunctionEntity> findByProjects(ProjectEntity projects);

	List<LambdaFunctionEntity> findByProjectsIn(HashSet<ProjectEntity> projects);
}
