package com.htec.naves.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.htec.naves.entity.FunctionExecutionEntity;
import com.htec.naves.entity.LambdaFunctionEntity;

@Transactional
public interface FunctionExecutionRepository extends JpaRepository<FunctionExecutionEntity, Long> {

	public List<FunctionExecutionEntity> findByLambdaFunctionEntity(LambdaFunctionEntity lambdaFunctionEntity);

	@Query(value = "SELECT SUBSTRING(exec.execution_timestamp, 1, 10) as date,lf.function_name,\r\n"
			+ "AVG(exec.execution_duration) as average_execution_duration, MAX(exec.execution_duration) as max_execution_duration,\r\n"
			+ "MIN(exec.execution_duration) as min_execution_duration, COUNT(*) as number_of_executions,\r\n"
			+ "SUM(case exec.execution_source when FALSE then 1 else 0 end) as number_of_executions_by_other_users\r\n"
			+ "FROM lambda_function lf \r\n" + "JOIN registered_user ru ON lf.owner_id=ru.user_id\r\n"
			+ "JOIN execution exec ON lf.function_id=exec.lambda_function\r\n"
			+ "WHERE ru.subscribed=TRUE AND ru.user_id=?1\r\n"
			+ "GROUP BY date, exec.lambda_function", nativeQuery = true)
	public List<Object[]> findDailyStatisticsForSubscribedUser(Long userId);

	@Query(value = "SELECT SUBSTRING(exec.execution_timestamp, 1, 10) as date,lf.function_name, COUNT(*) as number_of_executions\r\n"
			+ "FROM lambda_function lf \r\n" + "JOIN execution exec ON lf.function_id=exec.lambda_function\r\n"
			+ "WHERE lf.function_id=?1\r\n" + "GROUP BY date, exec.lambda_function;", nativeQuery = true)
	public List<Object[]> findStatisticsForFunction(Long functionId);

}
