package com.htec.naves.consumer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.htec.naves.dto.PathDTO;
import com.htec.naves.entity.LambdaFunctionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.enums.Trigger;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.repository.LambdaFunctionRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.service.ExecutionService;

import lombok.extern.slf4j.Slf4j;

/**
 * Class that is used to catch messages via @RabbitListener from publisher
 * (queues = "Verified-users") -> queue name from RabbitMq dashboard
 * 
 * 
 * @author Pavle Jacovic
 * @author Mia Pecelj
 * @Author Jovan Popovic
 * 
 */
@Service
@Slf4j
public class Consumer {

	private static final String PROJECTID = "projectId";

	@Autowired
	private RegisteredUserRepository registeredUserRepository;

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private LambdaFunctionRepository lambdaRepository;

	@Autowired
	private ExecutionService executionService;

	/**
	 * Every time publisher sends the message, the class that is annotated
	 * with @RabbitListener catches those messages and deserializes them so data can
	 * be accessible and written in database like here if needed
	 * 
	 * @param message array of bytes received from the publisher
	 */
	@RabbitListener(queues = "Verified-users-lambda-service")
	public void getMessageUser(byte[] message) {
		try (ObjectInput in = new ObjectInputStream(new ByteArrayInputStream(message))) {
			HashMap<String, String> data = (HashMap<String, String>) in.readObject();

			log.info("Saving registered user  lambda db. User: " + data.get("email"));
			registeredUserRepository.save(new RegisteredUserEntity(Long.valueOf(data.get("userId")), data.get("email"),
					Long.valueOf(data.get("type")), Boolean.parseBoolean(data.get("subscribed"))));
		} catch (Exception e) {
			log.info("Couldn't save user in lambda db. \n" + e.getMessage());
		}

	}

	/**
	 * This method gets messages from Delete-project queue and gets the id of the
	 * project that was deleted and sends a message to the lambda database so that
	 * all triggers regarding the deleted project can also be deleted.
	 * 
	 * @param message message containing project id that was send from the publisher
	 *                in project controller.
	 */
	@Transactional
	@RabbitListener(queues = "Delete-Project")
	public void getMessageDeleteProject(byte[] message) {
		try (ObjectInput in = new ObjectInputStream(new ByteArrayInputStream(message))) {

			Long projectId = Long.parseLong(in.readObject().toString());
			Optional<ProjectEntity> project = projectRepository.findById(projectId);
			if (project.isEmpty()) {
				log.info("Project with id: " + projectId + " doesn't exist.");
				throw new ResourceNotFoundException("Project does not exist");
			}
			for (LambdaFunctionEntity lambda : project.get().getLambdas()) {
				lambda.removeProject(project.get());
				lambdaRepository.save(lambda);
			}

			log.info("Deleting project with id: " + projectId + " from lambda db.");
			projectRepository.deleteById(projectId);
		} catch (Exception e) {
			log.info("Project couldn't be deleted. \n" + e.getMessage());
		}
	}

	/**
	 * This method gets project id and project owner from Create-Project queue that
	 * are going to be saved in project table in lambda service database
	 * 
	 * @param message message containing hashmap in which are project id and owner
	 */
	@Transactional
	@RabbitListener(queues = "Create-Project")
	public void getMessageCreateProject(byte[] message) {
		try (ObjectInput in = new ObjectInputStream(new ByteArrayInputStream(message))) {

			HashMap<String, Object> data = (HashMap<String, Object>) in.readObject();

			String email = data.get("ownerEmail").toString();
			Optional<RegisteredUserEntity> owner = registeredUserRepository.findByEmail(email);

			if (owner.isEmpty()) {
				log.info("Owner of the project couldn't be found.");
				throw new ResourceNotFoundException("Owner of the project not found!");
			}

			ProjectEntity projectEntity = new ProjectEntity();
			Long projectId = Long.valueOf(data.get(PROJECTID).toString());
			String projectName = data.get("projectName").toString();
			projectEntity.setId(projectId);
			projectEntity.setOwner(owner.get());
			projectEntity.setProjectName(projectName);

			projectRepository.save(projectEntity);
		} catch (Exception e) {
			log.info("Project couldn't be created. \n" + e.getMessage());
		}
	}

	@Transactional
	@RabbitListener(queues = "Rename-Project")
	public void getMessageRenameProject(byte[] message) {
		try (ObjectInput in = new ObjectInputStream(new ByteArrayInputStream(message))) {

			HashMap<String, Object> data = (HashMap<String, Object>) in.readObject();

			Long projectId = Long.valueOf(data.get(PROJECTID).toString());
			Optional<ProjectEntity> projectEntityOptional = projectRepository.findById(projectId);
			if (projectEntityOptional.isEmpty()) {
				throw new ResourceNotFoundException("Project does not exist");
			}

			ProjectEntity projectEntity = projectEntityOptional.get();

			String projectName = data.get("projectName").toString();
			projectEntity.setProjectName(projectName);

			projectRepository.save(projectEntity);
		} catch (Exception e) {
			log.info("Project couldn't be renamed. \n" + e.getMessage());
		}
	}

	/**
	 * When a user uploads, deletes or updates files in a project a message is sent
	 * to the Trigger queue containing information about files affected, project id,
	 * user email and operation that was performed.
	 * 
	 * @param message message containing all the informations.
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	@RabbitListener(queues = "Trigger")
	public void getMessageTrigger(byte[] message) {
		try (ObjectInput in = new ObjectInputStream(new ByteArrayInputStream(message))) {

			HashMap<String, Object> data = (HashMap<String, Object>) in.readObject();
			List<PathDTO> paths = (List<PathDTO>) data.get("params");
			Long projectId = Long.valueOf(data.get(PROJECTID).toString());
			String token = data.get("token").toString();
			Trigger action = Trigger.valueOf(data.get("action").toString());

			log.info("Invoking lambda on action: " + action + "for project:" + projectId);
			invokeLambda(paths, projectId, token, action);
		} catch (Exception e) {
			log.info("Lambda couldn't be invoked. \n" + e.getMessage());
		}
	}

	private void invokeLambda(List<PathDTO> paths, Long projectId, String token, Trigger action) throws IOException {
		HashMap<String, String[]> params = new HashMap<>();
		String[] projectIds = { projectId.toString() };
		params.put(PROJECTID, projectIds);

		List<String> pathsList = paths.stream().map(PathDTO::getPath).collect(Collectors.toList());
		String[] pathsArray = pathsList.toArray(new String[pathsList.size()]);

		HashSet<ProjectEntity> projects = new HashSet<>();
		Optional<ProjectEntity> project = projectRepository.findById(projectId);

		if (project.isPresent()) {
			projects.add(project.get());
		}

		List<LambdaFunctionEntity> links = lambdaRepository.findByProjectsIn(projects);
		String link = null;
		for (LambdaFunctionEntity lambda : links) {
			if (lambda.getTrigger().equals(action)) {
				link = lambda.getFunctionLink();
				String prefix = lambda.getPrefix();
				String suffix = lambda.getSuffix();

				List<String> filteredPaths = Stream.of(pathsArray)
						.filter(path -> path.startsWith(prefix) && path.endsWith(suffix)).collect(Collectors.toList());

				params.put("path", filteredPaths.toArray(new String[filteredPaths.size()]));
				try {
					log.info("Executing lambda with link: " + link + "on action: " + action);
					executionService.runContainerFromImage(link, token, null, params, null, null, action);
				} catch (Exception e) {
					log.info("Lambda couldn't be executed. \n" + e.getMessage());
				}
			}
		}
	}
}