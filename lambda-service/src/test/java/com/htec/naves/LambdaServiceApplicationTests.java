package com.htec.naves;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import com.htec.naves.constants.UserType;
import com.htec.naves.dto.LambdaFunctionDTO;
import com.htec.naves.dto.RegisteredUserDTO;
import com.htec.naves.entity.LambdaFunctionEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.enums.Language;
import com.htec.naves.enums.Trigger;
import com.htec.naves.exception.CommandLineException;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.mapper.MyLambdaFunctionDTOMapper;
import com.htec.naves.mapper.RegisteredUserEntityDTOMapper;
import com.htec.naves.repository.LambdaFunctionRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.service.LambdaService;
import com.htec.naves.util.CommandLineExecutor;
import com.htec.naves.util.JwtUtil;

/**
 * Service for generating docker images and executing docker containers
 * 
 * 
 * @author Pavle Jacovic
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LambdaServiceApplication.class)
class LambdaServiceApplicationTests {

	@Value("${lambda.path}")
	private String lambdaPath;

	private static final String TEST_STRING = "TEST";
	private static final String TEST_STRING_JAR = "jar";
	private static final byte[] dataBytesTest = new byte[] { 1, 2, 3, 4 };
	private static final RegisteredUserEntity registeredUserEntityTest = new RegisteredUserEntity();

	private static final MockMultipartFile fileTest = new MockMultipartFile("user-file", "test.txt", null,
			"test data".getBytes());

	private static final RegisteredUserDTO reguDtoTest = new RegisteredUserDTO(TEST_STRING, 1l, UserType.CLIENT, true);
	private static final LambdaFunctionDTO lambdaFDtoTest = new LambdaFunctionDTO(1l, TEST_STRING, TEST_STRING, true,
			Trigger.GET, fileTest, reguDtoTest, Language.JAVA, Collections.emptyList(), "", "");

	@Mock
	private JwtUtil jwtUtil;

	@Mock
	private RegisteredUserRepository registeredUserRepository;

	@Mock
	private LambdaFunctionRepository lambdaFunctionRepository;

	@Mock
	private ProcessBuilder processBuilder;

	@Mock
	private MyLambdaFunctionDTOMapper lambdaFunctionDTOMapper;

	@Mock
	private RegisteredUserEntityDTOMapper RegisteredUserDtoMapper;

	@Mock
	private CommandLineExecutor commandLineExecutor;

	@Mock
	private Process process;

	@Mock
	private File file;

	@Mock
	private LambdaFunctionEntity lambdaFunctionEntity;

	@InjectMocks
	private LambdaService testee;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	@DisplayName("Make image from jar happy path")
	void makeImageFromJar() {
		try (MockedStatic<Paths> staticMockPaths = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedConstruction<FileWriter> fileWriterMock = Mockito.mockConstruction(FileWriter.class)) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);

			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(TEST_STRING_JAR);

			Mockito.doReturn(Optional.of(registeredUserEntityTest)).when(registeredUserRepository)
					.findByEmail(TEST_STRING);

			staticMockFiles.when(() -> Files.readAllBytes(any())).thenReturn(dataBytesTest);

			Mockito.doReturn(lambdaFunctionEntity).when(lambdaFunctionDTOMapper).toEntity(lambdaFDtoTest);

			Mockito.doReturn(lambdaFunctionEntity).when(lambdaFunctionRepository).save(lambdaFunctionEntity);

			testee.makeJavaFunction(lambdaFDtoTest, TEST_STRING);
		}
	}

	@Test
	@DisplayName("Cannot make image - makeImageFromJar")
	void canNotMakeImageFromJar() {
		try (MockedStatic<Paths> staticMockPaths = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedConstruction<FileWriter> fileWriterMock = Mockito.mockConstruction(FileWriter.class)) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);

			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(TEST_STRING);

			assertThrows(InvalidArgumentException.class, () -> {
				testee.makeJavaFunction(lambdaFDtoTest, TEST_STRING);
			});

		}
	}

	@Test
	@DisplayName("CommandLineException - makeImageFromJar")
	@Disabled
	void errorWhileCreatingFunction1() {
		try (MockedStatic<Paths> staticMockPaths = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedConstruction<FileWriter> fileWriterMock = Mockito.mockConstruction(FileWriter.class)) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);

			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(TEST_STRING_JAR);

			Mockito.doReturn(Optional.of(registeredUserEntityTest)).when(registeredUserRepository)
					.findByEmail(TEST_STRING);

			staticMockFiles.when(() -> Files.readAllBytes(any())).thenReturn(dataBytesTest);

			assertThrows(CommandLineException.class, () -> {
				testee.makeJavaFunction(lambdaFDtoTest, TEST_STRING);
			});
		}
	}

	@Test
	@DisplayName("Function already exist prepareLambdaStorage")
	void funAlreadyExist() {
		try (MockedStatic<Paths> staticMockPaths = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedConstruction<FileWriter> fileWriterMock = Mockito.mockConstruction(FileWriter.class)) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);

			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(TEST_STRING_JAR);

			Mockito.doReturn(Optional.of(lambdaFunctionEntity)).when(registeredUserRepository).findByEmail(TEST_STRING);

			Mockito.doReturn(Optional.of(registeredUserEntityTest)).when(registeredUserRepository)
					.findByEmail(TEST_STRING);

			Mockito.doReturn(true).when(lambdaFunctionRepository).existsByFunctionName(any());

			assertThrows(ResourceAlreadyExistsException.class, () -> {
				testee.makeJavaFunction(lambdaFDtoTest, TEST_STRING);
			});
		}
	}

	@Test
	@DisplayName("Error while creating function FileSystemException1 prepareLambdaStorage")
	void errorWhileCreatingFunction2() {
		try (MockedStatic<Paths> staticMockPaths = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedConstruction<FileWriter> fileWriterMock = Mockito.mockConstruction(FileWriter.class)) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);

			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(TEST_STRING_JAR);

			Mockito.doReturn(Optional.of(registeredUserEntityTest)).when(registeredUserRepository)
					.findByEmail(TEST_STRING);

			staticMockFiles.when(() -> Files.exists(any())).thenReturn(false);

			staticMockFiles.when(() -> Files.createDirectories(any())).thenThrow(IOException.class);

			assertThrows(FileSystemException.class, () -> {
				testee.makeJavaFunction(lambdaFDtoTest, TEST_STRING);
			});
		}
	}

	@Test
	@DisplayName("Error while creating function FileSystemException2 prepareLambdaStorage")
	void errorWhileCreatingFunction3() {
		try (MockedStatic<Paths> staticMockPaths = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedConstruction<FileWriter> fileWriterMock = Mockito.mockConstruction(FileWriter.class)) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);

			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(TEST_STRING_JAR);

			Mockito.doReturn(Optional.of(registeredUserEntityTest)).when(registeredUserRepository)
					.findByEmail(TEST_STRING);

			staticMockFiles.when(() -> Files.exists(any())).thenReturn(false);

			staticMockFiles.when(() -> Files.readAllBytes(any())).thenThrow(IOException.class);

			assertThrows(FileSystemException.class, () -> {
				testee.makeJavaFunction(lambdaFDtoTest, TEST_STRING);
			});
		}
	}

}
