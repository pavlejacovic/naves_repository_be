package com.htec.naves;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;

import com.htec.naves.dto.LambdaFunctionDTO;
import com.htec.naves.dto.LambdaFunctionPropertiesUpdateDTO;
import com.htec.naves.dto.RegisteredUserDTO;
import com.htec.naves.entity.FunctionExecutionEntity;
import com.htec.naves.entity.LambdaFunctionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.enums.Language;
import com.htec.naves.enums.Trigger;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.mapper.MyLambdaFunctionDTOMapper;
import com.htec.naves.mapper.RegisteredUserEntityDTOMapper;
import com.htec.naves.mapper.UpdatePropertiesEntityDTOMapper;
import com.htec.naves.repository.LambdaFunctionRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.service.LambdaService;
import com.htec.naves.service.UnzippingService;
import com.htec.naves.util.JwtUtil;

/**
 * Service for generating docker images and executing docker containers
 * 
 * 
 * @author Pavle Jacovic
 */

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = LambdaServiceApplication.class)
class LambdaServiceTest {

	@Value("${lambda.path}")
	private String lambdaPath;

	private static final String EMAIL = "johndoe@gmail.com";
	private static final String TOKEN = "authorizationtoken12345678910";
	private static final String JAR_EXTENSION = "jar";
	private static final String ZIP_EXTENSION = "zip";
	private static final String WRONG_EXTENSION = "exten";
	private static final String FUNCTION_NAME = "function1";
	private static final String FUNCTION_LINK = "linkforfunction123";
	private static final String SUCCESS_MESSAGE = "This is success message";
	private static final byte[] dataBytes = new byte[] { 1, 2, 3, 4 };
	private static final RegisteredUserEntity registeredUserEntity = new RegisteredUserEntity(2l, EMAIL, 0l, true);
	private static final RegisteredUserEntity registeredUserEntity2 = new RegisteredUserEntity(14l, EMAIL, 0l, true);
	private static final MockMultipartFile multipartFile = new MockMultipartFile("user-file", "someFile.txt", null,
			"data".getBytes());
	private static final List<Long> projectsArray = Arrays.asList(0l, 1l, 2l, 3l, 4l, 5l);
	private static final LinkedList<Long> projects = new LinkedList<>(projectsArray);
	private static final RegisteredUserDTO registeredUserDTO = new RegisteredUserDTO(EMAIL, 1l, 0l, null);
	private static final LambdaFunctionDTO lambdaFunctionDTO = new LambdaFunctionDTO(1l, FUNCTION_NAME, FUNCTION_LINK,
			true, Trigger.GET, multipartFile, registeredUserDTO, Language.JAVA, projects, "", " ");
	private static final Set<ProjectEntity> projectsSet = new HashSet<ProjectEntity>();
	private static final List<FunctionExecutionEntity> testExecutions = new ArrayList<>();
	private static final LambdaFunctionPropertiesUpdateDTO lambdaFunctionPropertiesUpdateDTOTest = new LambdaFunctionPropertiesUpdateDTO(
			1l, FUNCTION_NAME, true, Trigger.POST, registeredUserDTO, projects, SUCCESS_MESSAGE, "", "");
	private static final LambdaFunctionEntity lambdaFunctionEntityTest = new LambdaFunctionEntity(1l, FUNCTION_NAME,
			FUNCTION_LINK, true, Trigger.POST, "", "", registeredUserEntity, projectsSet, testExecutions);

	@Mock
	private JwtUtil jwtUtil;

	@Mock
	private RegisteredUserRepository registeredUserRepository;

	@Mock
	private LambdaFunctionRepository lambdaFunctionRepository;

	@Mock
	private ProjectRepository projectRepository;

	@Mock
	private MyLambdaFunctionDTOMapper lambdaFunctionDTOMapper;

	@Mock
	private RegisteredUserEntityDTOMapper RegisteredUserDtoMapper;

	@Mock
	private UpdatePropertiesEntityDTOMapper updatePropertiesDTOMapper;

	@Mock
	private RegisteredUserEntity owner;

	@Mock
	private UnzippingService unzippingService;

	@InjectMocks
	private LambdaService testee;

	@Test
	@DisplayName("Make java function happy path")
	void shouldCreateJavaFunctionWhenPassedParametersAreOk() {
		try (MockedStatic<Paths> staticMockPaths = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedConstruction<FileWriter> fileWriterMock = Mockito.mockConstruction(FileWriter.class)) {

			// GIVEN
			Mockito.doReturn(EMAIL).when(jwtUtil).extractUsername(TOKEN);
			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(JAR_EXTENSION);
			Mockito.doReturn(Optional.of(registeredUserEntity)).when(registeredUserRepository).findByEmail(EMAIL);
			staticMockFiles.when(() -> Files.readAllBytes(any())).thenReturn(dataBytes);
			Mockito.doReturn(lambdaFunctionEntityTest).when(lambdaFunctionDTOMapper).toEntity(lambdaFunctionDTO);

			// WHEN
			Mockito.doReturn(lambdaFunctionEntityTest).when(lambdaFunctionRepository).save(lambdaFunctionEntityTest);

			// THEN
			testee.makeJavaFunction(lambdaFunctionDTO, TOKEN);
		}
	}

	@Test
	@DisplayName("Make java function with wrong file provided")
	void shouldNotCreateJavaFunctionWhenWrongTypeOfFileIsProvided() {
		try (MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class)) {

			// GIVEN
			Mockito.doReturn(EMAIL).when(jwtUtil).extractUsername(TOKEN);
			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(WRONG_EXTENSION);

			// WHEN-THEN
			assertThrows(InvalidArgumentException.class, () -> {
				testee.makeJavaFunction(lambdaFunctionDTO, TOKEN);
			});
		}
	}

	@Test
	@DisplayName("Make java function. Error while creating function 1")
	void shouldNotCreateJavaFunctionWhenLambdaFunctionDestinationPathIsNotValid() {
		try (MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);) {

			// GIVEN
			Mockito.doReturn(EMAIL).when(jwtUtil).extractUsername(TOKEN);
			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(JAR_EXTENSION);
			Mockito.doReturn(Optional.of(registeredUserEntity)).when(registeredUserRepository).findByEmail(EMAIL);
			staticMockFiles.when(() -> Files.createDirectories(any())).thenThrow(IOException.class);

			// WHEN-THEN
			assertThrows(FileSystemException.class, () -> {
				testee.makeJavaFunction(lambdaFunctionDTO, TOKEN);
			});
		}
	}

	@Test
	@DisplayName("Make java function. Error while creating function 2")
	void shouldNotCreateJavaFunctionWhenExeFilePathIsNotValid() {
		try (MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);) {

			// GIVEN
			Mockito.doReturn(EMAIL).when(jwtUtil).extractUsername(TOKEN);
			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(JAR_EXTENSION);
			Mockito.doReturn(Optional.of(registeredUserEntity)).when(registeredUserRepository).findByEmail(EMAIL);
			staticMockFiles.when(() -> Files.copy(any(), any())).thenThrow(IOException.class);

			// WHEN-THEN
			assertThrows(FileSystemException.class, () -> {
				testee.makeJavaFunction(lambdaFunctionDTO, TOKEN);
			});
		}
	}

	@Test
	@DisplayName("Make c# function happy path")
	void shouldCreateCSFunctionWhenPassedParametersAreOk() {
		try (MockedStatic<Paths> staticMockPaths = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedConstruction<FileWriter> fileWriterMock = Mockito.mockConstruction(FileWriter.class)) {

			// GIVEN
			Mockito.doReturn(EMAIL).when(jwtUtil).extractUsername(TOKEN);
			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(ZIP_EXTENSION);
			Mockito.doReturn(Optional.of(registeredUserEntity)).when(registeredUserRepository).findByEmail(EMAIL);
			staticMockFiles.when(() -> Files.readAllBytes(any())).thenReturn(dataBytes);
			Mockito.doReturn(lambdaFunctionEntityTest).when(lambdaFunctionDTOMapper).toEntity(lambdaFunctionDTO);

			// WHEN
			Mockito.doReturn(lambdaFunctionEntityTest).when(lambdaFunctionRepository).save(lambdaFunctionEntityTest);

			// THEN
			testee.makeImageFromDll(lambdaFunctionDTO, TOKEN);
		}
	}

	@Test
	@DisplayName("Make c# function with wrong file provided")
	void shouldNotCreateCSFunctionWhenWrongTypeOfFileIsProvided() {
		try (MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class)) {

			// GIVEN
			Mockito.doReturn(EMAIL).when(jwtUtil).extractUsername(TOKEN);
			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(WRONG_EXTENSION);

			// WHEN-THEN
			assertThrows(InvalidArgumentException.class, () -> {
				testee.makeImageFromDll(lambdaFunctionDTO, TOKEN);
			});
		}
	}

	@Test
	@DisplayName("Make c# function. Error while creating function 1")
	void shouldNotCreateCSFunctionWhenLambdaFunctionDestinationPathIsNotValid() {
		try (MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);) {

			// GIVEN
			Mockito.doReturn(EMAIL).when(jwtUtil).extractUsername(TOKEN);
			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(ZIP_EXTENSION);
			Mockito.doReturn(Optional.of(registeredUserEntity)).when(registeredUserRepository).findByEmail(EMAIL);
			staticMockFiles.when(() -> Files.createDirectories(any())).thenThrow(IOException.class);

			// WHEN-THEN
			assertThrows(FileSystemException.class, () -> {
				testee.makeImageFromDll(lambdaFunctionDTO, TOKEN);
			});
		}
	}

	@Test
	@DisplayName("Make c# function. Error while creating function 2")
	void shouldNotCreateCSFunctionWhenExeFilePathIsNotValid() {
		try (MockedStatic<FilenameUtils> fileNameutilsMock = Mockito.mockStatic(FilenameUtils.class);
				MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);) {

			// GIVEN
			Mockito.doReturn(EMAIL).when(jwtUtil).extractUsername(TOKEN);
			fileNameutilsMock.when(() -> FilenameUtils.getExtension(any())).thenReturn(ZIP_EXTENSION);
			Mockito.doReturn(Optional.of(registeredUserEntity)).when(registeredUserRepository).findByEmail(EMAIL);
			staticMockFiles.when(() -> Files.copy(any(), any())).thenThrow(IOException.class);

			// WHEN-THEN
			assertThrows(FileSystemException.class, () -> {
				testee.makeImageFromDll(lambdaFunctionDTO, TOKEN);
			});
		}
	}
}
