package com.htec.naves.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import com.htec.naves.LambdaServiceApplication;
import com.htec.naves.dto.ResponseEntityDTO;
import com.htec.naves.entity.LambdaFunctionEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.enums.Trigger;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.PermissionException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.repository.LambdaFunctionRepository;
import com.htec.naves.util.JwtUtil;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = LambdaServiceApplication.class)
public class ExecutionServiceTest {

	private static final String NAME = "TEST";
	private static final String JSON = "{\"TEST\":\"TEST\"}";
	private static final RegisteredUserEntity USER_ENTITY = new RegisteredUserEntity();
	private static final Map<String, String[]> PARAMS = Collections.singletonMap(NAME, Arrays.array(NAME));
	private static final MockMultipartFile MOCK_FILE = new MockMultipartFile("user-file", "test.txt", null,
			"test data".getBytes());
	private static final Map<String, MultipartFile> FILE_MAP = Collections.singletonMap(NAME, MOCK_FILE);

	private static final ResponseEntityDTO EXPECTED_RESPONSE = new ResponseEntityDTO();

	@Mock
	private JwtUtil jwtUtil;

	@Mock
	private LambdaFunctionRepository lambdaFunctionRepository;

	@Mock
	private FunctionExecutionStatisticsService statisticsService;

	@InjectMocks
	private ExecutionService testee;

	@BeforeAll
	static void init() {
		EXPECTED_RESPONSE.setStatus(502);
		EXPECTED_RESPONSE.setHeaders(new LinkedMultiValueMap<String, String>());
	}

	@Test
	@DisplayName("Invalid function link - runContainerFromImage")
	void shouldThrowResourceNotFoundExceptionWhenFunctionLinkIsInvalid() {
		// Given
		Mockito.doReturn(Optional.empty()).when(lambdaFunctionRepository).findByFunctionLink(NAME);

		// When - Then
		assertThrows(ResourceNotFoundException.class, () -> {
			testee.runContainerFromImage(NAME, NAME, null, PARAMS, null, NAME, Trigger.GET);
		});
	}

	@Test
	@DisplayName("Function is not public - runContainerFromImage")
	void shouldThrowPermissionExceptionWhenUserDoesntHavePermission() {
		// Given
		LambdaFunctionEntity functionEntity = getFunctionEntityWithAccess(false);
		Mockito.doReturn(Optional.of(functionEntity)).when(lambdaFunctionRepository).findByFunctionLink(NAME);

		// When - Then
		assertThrows(PermissionException.class, () -> {
			testee.runContainerFromImage(NAME, null, null, PARAMS, null, NAME, Trigger.GET);
		});

	}

	@Test
	@DisplayName("Container already exists - runContainerFromImage")
	void shouldThrowResourceAlreadyExistsExceptionWhenContainerWithTheSameNameExists() {
		try (MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);) {
			// Given
			LambdaFunctionEntity functionEntity = getFunctionEntityWithAccess(true);
			Mockito.doReturn(Optional.of(functionEntity)).when(lambdaFunctionRepository).findByFunctionLink(NAME);
			staticMockFiles.when(() -> Files.exists(any(Path.class))).thenReturn(true);

			// When - Then
			assertThrows(ResourceAlreadyExistsException.class, () -> {
				testee.runContainerFromImage(NAME, null, null, PARAMS, null, NAME, Trigger.GET);
			});
		}
	}

	@Test
	@DisplayName("File system exception - runContainerFromImage")
	void shouldThrowFileSystemExceptionWhenFilesCantBeCopied() {
		try (MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedConstruction<FileWriter> fileWriterConstructorMock = Mockito
						.mockConstruction(FileWriter.class);) {
			// Given
			LambdaFunctionEntity functionEntity = getFunctionEntityWithAccess(true);
			Mockito.doReturn(Optional.of(functionEntity)).when(lambdaFunctionRepository).findByFunctionLink(NAME);
			staticMockFiles.when(() -> Files.exists(any(Path.class))).thenReturn(false);
			staticMockFiles.when(() -> Files.copy(any(InputStream.class), any(Path.class)))
					.thenThrow(IOException.class);

			// When - Then
			assertThrows(FileSystemException.class, () -> {
				testee.runContainerFromImage(NAME, null, "multipart/form-data", PARAMS, FILE_MAP, null, Trigger.GET);
			});
		}
	}

	@Test
	@DisplayName("Happy path multipart/form-data - runContainerFromImage")
	void shouldRunFunctionWhenParametersAreOkInFormDataFormat() {
		try (MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedConstruction<FileWriter> fileWriterConstructorMock = Mockito
						.mockConstruction(FileWriter.class);) {
			// Given
			LambdaFunctionEntity functionEntity = getFunctionEntityWithAccess(true);
			Mockito.doReturn(Optional.of(functionEntity)).when(lambdaFunctionRepository).findByFunctionLink(NAME);
			staticMockFiles.when(() -> Files.exists(any(Path.class))).thenReturn(false);

			// When
			ResponseEntityDTO actualResponse = testee.runContainerFromImage(NAME, null, "multipart/form-data", PARAMS,
					FILE_MAP, null, Trigger.GET);

			// Then
			assertEquals(EXPECTED_RESPONSE, actualResponse);
		}
	}

	@Test
	@DisplayName("Happy path other - runContainerFromImage")
	void shouldRunFunctionWhenParametersAreOkInOtherFormats() {
		try (MockedStatic<Files> staticMockFiles = Mockito.mockStatic(Files.class);
				MockedConstruction<FileWriter> fileWriterConstructorMock = Mockito
						.mockConstruction(FileWriter.class);) {
			// Given
			LambdaFunctionEntity functionEntity = getFunctionEntityWithAccess(true);
			Mockito.doReturn(Optional.of(functionEntity)).when(lambdaFunctionRepository).findByFunctionLink(NAME);
			staticMockFiles.when(() -> Files.exists(any(Path.class))).thenReturn(false);

			// When
			ResponseEntityDTO actualResponse = testee.runContainerFromImage(NAME, null, null, PARAMS, null, JSON,
					Trigger.GET);

			// Then
			assertEquals(EXPECTED_RESPONSE, actualResponse);
		}
	}

	private LambdaFunctionEntity getFunctionEntityWithAccess(Boolean publicAccess) {
		return new LambdaFunctionEntity(1l, NAME, NAME, publicAccess, Trigger.GET, "", "", USER_ENTITY,
				Collections.emptySet(), Collections.emptyList());
	}

}
