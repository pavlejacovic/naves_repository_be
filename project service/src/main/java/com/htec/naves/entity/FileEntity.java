package com.htec.naves.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "file")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileEntity {

	@Id
	@Column(name = "file_path")
	private String filePath;

	@Column(name = "file_size")
	private Long fileSize;

	@Column(name = "uploaded_by")
	private String uploadedBy;

	@Column(name = "file_name")
	private String fileName;

	@Column(name = "created_date")
	@CreatedDate
	private LocalDateTime createdDate = LocalDateTime.now();

	@ManyToOne
	@JoinColumn(name = "parent_project")
	private ProjectEntity parentProject;

}
