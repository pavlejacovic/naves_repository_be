package com.htec.naves.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "registered_user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisteredUserEntity {

	@Id
	private String email;
	private Long type;

}
