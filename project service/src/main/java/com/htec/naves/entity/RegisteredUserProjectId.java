package com.htec.naves.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisteredUserProjectId implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "project_id")
	private Long projectId;
	@Column(name = "registered_user")
	private String registeredUser;

}
