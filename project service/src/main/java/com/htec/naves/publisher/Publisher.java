package com.htec.naves.publisher;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.naves.entity.ProjectEntity;

/**
 * Class containing method that sends information about verified user and
 * triggers to all of the subscriber classes in different projects.
 * 
 * @author Pavle Jacovic
 * @author Mia Pecelj
 * @author Jovan Popovic
 * 
 */
@Service
public class Publisher {

	@Autowired
	RabbitTemplate rabbitTemplate;

	/**
	 * Sends serializable data to the Trigger queue using direct exchange principle
	 * 
	 * @param triggerData When user does the action that triggers a lambda, trigger
	 *                    data in a form of hash map is being sent to queues of
	 *                    other services
	 * @throws IOException
	 */
	public void sendTriggerData(HashMap<String, Object> triggerData) throws IOException {

		ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
		ObjectOutput objectOutStream = new ObjectOutputStream(byteOutStream);
		objectOutStream.writeObject(triggerData);
		objectOutStream.flush();
		objectOutStream.close();

		byte[] byteMessage = byteOutStream.toByteArray();
		byteOutStream.close();

		Message message = MessageBuilder.withBody(byteMessage).build();

		rabbitTemplate.convertAndSend("Direct-Exchange", "trigger", message);
	}

	/**
	 * Sends serializable data to the Project queue using direct exchange principle
	 * 
	 * @param project_id When user deletes a project, project id is being sent to
	 *                   queues of other services
	 * @throws IOException
	 */
	public void sendDeleteProject(Long project_id) throws IOException {

		ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
		ObjectOutput objectOutStream = new ObjectOutputStream(byteOutStream);
		objectOutStream.writeObject(project_id);
		objectOutStream.flush();
		objectOutStream.close();

		byte[] byteMessage = byteOutStream.toByteArray();
		byteOutStream.close();

		Message message = MessageBuilder.withBody(byteMessage).build();

		rabbitTemplate.convertAndSend("Direct-Exchange", "project", message);
	}

	/**
	 * This function send serializable data to the created-project queue using
	 * direct exchange principle
	 * 
	 * @param project Contains information about project which is sent to the queues
	 *                of other services so it can be saved in their databases
	 * @throws IOException
	 */
	public void createProject(ProjectEntity project) throws IOException {

		ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
		ObjectOutput objectOutStream = new ObjectOutputStream(byteOutStream);
		HashMap<String, Object> projectData = new HashMap<String, Object>();
		projectData.put("projectId", project.getId());
		projectData.put("ownerEmail", project.getOwner().getEmail());
		projectData.put("projectName", project.getProjectName());
		objectOutStream.writeObject(projectData);
		objectOutStream.flush();
		objectOutStream.close();

		byte[] byteMessage = byteOutStream.toByteArray();
		byteOutStream.close();

		Message message = MessageBuilder.withBody(byteMessage).build();

		rabbitTemplate.convertAndSend("Direct-Exchange", "create-project", message);
	}

	public void renameProject(ProjectEntity project) throws IOException {

		ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
		ObjectOutput objectOutStream = new ObjectOutputStream(byteOutStream);
		HashMap<String, Object> projectData = new HashMap<String, Object>();
		projectData.put("projectId", project.getId());
		projectData.put("ownerEmail", project.getOwner().getEmail());
		projectData.put("projectName", project.getProjectName());
		objectOutStream.writeObject(projectData);
		objectOutStream.flush();
		objectOutStream.close();

		byte[] byteMessage = byteOutStream.toByteArray();
		byteOutStream.close();

		Message message = MessageBuilder.withBody(byteMessage).build();

		rabbitTemplate.convertAndSend("Direct-Exchange", "rename-project", message);

	}

}