package com.htec.naves.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.dto.DeleteResponseDTO;
import com.htec.naves.dto.PathDTO;
import com.htec.naves.dto.RenameDTO;
import com.htec.naves.enums.Trigger;
import com.htec.naves.service.FileService;
import com.htec.naves.util.JwtUtil;
import com.htec.naves.util.TriggerEventUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller for file/folder deleting and renaming API calls
 * 
 * 
 * @author Ivan Kukrkic
 *
 */
@RestController
@Slf4j
public class FileController {

	@Autowired
	private FileService fileService;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private TriggerEventUtil triggerEventUtil;

	/**
	 * Attempts to delete the list of given files/folders for the given project if
	 * the user has writing permissions
	 * 
	 * @param projectId      id of the project in which the files/folder is located
	 * @param deleteRequests list of paths to the files/folders to be deleted
	 * @param token          user's token from the authorization header received
	 *                       during login
	 * 
	 * @return HTTP response with a list of messages for each request for the user
	 */
	@DeleteMapping(value = "/projects/{projectId}/files")
	public ResponseEntity<?> deleteFiles(@PathVariable("projectId") Long projectId,
			@RequestBody List<PathDTO> deleteRequests, @RequestHeader(name = "Authorization") String token) {

		List<DeleteResponseDTO> responseMessages = fileService.deleteFiles(deleteRequests, projectId, token);

		if (responseMessages.isEmpty()) {

			log.info("User " + jwtUtil.extractUsername(token) + " sent a blank delete request");

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Can't send blank delete request");
		} else {

			log.info("User " + jwtUtil.extractUsername(token) + " attempted to delete files "
					+ StringUtils.join(deleteRequests, ", ") + "; response: " + responseMessages);

			List<PathDTO> paths = new ArrayList<>();

			for (DeleteResponseDTO message : responseMessages) {
				if (message.isSuccessful()) {
					paths.add(new PathDTO(message.getPath()));
				}
			}

			triggerEventUtil.sendTrigger(paths, projectId, token, Trigger.BUCKET_DELETE_FILE);

			HttpStatus status = HttpStatus.MULTI_STATUS;
			if (paths.size() == responseMessages.size()) {
				status = HttpStatus.OK;
			}
			if (paths.size() == 0) {
				status = HttpStatus.BAD_REQUEST;
			}

			return ResponseEntity.status(status).body(responseMessages);
		}
	}

	/**
	 * Renames a file or folder if the user has writing permissions
	 * 
	 * @param projectId     id of the project in which the files/folder is located
	 * @param renameRequest DTO containing a path to the file/folder to be renamed
	 *                      and a new name
	 * @param auth          user's token from the authorization header received
	 *                      during login
	 * @return HTTP response with a message for the user
	 */
	@PutMapping(value = "/projects/{projectId}/files")
	public ResponseEntity<?> renameFile(@PathVariable("projectId") Long projectId, @RequestBody RenameDTO renameRequest,
			@RequestHeader(name = "Authorization") String auth) {
		fileService.renameFile(renameRequest.getPath(), renameRequest.getNewName(), projectId, auth);

		log.info("User " + jwtUtil.extractUsername(auth) + " renamed " + renameRequest.getPath() + " to "
				+ renameRequest.getNewName());
		return ResponseEntity.status(HttpStatus.OK).body("Successfuly renamed file");
	}

}
