package com.htec.naves.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.dto.PathDTO;
import com.htec.naves.dto.ResourceDTO;
import com.htec.naves.enums.Trigger;
import com.htec.naves.service.FileDownloadService;
import com.htec.naves.util.JwtUtil;
import com.htec.naves.util.TriggerEventUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller for file/folder download API calls
 * 
 * 
 * @author Jovan Popovic
 * @author Ognjen Pejcic
 *
 */
@Slf4j
@RestController
public class FilesDownloadController {

	private final FileDownloadService fileDownloadService;
	private final TriggerEventUtil triggerEventUtil;

	@Value("${upload.path}")
	private String uploadPath;

	@Autowired
	public FilesDownloadController(FileDownloadService fileDownloadService, TriggerEventUtil triggerEventUtil) {
		this.fileDownloadService = fileDownloadService;
		this.triggerEventUtil = triggerEventUtil;
	}

	@Autowired
	private JwtUtil jwtUtil;

	/**
	 * Returns a list of all files and directories in a given directory
	 * 
	 * @param filePath path to the directory
	 * @param token    user's token from the authorization header received during
	 *                 login
	 * @param id       project in which the file is located
	 * @return HTTP response with a status and list of file and directory names or
	 *         an error message
	 */
	@PostMapping("/projects/{projectId}/folders")
	@ResponseBody
	public ResponseEntity<?> getFiles(@RequestBody @Nullable PathDTO filePath,
			@RequestHeader(name = "Authorization") String token, @PathVariable("projectId") Long id) {

		ResourceDTO returnDTO = fileDownloadService.getAllFiles(filePath, id, token);
		log.info("Returning list of files in directory " + filePath.getPath() + " for user "
				+ jwtUtil.extractUsername(token));
		return ResponseEntity.status(HttpStatus.OK).body(returnDTO);

	}

	/**
	 * Downloads a single file
	 * 
	 * @param filePath path to the file being downloaded
	 * @param token    user's token from the authorization header received during
	 *                 login
	 * @param id       project in which the file is located
	 * @return HTTP response with a status and the requested resource or an error
	 *         message
	 */
	@PostMapping("/projects/{projectId}/file")
	@ResponseBody
	public ResponseEntity<?> downloadFile(@RequestBody PathDTO filePath,
			@RequestHeader(name = "Authorization") String token, @PathVariable("projectId") Long id) {

		String email = jwtUtil.extractUsername(token);

		Resource resource = fileDownloadService.loadFileResource(filePath.getPath(), id, token);
		List<PathDTO> paths = new ArrayList<>();
		paths.add(filePath);

		triggerEventUtil.sendTrigger(paths, id, token, Trigger.BUCKET_DOWNLOAD_FILE);

		log.info("User " + email + " downloaded: " + filePath.getPath());

		return ResponseEntity.status(HttpStatus.OK)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);

	}

	/**
	 * Previews a single file
	 * 
	 * @param filePath path to the file to be previewed
	 * @param request  HTTP request
	 * @param token    user's token from the authorization header received during
	 *                 login
	 * @param id       project in which the file is located
	 * @return HTTP response with a status and a preview of the file or an error
	 *         message
	 */
	@PostMapping("/projects/{projectId}/files/preview")
	public ResponseEntity<?> previewFile(@RequestBody PathDTO filePath, HttpServletRequest request,
			@RequestHeader(name = "Authorization") String token, @PathVariable("projectId") Long id) {

		String mail = jwtUtil.extractUsername(token);
		Resource resource = fileDownloadService.loadFileResource(filePath.getPath(), id, token);

		log.info("Previewing file: " + filePath.getPath());

		return ResponseEntity.status(HttpStatus.OK)
				.contentType(MediaType.parseMediaType(fileDownloadService.getMimeType(request.getServletContext()
						.getMimeType(fileDownloadService.getResourceAbsolutePath(resource, mail)))))
				.header(HttpHeaders.CONTENT_DISPOSITION, "inline;fileName=" + resource.getFilename()).body(resource);
	}

	/**
	 * Downloads multiple files as a zip file
	 * 
	 * @param files    list of files to be downloaded
	 * @param response HTTP response
	 * @param token    user's token from the authorization header received during
	 *                 login
	 * @param id       project in which the files are located
	 */
	@PostMapping("/projects/{projectId}/files/zip")
	public void zipDownload(@RequestBody List<PathDTO> files, HttpServletResponse response,
			@RequestHeader(name = "Authorization") String token, @PathVariable("projectId") Long id) {

		fileDownloadService.zip(files, response, token, id);

		response.setStatus(200);
		response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;fileName=zipfile");

		triggerEventUtil.sendTrigger(files, id, token, Trigger.BUCKET_DOWNLOAD_FILE);

		log.info("Zipped files for user " + jwtUtil.extractUsername(token));
	}

}
