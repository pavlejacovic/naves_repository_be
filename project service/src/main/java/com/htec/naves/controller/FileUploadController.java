package com.htec.naves.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.dto.CreateDirectoryDTO;
import com.htec.naves.dto.PathDTO;
import com.htec.naves.dto.ResponseDTO;
import com.htec.naves.dto.UploadDTO;
import com.htec.naves.enums.Trigger;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.exception.PermissionException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.exception.SizeExceededException;
import com.htec.naves.service.FileUploadService;
import com.htec.naves.util.JwtUtil;
import com.htec.naves.util.TriggerEventUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller for file upload and folder creation API calls
 * 
 * @author Uros Stokovic
 *
 */
@RestController
@Slf4j
public class FileUploadController {

	@Autowired
	private FileUploadService fileService;

	@Autowired
	private TriggerEventUtil triggerEventUtil;

	@Autowired
	private JwtUtil jwtUtil;

	/**
	 * Attempts to upload the list of given files for the given project on the given
	 * path if the user has writing permissions
	 * 
	 * @param uploadRequest object that contains list of files that user wants to
	 *                      upload as array of MultipartFile objects and String
	 *                      variable that represents path in which user uploads his
	 *                      files
	 * @param projectId     project to which the files are being uploaded
	 * @param token         user's token from the authorization header received
	 *                      during login
	 * @return HTTP response with a status and a list of messages for each file
	 *         individually
	 * 
	 * @throws InvalidArgumentException  when path is invalid
	 * 
	 * @throws PermissionException       when user does not have permission for this
	 *                                   operation
	 * @throws ResourceNotFoundException when there is no project with specified
	 *                                   projectId or when the directory into which
	 *                                   upload is requested does not exist
	 */
	@PostMapping("upload/{projectId}")
	public ResponseEntity<?> uploadFile(@ModelAttribute UploadDTO uploadRequest,
			@PathVariable("projectId") Long projectId, @RequestHeader(name = "Authorization") String token) {
		List<ResponseDTO> responses = fileService.save(uploadRequest.getFiles(), projectId, token,
				uploadRequest.getPath());
		List<PathDTO> paths = new ArrayList<>();

		for (ResponseDTO response : responses) {
			if (response.isSuccessful()) {
				paths.add(new PathDTO(response.getFile().getFilePath()));
			}
		}

		triggerEventUtil.sendTrigger(paths, projectId, token, Trigger.BUCKET_UPLOAD_FILE);

		HttpStatus status = HttpStatus.MULTI_STATUS;
		if (paths.size() == responses.size()) {
			status = HttpStatus.OK;
		}
		if (paths.size() == 0) {
			status = HttpStatus.BAD_REQUEST;
		}
		return ResponseEntity.status(status).body(responses);
	}

	/**
	 * 
	 * Returns how much space requested project uses and also what is the limit
	 *
	 * @param path      path where files will be uploaded
	 * @param projectId project to which the files are being uploaded
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @return HTTP response with a status and information about currentSize and
	 *         allowedSize for the requested project
	 * 
	 * @throws InvalidArgumentException  when path is invalid or when user has not
	 *                                   specified files to upload
	 * 
	 * @throws SizeExceededException     when user wants to upload more than 10
	 *                                   files in one request
	 * @throws InvalidArgumentException  when at least one file contains / or \
	 *                                   character in its name
	 * @throws PermissionException       when user does not have permission for this
	 *                                   operation
	 * @throws ResourceNotFoundException when there is no project with specified
	 *                                   projectId or when the directory into which
	 *                                   upload is requested does not exist
	 */
	@PostMapping("check/{projectId}")
	public ResponseEntity<?> getAvailableSpace(@RequestBody PathDTO path, @PathVariable("projectId") Long projectId,
			@RequestHeader(name = "Authorization") String token) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(fileService.checkProjectSize(projectId, token, path.getPath()));
	}

	/**
	 * Attempts to create a folder for the given project on the given path if the
	 * user has writing permissions
	 * 
	 * @param projectId project in which the folder is being created
	 * @param path      path to the new folder
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @return ResponseEntity with message that directory is successfully created
	 *         and if no exception was thrown
	 * 
	 * @throws IOException               when there is a problem with file system
	 *                                   when creating a directory
	 * @throws ResourceNotFoundException when there is no project for the specified
	 *                                   projectId
	 * @throws PermissionException       when user does not have permission for this
	 *                                   operation
	 */
	@PostMapping("/directories/{projectId}")
	public ResponseEntity<String> createDirectory(@PathVariable Long projectId,
			@RequestHeader(name = "Authorization") String token, @RequestBody @Valid CreateDirectoryDTO path,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach((error) -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			log.info("User didn't provide valid input for path in POST request /directories");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid path " + errors);
		} else {
			fileService.createDirectory(Long.valueOf(projectId), token, path.getPath(), path.getFolderName());

			log.info("Created directory for user " + jwtUtil.extractUsername(token) + " on path "
					+ jwtUtil.extractUsername(token) + File.separator + projectId + File.separator + path.getPath());
			return ResponseEntity.status(HttpStatus.CREATED).body("Successfully created directory");
		}
	}

}