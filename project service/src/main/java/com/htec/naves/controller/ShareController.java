package com.htec.naves.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.dto.InsertPermissionDTO;
import com.htec.naves.dto.PermissionResponseDTO;
import com.htec.naves.service.ShareService;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller for project sharing API calls
 * 
 * @author Uros Stokovic
 * @author Ivan Kukrkic
 *
 */
@RestController
@Slf4j
@Validated
public class ShareController {

	@Autowired
	private ShareService shareService;

	/**
	 * Changes the access rights of users to a given project
	 * 
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @param projectId id of a project for which the permissions are being changed
	 * @param users     list of DTOs containing the permissions
	 * @return HTTP response with a list of messages for each request for the user
	 */
	@PostMapping("/projects/{projectId}/share")
	public ResponseEntity<?> shareProject(@RequestHeader(name = "Authorization") String token,
			@PathVariable("projectId") Long projectId, @RequestBody @Valid InsertPermissionDTO[] permissions) {
		List<PermissionResponseDTO> responseMessages = shareService.shareProject(projectId, token, permissions);
		int successfulOperations = 0;
		for (PermissionResponseDTO message : responseMessages) {
			if (message.isSuccessful()) {
				successfulOperations++;
			}
		}

		HttpStatus status = HttpStatus.MULTI_STATUS;
		if (successfulOperations == responseMessages.size()) {
			status = HttpStatus.OK;
		}
		if (successfulOperations == 0) {
			status = HttpStatus.BAD_REQUEST;
		}
		return ResponseEntity.status(status).body(responseMessages);
	}

	/**
	 * Returns a list of all permissions for a project
	 * 
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @param projectId id of a project for which the permissions are being returned
	 * @return HTTP response with a status and a message for the client
	 */
	@GetMapping("/projects/{projectId}/share")
	public ResponseEntity<?> getAllPermissions(@RequestHeader(name = "Authorization") String token,
			@PathVariable("projectId") Long projectId) {
		return ResponseEntity.status(HttpStatus.OK).body(shareService.getAllPermissions(token, projectId));

	}

}
