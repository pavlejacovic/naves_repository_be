package com.htec.naves.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.dto.PermissionDTO;
import com.htec.naves.dto.ProjectDashboardDTO;
import com.htec.naves.service.AdminService;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller for admin dashboard
 * 
 * 
 * @author Jovan Popovic
 * @author Mia Pecelj
 *
 */
@RestController
@Slf4j
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private AdminService adminService;

	/**
	 * Return number of all projects.
	 * 
	 * @param token admin token
	 */
	@GetMapping
	public ResponseEntity<Object> getNumberOfProjects(@RequestHeader(name = "Authorization") String token) {
		Long numberOfProjects = adminService.getNumberOfProjects(token);
		return ResponseEntity.status(HttpStatus.OK).body(numberOfProjects);

	}

	/**
	 * Returns information about projects(size, owner, name, shared with).
	 * 
	 * @param token admin token
	 */
	@GetMapping("/projects")
	public ResponseEntity<Object> getProjecstInformation(@RequestHeader(name = "Authorization") String token) {
		List<ProjectDashboardDTO> projects = adminService.getProjectsInformation(token);
		return ResponseEntity.status(HttpStatus.OK).body(projects);

	}

	/**
	 * Returns information about projects(size, owner, name, shared with) by page.
	 * 
	 * @param token admin token
	 * @param page
	 */
	@GetMapping("/projects/page")
	public ResponseEntity<Object> getProjecstInformationPageable(@RequestHeader(name = "Authorization") String token,
			Pageable page) {
		List<ProjectDashboardDTO> projects = adminService.getProjectsInformation(token, page);
		return ResponseEntity.status(HttpStatus.OK).body(projects);

	}

	/**
	 * Returns information about projects(size, owner, name, shared with).
	 * 
	 * @param token admin token
	 */
	@GetMapping("/shared-with/{projectId}")
	public ResponseEntity<Object> getSharedWith(@RequestHeader(name = "Authorization") String token,
			@PathVariable Long projectId) {
		List<PermissionDTO> projects = adminService.getSharedWith(token, projectId);
		return ResponseEntity.status(HttpStatus.OK).body(projects);

	}

}
