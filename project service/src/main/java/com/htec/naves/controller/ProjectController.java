
package com.htec.naves.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.dto.ProjectDTO;
import com.htec.naves.service.ProjectService;
import com.htec.naves.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller for project creating, renaming and deleting API calls
 * 
 * @author Uros Stokovic
 *
 */
@RestController
@Slf4j
public class ProjectController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private JwtUtil jwtUtil;

	/**
	 * Attempts to create a project with the given name for the user
	 * 
	 * @param token         user's token from the authorization header received
	 *                      during login
	 * @param project       DTO containing the name of the project
	 * @param bindingResult format validation
	 * @return HTTP response with a status and a message for the client
	 */
	@PostMapping("/projects")
	public ResponseEntity<?> createProject(@RequestHeader(name = "Authorization") String token,
			@RequestBody @Valid ProjectDTO project, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach((error) -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			log.info("User didn't provide valid input for ProjectDTO in POST request /createProject");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid project name " + errors);
		}
		projectService.createProject(project, token);

		log.info("Created new project with name " + project.getProjectName() + " for user "
				+ jwtUtil.extractUsername(token));
		return ResponseEntity.status(HttpStatus.CREATED)
				.body("Created a project with name " + project.getProjectName());

	}

	/**
	 * Attempts to rename a user's project
	 * 
	 * @param token      user's token from the authorization header received during
	 *                   login
	 * @param project_id id of the project to be renamed
	 * @param new_name   project's new name
	 * @return HTTP response with a status and a message for the client
	 */
	@PutMapping("/projects/{project_id}")
	public ResponseEntity<?> renameProject(@RequestHeader(name = "Authorization") String token,
			@PathVariable("project_id") Long project_id, @RequestBody @Valid ProjectDTO project,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach((error) -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			log.info("User didn't provide valid input for ProjectDTO in POST request /createProject");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid project name " + errors);
		}
		project.setId(project_id);
		projectService.renameProject(project, token);
		log.info("Project with id " + project.getId() + " for user " + jwtUtil.extractUsername(token)
				+ " was renamed to " + project.getProjectName());
		return ResponseEntity.status(HttpStatus.OK).body("Project renamed successfully to " + project.getProjectName());

	}

	/**
	 * Attempts to delete the user's project
	 * 
	 * @param token      user's token from the authorization header received during
	 *                   login
	 * @param project_id id of the project to be deleted
	 * @return HTTP response with a status and a message for the client
	 */
	@DeleteMapping("/projects/{project_id}")
	public ResponseEntity<?> deleteProject(@RequestHeader(name = "Authorization") String token,
			@PathVariable("project_id") Long project_id) {
		projectService.deleteProject(project_id, token);
		log.info("Project with id " + project_id + " for user " + jwtUtil.extractUsername(token)
				+ " was deleted successfully");
		return ResponseEntity.status(HttpStatus.OK).body("Project deleted successfully");
	}

	/**
	 * Finds all projects a user owns
	 * 
	 * @param token user's token from the authorization header received during login
	 * @return HTTP response with a status and a list of DTOs containing project
	 *         information or an error message
	 */
	@GetMapping("/projects")
	public ResponseEntity<?> getMyProjects(@RequestHeader(name = "Authorization") String token) {

		return ResponseEntity.status(HttpStatus.OK).body(projectService.getMyProjects(token));

	}

	/**
	 * Finds all projects shared with the user
	 * 
	 * @param token user's token from the authorization header received during login
	 * @return HTTP response with a status and a list of DTOs containing project
	 *         information or an error message
	 */
	@GetMapping("/projects/shared")
	public ResponseEntity<?> getAllSharedProjects(@RequestHeader(name = "Authorization") String token) {
		return ResponseEntity.status(HttpStatus.OK).body(projectService.getSharedProjects(token));

	}

	/**
	 * Finds all projects a user owns split into pages
	 * 
	 * @param token user's token from the authorization header received during login
	 * @param page  number of the page being returned and number of projects per
	 *              page
	 * @return HTTP response with a status and a list of DTOs containing project
	 *         information or an error message
	 */
	@GetMapping("/projects/page")
	public ResponseEntity<?> getMyProjects(@RequestHeader(name = "Authorization") String token, Pageable page) {
		return ResponseEntity.status(HttpStatus.OK).body(projectService.getMyProjects(token, page));

	}

	/**
	 * Finds all projects shared with the user split into pages
	 * 
	 * @param token user's token from the authorization header received during login
	 * @param page  number of the page being returned and number of projects per
	 *              page
	 * @return HTTP response with a status and a list of DTOs containing project
	 *         information or an error message
	 */
	@GetMapping("/projects/shared/page")
	public ResponseEntity<?> getAllSharedProjects(@RequestHeader(name = "Authorization") String token, Pageable page) {
		return ResponseEntity.status(HttpStatus.OK).body(projectService.getSharedProjects(token, page));
	}

}
