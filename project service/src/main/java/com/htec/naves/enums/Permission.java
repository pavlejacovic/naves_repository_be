package com.htec.naves.enums;

public enum Permission {
	DELETE, READ_ONLY, WRITE
}
