package com.htec.naves.enums;

public enum Trigger {
	GET, POST, BUCKET_DOWNLOAD_FILE, BUCKET_UPLOAD_FILE, BUCKET_DELETE_FILE
}
