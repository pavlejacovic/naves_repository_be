package com.htec.naves.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.naves.constants.UserType;
import com.htec.naves.entity.PermissionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.enums.Operation;
import com.htec.naves.exception.InvalidTokenException;
import com.htec.naves.exception.PermissionException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.repository.PermissionRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.JwtUtil;

/**
 * Service containing methods for operation permission control
 * 
 * @author Ivan Kukrkic
 * @author Uros Stokovic
 *
 */
@Service
public class PermissionService {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private PermissionRepository permissionRepository;

	@Autowired
	private RegisteredUserRepository userRepository;

	@Autowired
	private JwtUtil jwtUtil;

	/**
	 * Checks if the user has the permission for the given project/operation
	 * combination
	 * 
	 * @param operation operation which the user is attempting
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @param projectId project for which the permission is being checked
	 * @throws InvalidTokenException     in case authentication token expired or is
	 *                                   invalid
	 * @throws ResourceNotFoundException in case there is not such project in
	 *                                   database
	 * @throws PermissionException       when user has no permission for such
	 *                                   operation
	 * @return false if user has read_only permission and true if he has write permission
	 */
	public boolean checkPermission(Operation operation, String token, Long projectId) {
		String mail = jwtUtil.extractUsername(token);
		Optional<ProjectEntity> project = projectRepository.findById(projectId);
		if (project.isEmpty()) {
			throw new ResourceNotFoundException(mail, "Project does not exist");
		}

		if (!project.get().getOwner().getEmail().equalsIgnoreCase(mail)) {
			// He is not an owner of project, check if user has permissions in table
			Optional<PermissionEntity> permissionEntity = permissionRepository
					.findByUserAndProject(userRepository.findByEmail(mail).get(), project.get());

			if (!permissionEntity.isPresent()) {
				throw new PermissionException(mail, "You don't have the permission for this operation");
			}

			if (operation == Operation.WRITE) {
				if (!permissionEntity.get().isWritePermission()) {
					throw new PermissionException(mail, "You don't have writing permission");
				}
			}
			return permissionEntity.get().isWritePermission();
		}
		return true;

	};

	/**
	 * Returns the project with the matching id if the user owns the project.
	 * 
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @param projectId id of project for which the permission is being checked
	 * @return ProjectEntity with the same id if the token matches the owner
	 * @throws InvalidTokenException     in case authentication token expired or is
	 *                                   invalid
	 * @throws ResourceNotFoundException if there is no project with specified id
	 * @throws PermissionException       in case the user isn't the owner of the
	 *                                   project
	 * 
	 */
	public ProjectEntity getProjectIfOwner(String token, Long projectId) {
		String mail = jwtUtil.extractUsername(token);
		Optional<ProjectEntity> project = projectRepository.findById(projectId);
		Optional<RegisteredUserEntity> user = userRepository.findByEmail(mail);
		if (project.isEmpty()) {
			throw new ResourceNotFoundException(mail, "Project does not exist");
		}
		if (user.isEmpty()) {
			throw new InvalidTokenException("User doesn't exist!");
		}

		if (!project.get().getOwner().getEmail().equals(mail) && user.get().getType() == UserType.CLIENT) {
			throw new PermissionException(mail, "You are not the owner of this project");
		}
		return project.get();
	}
}
