package com.htec.naves.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import com.htec.naves.dto.FileDTO;
import com.htec.naves.dto.FolderDTO;
import com.htec.naves.dto.PathDTO;
import com.htec.naves.dto.ProjectDTO;
import com.htec.naves.dto.ResourceDTO;
import com.htec.naves.entity.FileEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.enums.Operation;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.mapper.FileEntityDTOMapper;
import com.htec.naves.mapper.ProjectEntityDTOMapper;
import com.htec.naves.repository.FileRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.util.JwtUtil;
import com.htec.naves.util.PathUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Service containing methods for downloading files and folders
 * 
 * @author Jovan Popovic
 * @author Ognjen Pejcic
 *
 */
@Slf4j
@Service
public class FileDownloadService {

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private PathUtil pathUtil;

	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private ProjectRepository projectRepository;

	@Value("${upload.path}")
	private String uploadPath;

	@Autowired
	private FileEntityDTOMapper fileMapper;

	@Autowired
	private ProjectEntityDTOMapper projectMapper;

	/**
	 * Loads a file from a path
	 * 
	 * @param filePath path to the file to be loaded
	 * @param id       project in which the file is located
	 * @param token    user's token from the authorization header received during
	 *                 login
	 * @return targeted file
	 * @throws ResourceNotFoundException in case the file doesn't exist
	 * @throws FileSystemException       in case there was an error reading the file
	 */
	public File loadFile(String filePath, Long id, String token) {
		try {
			Resource resource = loadResource(filePath, id, token);
			return new File(resource.getURI().getPath().toString());
		} catch (IOException e) {
			throw new FileSystemException(jwtUtil.extractUsername(token), "Error reading file " + filePath, e);
		}
	}

	/**
	 * Loads a file or a directory as a resource
	 * 
	 * @param filePath path to the file to be loaded
	 * @param id       project in which the file is located
	 * @param token    user's token from the authorization header received during
	 *                 login
	 * @return targeted file as a resource
	 * @throws ResourceNotFoundException in case the file doesn't exist
	 * @throws FileSystemException       in case there was an error reading the file
	 */
	public Resource loadResource(String filePath, Long id, String token) {
		permissionService.checkPermission(Operation.READ, token, id);
		String ownerEmail = projectRepository.findById(id).get().getOwner().getEmail();
		String email = jwtUtil.extractUsername(token);
		try {
			if (filePath == null) {
				filePath = "";
			}
			Path path = Paths
					.get(uploadPath + File.separator + ownerEmail + File.separator + id + File.separator + filePath)
					.normalize();
			pathUtil.checkPath(token, ownerEmail, id, filePath);
			Resource resource = new UrlResource(path.toUri());

			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new ResourceNotFoundException(email, filePath + " does not exist");
			}
		} catch (IOException e) {
			throw new FileSystemException(email, "Error reading file " + filePath, e);
		}
	}

	/**
	 * Loads a file as a resource
	 * 
	 * @param filePath path to the file to be loaded
	 * @param id       project in which the file is located
	 * @param token    user's token from the authorization header received during
	 *                 login
	 * @return targeted file as a resource
	 * @throws InvalidArgumentException  in case filePath is a directory
	 * @throws ResourceNotFoundException in case the file doesn't exist
	 * @throws FileSystemException       in case there was an error reading the file
	 */
	public Resource loadFileResource(String filePath, Long id, String token) {
		try {
			Resource resource = loadResource(filePath, id, token);
			if (resource.getFile().isDirectory()) {
				throw new InvalidArgumentException(jwtUtil.extractUsername(token), filePath + " is a directory");
			}
			return resource;
		} catch (IOException e) {
			throw new FileSystemException(jwtUtil.extractUsername(token), "Error reading file " + filePath, e);
		}
	}

	/**
	 * Returns a list of paths to all files and folders in a directory
	 * 
	 * @param pathValue path to the folder
	 * @param id        project in which the file is located
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @return list of paths to all files and folders
	 * @throws InvalidArgumentException  in case user provided a path to a directory
	 * @throws FileSystemException       in case there was an error reading the
	 *                                   files and directories
	 * @throws ResourceNotFoundException in case the directory does not exist
	 */
	public ResourceDTO getAllFiles(PathDTO filePath, Long id, String token) {
		String mail = jwtUtil.extractUsername(token);
		File root = loadFile(filePath.getPath(), id, token);

		boolean userPermission = permissionService.checkPermission(Operation.READ, token, id);

		if (!root.isDirectory()) {
			throw new InvalidArgumentException(mail, filePath.getPath() + " is not a directory");
		}

		Path pathValue = root.toPath();
		try (Stream<Path> subPath = Files.walk(pathValue, 1)) {
			List<FolderDTO> folderList = new ArrayList<FolderDTO>();
			List<FileDTO> fileList = new ArrayList<FileDTO>();
			Optional<ProjectEntity> projectEntity = projectRepository.findById(id);
			ProjectDTO project = projectMapper.toDTO(projectEntity.get());

			List<String> subPathList = subPath.map(f -> f.toString()).collect(Collectors.toList());
			subPathList = subPathList.subList(1, (int) subPathList.size());

			for (String path : subPathList) {

				Path temp = Paths.get(path);
				String item = (pathValue.relativize(temp)).toString();
				File directory = new File(uploadPath);
				String repoPath = (Paths.get(directory.getAbsolutePath()).relativize(temp)).toString();

				File file = temp.toFile();

				Path trunc = Paths.get(directory.getAbsolutePath() + File.separator
						+ projectEntity.get().getOwner().getEmail() + File.separator + id);
				String pathForFront = (trunc.relativize(temp)).toString();

				if (file.isDirectory()) {
					FolderDTO folder = new FolderDTO();
					folder.setFolderPath(pathForFront);
					folder.setFolderSize(Files.walk(temp).filter(p -> p.toFile().isFile())
							.mapToLong(p -> p.toFile().length()).sum());
					folder.setFolderName(item);
					folderList.add(folder);
				} else {
					Optional<FileEntity> listFileEntitiy = fileRepository.findByFilePath(repoPath);
					if (!listFileEntitiy.isEmpty()) {
						FileEntity fileEntity = listFileEntitiy.get();
						FileDTO fileDto = fileMapper.toDTO(fileEntity);
						fileDto.setFilePath(pathForFront);
						fileDto.setFileName(item);
						fileList.add(fileDto);
					}
				}
			}
			ResourceDTO resourceDTO = new ResourceDTO(fileList, folderList, project, userPermission);
			return resourceDTO;

		} catch (IOException e) {
			throw new FileSystemException(mail, "Error reading file " + filePath, e);
		}
	}

	/**
	 * Downloads multiple files as a zip
	 * 
	 * @param files    list of files to be downloaded
	 * @param response HTTP response
	 * @param token    user's token from the authorization header received during
	 *                 login
	 * @param id       project in which the files are located
	 * @throws FileSystemException in case there was an error zipping the files
	 */
	public void zip(List<PathDTO> files, HttpServletResponse response, String token, Long id) {
		String mail = jwtUtil.extractUsername(token);
		Optional<ProjectEntity> projectEntity = projectRepository.findById(id);
		if (projectEntity.isEmpty()) {
			throw new ResourceNotFoundException(mail, "No such project");
		}

		permissionService.checkPermission(Operation.READ, token, id);

		String ownerEmail = projectEntity.get().getOwner().getEmail();
		try (ZipOutputStream zos = new ZipOutputStream(response.getOutputStream());
				Stream<PathDTO> pathStream = files.stream();) {
			pathStream.forEach(file -> {

				try {
					Resource resource = loadResource(file.getPath(), id, token);
					log.info("Zipping file on path: " + resource.getURI().getPath().toString());
					File nextFile = new File(resource.getURI().getPath().toString());
					Path parentPath = Paths.get(nextFile.getPath()).normalize();
					pathUtil.checkPath(token, ownerEmail, id, file.getPath());

					if (nextFile.isDirectory()) {
						try (Stream<Path> walkStream = Files.walk(parentPath)) {
							walkStream.filter(path -> !Files.isDirectory(path)).forEach(path -> {
								ZipEntry zipEntry = new ZipEntry(
										Paths.get(parentPath.getParent().toString()).relativize(path).toString());
								try {
									zos.putNextEntry(zipEntry);
									Files.copy(path, zos);
									zos.closeEntry();
								} catch (IOException e) {
									System.err.println(e);
								}
							});
						}
					} else {
						try (InputStream fileInputStream = resource.getInputStream()) {
							ZipEntry zipEntry = new ZipEntry(resource.getFilename());

							zipEntry.setSize(resource.contentLength());
							zos.putNextEntry(zipEntry);
							StreamUtils.copy(fileInputStream, zos);

							zos.closeEntry();
						}
					}

				} catch (IOException e) {
					log.info("Error while zipping files : " + e.getMessage());
				}
			});
			zos.finish();
		} catch (IOException e) {
			throw new FileSystemException(mail, "Error zipping files", e);
		}
	}

	public String getMimeType(String mimeTypeContent) {
		return mimeTypeContent == null ? MediaType.APPLICATION_OCTET_STREAM_VALUE : mimeTypeContent;
	}

	public String getResourceAbsolutePath(Resource resource, String mail) {
		try {
			return resource.getFile().getAbsolutePath();
		} catch (IOException e) {
			throw new FileSystemException(mail, "Error reading file " + resource.getFilename(), e);
		}

	}

}
