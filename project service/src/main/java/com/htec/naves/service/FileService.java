package com.htec.naves.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import com.htec.naves.dto.DeleteResponseDTO;
import com.htec.naves.dto.PathDTO;
import com.htec.naves.entity.FileEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.enums.Operation;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.exception.InvalidTokenException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.repository.FileRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.util.JwtUtil;
import com.htec.naves.util.NameValidator;
import com.htec.naves.util.PathUtil;

/**
 * Service containing methods for deleting and renaming files and folders
 * 
 * 
 * @author Ivan Kukrkic
 *
 */
@Service
public class FileService {

	@Autowired
	private PermissionService permisionService;

	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private PathUtil pathUtil;

	@Autowired
	private NameValidator nameValidator;

	private Path fileStoragePath;

	private String fileStorageLocation;

	public FileService(@Value("${upload.path:temp}") String fileStorageLocation) {

		this.fileStorageLocation = fileStorageLocation;
		fileStoragePath = Paths.get(fileStorageLocation).toAbsolutePath().normalize();

		try {
			Files.createDirectories(fileStoragePath);
		} catch (IOException e) {
			throw new RuntimeException("Issue in creating file directory");
		}
	}

	/**
	 * Deletes a file/folder for a given project if the user has writing permission
	 * for the project.
	 * 
	 * @param fileName  path to the file/folder to be deleted
	 * @param projectId id of the project in which the file/folder is located
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @return list of messages describing the success of each operation
	 */
	public List<DeleteResponseDTO> deleteFiles(List<PathDTO> deleteRequests, Long projectId, String token) {
		permisionService.checkPermission(Operation.WRITE, token, projectId);

		List<DeleteResponseDTO> returnMessage = new ArrayList<DeleteResponseDTO>();

		for (PathDTO fileName : deleteRequests) {
			try {
				deleteSingleFile(fileName, projectId, token);
				returnMessage.add(
						new DeleteResponseDTO(true, "Successfully deleted " + fileName.getPath(), fileName.getPath()));
			} catch (Exception e) {
				returnMessage.add(new DeleteResponseDTO(false, e.getMessage(), fileName.getPath()));
			}
		}

		return returnMessage;

	}

	private void deleteSingleFile(PathDTO fileName, Long projectId, String token) {
		String mail = jwtUtil.extractUsername(token);

		Optional<ProjectEntity> project = projectRepository.findById(projectId);
		if (project.isEmpty()) {
			throw new ResourceNotFoundException(mail, "Project does not exist");
		}

		String ownerMail = project.get().getOwner().getEmail();
		Path filePath = null;
		try {
			filePath = Paths.get(fileStoragePath + File.separator + ownerMail + File.separator + projectId.toString()
					+ File.separator + fileName.getPath()).normalize();
			pathUtil.checkPath(token, ownerMail, projectId, fileName.getPath());
		} catch (Exception e) {
			throw new InvalidArgumentException(mail, "Path is invalid", e);
		}

		if (filePath.equals(
				Paths.get(fileStoragePath + File.separator + ownerMail + File.separator + projectId.toString()))) {
			throw new InvalidArgumentException(mail, "Can't delete root directory");
		}

		if (Files.isDirectory(filePath)) {
			FileSystemUtils.deleteRecursively(filePath.toFile());
			fileRepository.deleteAll(
					fileRepository.findByFilePathStartsWith(pathToRelativeString(filePath) + File.separator));
		} else {
			if (!Files.isRegularFile(filePath)) {
				throw new ResourceNotFoundException(mail, "File doesn't exist");
			}

			try {
				Files.deleteIfExists(filePath);
			} catch (IOException e) {
				throw new FileSystemException(mail, "Error deleting file", e);
			}

			fileRepository.deleteById(pathToRelativeString(filePath));
		}
	}

	/**
	 * Renames a file/folder for a given project if the user has writing permission
	 * for the project.
	 * 
	 * @param fileName  path to the file/folder to be renamed
	 * @param newName   new file/folder name
	 * @param projectId id of the project in which the file/folder is located
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @throws InvalidTokenException          in case authentication token expired
	 *                                        or is invalid
	 * @throws InvalidArgumentException       in case user provided invalid input
	 * @throws ResourceAlreadyExistsException in case user already has a file with
	 *                                        the same name
	 * @throws ResourceNotFoundException      in case the file doesn't exist
	 * @throws FileSystemException            in case there was an error renaming
	 *                                        the project on the file system
	 */
	public void renameFile(String fileName, String newName, Long projectId, String token) {
		String mail = jwtUtil.extractUsername(token);
		try {

			permisionService.checkPermission(Operation.WRITE, token, projectId);

			String ownerMail = projectRepository.findById(projectId).get().getOwner().getEmail();

			nameValidator.checkName(mail, newName);

			Path filePath = Paths.get(fileStoragePath + File.separator + ownerMail + File.separator
					+ projectId.toString() + File.separator + fileName).normalize();

			pathUtil.checkPath(token, ownerMail, projectId, fileName);

			if (filePath.equals(
					Paths.get(fileStoragePath + File.separator + ownerMail + File.separator + projectId.toString())))
				throw new InvalidArgumentException(mail, "Can't rename root directory");

			if (Files.isDirectory(filePath)) {
				renameDirectory(filePath, fileName, newName, projectId);

			} else {
				if (!Files.isRegularFile(filePath)) {
					throw new ResourceNotFoundException(mail, "File " + fileName + " does not exist");
				}
				renameFile(filePath, fileName, newName, mail, projectId);
			}
		} catch (InvalidPathException e) {
			throw new InvalidArgumentException(mail, "Path is invalid", e);
		} catch (FileAlreadyExistsException e) {
			throw new ResourceAlreadyExistsException(mail, "File " + newName + " already exists", e);
		} catch (IOException e) {
			throw new FileSystemException(mail, "Could not rename file " + fileName, e);
		}

	}

	private void renameDirectory(Path filePath, String fileName, String newName, Long projectId) throws IOException {

		Files.move(filePath, filePath.resolveSibling(newName));

		List<FileEntity> files = fileRepository
				.findByFilePathStartsWith(pathToRelativeString(filePath) + File.separator);
		for (FileEntity file : files) {
			fileRepository.delete(file);
			file.setFilePath(StringUtils.replaceOnce(file.getFilePath(), pathToRelativeString(filePath),
					pathToRelativeString(filePath.resolveSibling(newName))));
			fileRepository.save(file);
		}
	}

	private void renameFile(Path filePath, String fileName, String newName, String mail, Long projectId)
			throws IOException {
		if (!FilenameUtils.getExtension(newName).equals(FilenameUtils.getExtension(fileName))) {
			throw new InvalidArgumentException(mail, "Can't change file extension");
		}

		Files.move(filePath, filePath.resolveSibling(newName));

		Optional<FileEntity> file = fileRepository.findById(pathToRelativeString(filePath));
		fileRepository.delete(file.get());
		file.get().setFilePath(pathToRelativeString(filePath.resolveSibling(newName)));
		file.get().setFileName(newName);
		fileRepository.save(file.get());
	}

	private String pathToRelativeString(Path filePath) {
		return fileStoragePath.relativize(filePath).toString();
	}

	public Path getFileStoragePath() {
		return fileStoragePath;
	}

	public void setFileStoragePath(Path fileStoragePath) {
		this.fileStoragePath = fileStoragePath;
	}

	public String getFileStorageLocation() {
		return fileStorageLocation;
	}

	public void setFileStorageLocation(String fileStorageLocation) {
		this.fileStorageLocation = fileStorageLocation;
	}

}
