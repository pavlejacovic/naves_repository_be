package com.htec.naves.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.htec.naves.dto.FileDTO;
import com.htec.naves.dto.ProjectSizeDTO;
import com.htec.naves.dto.ResponseDTO;
import com.htec.naves.entity.FileEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.enums.Operation;
import com.htec.naves.exception.DatabaseException;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.exception.PermissionException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.exception.SizeExceededException;
import com.htec.naves.mapper.FileEntityDTOMapper;
import com.htec.naves.mapper.ProjectEntityDTOMapper;
import com.htec.naves.repository.FileRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.util.JwtUtil;
import com.htec.naves.util.NameValidator;
import com.htec.naves.util.PathUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Service containing methods for uploading files and creating folders
 * 
 * @author Uros Stokovic
 *
 */
@Service
@Slf4j
public class FileUploadService {

	// FOR NOW IT IS ONLY 50MB, BECAUSE OF QA TESTING
	private static final Long MAX_PROJECT_SIZE = 52428800l;

	// IF USER ALREADY HAS FILE WITH THE SAME NAME, WE NEED TO ADD SUFFIX
	private static final Long MAX_INSTANCES = 100l;

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private ProjectEntityDTOMapper projectMapper;

	@Autowired
	private FileEntityDTOMapper fileMapper;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private PathUtil pathUtil;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private NameValidator nameValidator;

	@Value("${upload.path}")
	private String uploadPath;

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	/**
	 * Creates directory where user projects will be placed
	 */
	@PostConstruct
	public void init() {
		try {
			Files.createDirectories(Paths.get(uploadPath));
		} catch (IOException e) {
			log.warn("Could not create upload folder!");
			throw new RuntimeException("Could not create upload folder!");
		}
	}

	/**
	 * Checks if request is valid and if user has permission for this action
	 * 
	 * @param projectId project to which the file is being uploaded
	 * 
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @param path      path to which the files are uploaded in the project
	 * 
	 * @return ProjectEntity object that has information about project for which the
	 *         upload is requested
	 *
	 * @throws InvalidArgumentException  when path is invalid
	 * 
	 * @throws PermissionException       when user does not have permission for this
	 *                                   operation
	 * @throws ResourceNotFoundException when there is no project with specified
	 *                                   projectId or when the directory into which
	 *                                   upload is requested does not exist
	 */
	public ProjectEntity checkValidRequest(Long projectId, String token, String path) {
		permissionService.checkPermission(Operation.WRITE, token, projectId);
		ProjectEntity projectEntity = projectRepository.findById(projectId).get();
		pathUtil.checkPath(token, projectEntity.getOwner().getEmail(), projectId, path);

		Path root = Paths.get(uploadPath + File.separator + projectEntity.getOwner().getEmail() + File.separator
				+ projectEntity.getId() + File.separator + path);
		root = root.normalize();
		if (!Files.exists(root)) {
			log.info("Directory " + path + " does not exist for user " + jwtUtil.extractUsername(token));
			throw new ResourceNotFoundException(jwtUtil.extractUsername(token),
					"Directory " + path + " does not exist");
		}
		return projectEntity;
	}

	/**
	 * Returns how much storage does a project with specified id use and what is the
	 * maximum value for it
	 *
	 * @param projectId project to which the file is being uploaded
	 * 
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @param path      path to which the files are uploaded in the project
	 * 
	 * @return ProjectSizeDTO object that contains current size of the project and
	 *         maximum allowed size
	 * 
	 * @throws InvalidArgumentException  when path is invalid
	 * 
	 * @throws PermissionException       when user does not have permission for this
	 *                                   operation
	 * @throws ResourceNotFoundException when there is no project with specified
	 *                                   projectId or when the directory into which
	 *                                   upload is requested does not exist
	 */
	public ProjectSizeDTO checkProjectSize(Long projectId, String token, String path) {
		ProjectEntity projectEntity = checkValidRequest(projectId, token, path);

		return new ProjectSizeDTO(projectEntity.getProjectSize(), MAX_PROJECT_SIZE);
	}

	/**
	 * Attempts to save the files in the given path for the given project if the
	 * user has writing permissions
	 * 
	 * @param files     files to be uploaded
	 * 
	 * @param projectId project to which the file is being uploaded
	 * 
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @param path      path to which the files are uploaded in the project
	 * 
	 * @return returns list of ResponseDTO that contains list of information about
	 *         uploaded file, status of the requested operation and message
	 * 
	 * @throws InvalidArgumentException  when path is invalid or when user has not
	 *                                   specified files to upload
	 * 
	 * @throws SizeExceededException     when user wants to upload more than 10
	 *                                   files in one request
	 * @throws InvalidArgumentException  when at least one file contains / or \
	 *                                   character in its name
	 * @throws PermissionException       when user does not have permission for this
	 *                                   operation
	 * @throws ResourceNotFoundException when there is no project with specified
	 *                                   projectId or when the directory into which
	 *                                   upload is requested does not exist
	 */
	public List<ResponseDTO> save(MultipartFile[] files, Long projectId, String token, String path) {
		if (files == null || (files != null && files[0].getOriginalFilename().length() == 0)) {
			throw new InvalidArgumentException(jwtUtil.extractUsername(token), "You did not specify files to upload");
		}
		if (files.length > 10) {
			throw new SizeExceededException(jwtUtil.extractUsername(token),
					"Cannot upload more than 10 files in single request");
		}
		ProjectEntity projectEntity = checkValidRequest(projectId, token, path);
		List<ResponseDTO> uploadResponseList = new LinkedList<>();
		for (MultipartFile multipartFile : files) {
			try {
				uploadResponseList
						.add(new ResponseDTO(true, "File upload successful: " + multipartFile.getOriginalFilename(),
								saveSingleFile(multipartFile, projectEntity, path, token)));
			} catch (SizeExceededException e) {
				// No need to free space because current file has not allocated any space
				uploadResponseList.add(new ResponseDTO(false, e.getMessage(), null));
			} catch (Exception e) {
				// Free space allocated by current file
				projectRepository.free(projectId, multipartFile.getSize());
				uploadResponseList.add(new ResponseDTO(false, e.getMessage(), null));
			}
		}
		return uploadResponseList;
	}

	/**
	 * Attempts to save specific file in the given path for the given project if the
	 * user has writing permissions. Adds new entry to uploadResponseList
	 * 
	 * @param multipartFile file to be uploaded
	 *
	 * @param projectEntity project to which the file is being uploaded
	 * 
	 * @param path          path to which the files are uploaded in the project
	 * 
	 * @param token         user's token from the authorization header received
	 *                      during login
	 * 
	 * @return FileDTO that contains file information if it was successfully
	 *         uploaded
	 * 
	 * @throws IOException                    when there is a problem during upload,
	 *                                        uploaded file must be deleted from
	 *                                        file system in order to preserve
	 *                                        consistency
	 * @throws ResourceAlreadyExistsException when there is already a file with the
	 *                                        same name at the same location
	 * @throws DatabaseException              when there is a problem when saving
	 *                                        file information to database
	 * @throws SizeExceededException          when there is not enough space to
	 *                                        upload file
	 * 
	 * @throws ResourceNotFoundException      when directory into which file should
	 *                                        be uploaded does not exist
	 */
	private FileDTO saveSingleFile(MultipartFile multipartFile, ProjectEntity projectEntity, String path, String token)
			throws IOException {
		String email = projectEntity.getOwner().getEmail();

		nameValidator.checkName(email, multipartFile.getOriginalFilename());

		// Reserve space for this file
		if (projectRepository.allocate(projectEntity.getId(), multipartFile.getSize(), MAX_PROJECT_SIZE) != 1) {
			throw new SizeExceededException(jwtUtil.extractUsername(token), "Not enough space");
		}

		Path root = Paths.get(
				uploadPath + File.separator + email + File.separator + projectEntity.getId() + File.separator + path);
		root = root.normalize();
		String filePath = root.resolve(multipartFile.getOriginalFilename()).toString();
		String fileName = multipartFile.getOriginalFilename();
		try {
			Files.copy(multipartFile.getInputStream(), root.resolve(multipartFile.getOriginalFilename()));
		} catch (NoSuchFileException e) {
			throw new ResourceNotFoundException(email, "Directory " + path + " does not exist");
		} catch (Exception e) {
			log.info("File " + multipartFile.getOriginalFilename() + " already exists for user "
					+ jwtUtil.extractUsername(token) + ". We are adding suffix");

			// For now we upload file but add suffix to the name
			int i = 1;
			for (; i < MAX_INSTANCES; i++) {
				try {
					fileName = multipartFile.getOriginalFilename();
					int lastIndexOfDot = fileName.lastIndexOf(".");
					if (lastIndexOfDot == -1) {
						fileName += "(" + i + ")";
					} else {
						fileName = fileName.substring(0, fileName.lastIndexOf(".")) + "(" + i + ")"
								+ fileName.substring(fileName.lastIndexOf("."));
					}
					Files.copy(multipartFile.getInputStream(), root.resolve(fileName));
					filePath = root.resolve(fileName).toString();
					break;
				} catch (NoSuchFileException ex) {
					throw new ResourceNotFoundException(email, "Directory " + path + " does not exist");
				} catch (Exception ex) {
					log.info("Suffix " + i + " for file " + multipartFile.getOriginalFilename() + " is already taken");
				}
			}
			if (i >= MAX_INSTANCES) {
				throw new ResourceAlreadyExistsException(email, "Can not create more instances of the same file");
			}
		}

		try {
			FileDTO fileDTO = new FileDTO();
			fileDTO.setFilePath(filePath.substring(uploadPath.length() + 1));
			fileDTO.setParentProject(projectMapper.toDTO(projectEntity));
			fileDTO.setFileSize(multipartFile.getSize());
			fileDTO.setUploadedBy(email);
			fileDTO.setFileName(fileName);

			FileEntity uploadedFile = fileRepository.save(fileMapper.toEntity(fileDTO));
			log.info("Uploaded file " + multipartFile.getOriginalFilename() + " for user "
					+ jwtUtil.extractUsername(token) + " on path " + email + File.separator + projectEntity.getId()
					+ path);

			Path start = Paths.get(uploadPath + File.separator + email + File.separator + projectEntity.getId())
					.normalize();
			fileDTO = fileMapper.toDTO(uploadedFile);
			fileDTO.setFilePath(start.relativize(root).toString() + File.separator + fileName);
			return fileDTO;
		} catch (Exception e) {
			// Delete file from directory because we couldn't save info to database
			FileSystemUtils.deleteRecursively(root.resolve("" + filePath));
			throw new DatabaseException(jwtUtil.extractUsername(token),
					"Error while trying to save file info to database", e);
		}
	}

	/**
	 * Attempts to create a folder for the given project on the given path if the
	 * user has writing permissions
	 * 
	 * @param projectId project in which the folder is being created
	 * 
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @param path      path to the new folder
	 *
	 * @throws IOException               when there is a problem with file system
	 *                                   when creating a directory
	 * @throws ResourceNotFoundException when there is no project for the specified
	 *                                   projectId
	 * @throws PermissionException       when user does not have permission for this
	 *                                   operation
	 */
	public void createDirectory(Long projectId, String token, String path, String folderName) {
		Optional<ProjectEntity> projectEntity = projectRepository.findById(projectId);
		if (projectEntity.isEmpty()) {
			// Project does not exist
			throw new ResourceNotFoundException(jwtUtil.extractUsername(token),
					"No project for the requested project projectId - " + projectId);
		}
		String email = projectEntity.get().getOwner().getEmail();

		folderName = folderName.trim();
		nameValidator.checkName(email, folderName);

		path = path + File.separator + folderName;

		pathUtil.checkPath(token, email, projectId, path);

		// Check if i have permissions
		permissionService.checkPermission(Operation.WRITE, token, projectId);
		// ---------------------------

		if (Files.exists(
				Paths.get(uploadPath + File.separator + email + File.separator + projectId + File.separator + path))) {
			throw new ResourceAlreadyExistsException(jwtUtil.extractUsername(token),
					"File or a directory with this name already exists.");
		}

		try {
			// Create directory
			Files.createDirectories(Paths
					.get(uploadPath + File.separator + email + File.separator + projectId + File.separator + path));
		} catch (IOException e) {
			throw new FileSystemException(email, "Could not create directory", e);
		}

	}

}
