package com.htec.naves.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import com.htec.naves.dto.PermissionDTO;
import com.htec.naves.dto.ProjectDTO;
import com.htec.naves.entity.PermissionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.enums.Operation;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.InterserviceCommunicationException;
import com.htec.naves.exception.InvalidTokenException;
import com.htec.naves.exception.PermissionException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.mapper.PermissionEntityDTOMapper;
import com.htec.naves.mapper.ProjectEntityDTOMapper;
import com.htec.naves.mapper.RegisteredUserEntityDTOMapper;
import com.htec.naves.publisher.Publisher;
import com.htec.naves.repository.PermissionRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.JwtUtil;

/**
 * Service containing methods for project creating, renaming and deleting
 * 
 * @author Uros Stokovic
 *
 */
@Service
public class ProjectService {

	@Value("${upload.path}")
	private String uploadPath;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private ProjectEntityDTOMapper projectMapper;

	@Autowired
	private RegisteredUserEntityDTOMapper registeredUserMapper;

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private RegisteredUserRepository registeredUserRepository;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private PermissionRepository permissionRepository;

	@Autowired
	private PermissionEntityDTOMapper permissionMapper;

	@Autowired
	private Publisher publisher;

	/**
	 * Attempts to create a project with the given name for the user
	 * 
	 * @param project DTO containing the name of the project
	 * @param token   user's token from the authorization header received during
	 *                login
	 * @throws InvalidTokenException          in case authentication token expired
	 *                                        or is invalid
	 * @throws ResourceAlreadyExistsException in case user already has a project
	 *                                        with the same name
	 * @throws FileSystemException            in case there was an error creating a
	 *                                        directory on the file system
	 * @throws PermissionException            in case user doesn't have permissions
	 *                                        for this operation
	 */
	@Transactional
	public void createProject(ProjectDTO project, String token) {
		// Extract email from token
		String email = jwtUtil.extractUsername(token);

		Optional<RegisteredUserEntity> registeredUser = registeredUserRepository.findByEmail(email);

		// Check if user already has project with same name
		Optional<String> projectFromDatabase = projectRepository.findProjectForUser(email, project.getProjectName());
		if (projectFromDatabase.isPresent()) {
			throw new ResourceAlreadyExistsException(email, "Project " + project.getProjectName() + " already exists");
		}

		project.setNumberOfFiles(Long.valueOf(0));
		project.setProjectSize(Long.valueOf(0));
		project.setOwner(registeredUserMapper.toDTO(registeredUser.get()));

		ProjectEntity pe;
		pe = projectRepository.save(projectMapper.toEntity(project));
		try {
			publisher.createProject(pe);
		} catch (IOException e1) {
			throw new InterserviceCommunicationException(email, "Unable to establish communication with publisher", e1);
		}

		// Create directory
		try {
			Files.createDirectories(Paths.get(uploadPath + File.separator + email + File.separator + pe.getId()));
		} catch (Exception e) {
			// Could not create directory, must delete from database
			projectRepository.deleteById(pe.getId());
			throw new FileSystemException(email, "Could not create project " + project.getProjectName(), e);
		}

	}

	/**
	 * Attempts to rename a user's project
	 * 
	 * @param project id of the project to be renamed
	 * @param token   user's token from the authorization header received during
	 *                login
	 * @throws InvalidTokenException          in case authentication token expired
	 *                                        or is invalid
	 * @throws ResourceAlreadyExistsException in case user already has a project
	 *                                        with the same name
	 * @throws PermissionException            in case user doesn't have permissions
	 *                                        for this operation
	 */
	public void renameProject(ProjectDTO project, String token) {
		// Extract email from token
		String email = jwtUtil.extractUsername(token);

		// Check permissions
		permissionService.checkPermission(Operation.WRITE, token, project.getId());

		// Get email of project owner
		ProjectEntity projectEntity = projectRepository.findById(project.getId()).get();
		String ownerEmail = projectEntity.getOwner().getEmail();

		// Check if user already has project with same name
		Optional<String> projectFromDatabase = projectRepository.findProjectForUser(ownerEmail,
				project.getProjectName());
		if (projectFromDatabase.isPresent()) {
			throw new ResourceAlreadyExistsException(email, "Project " + project.getProjectName() + " already exists");
		}

		// Change name
		projectEntity.setProjectName(project.getProjectName());

		projectRepository.save(projectEntity);
		projectEntity.setProjectName(project.getProjectName());
		try {
			publisher.renameProject(projectEntity);
		} catch (IOException ee) {
			throw new InterserviceCommunicationException(email, "Unable to establish communication with publisher", ee);
		}

	}

	/**
	 * Attempts to delete a user's project
	 * 
	 * @param project_id id of the project to be deleted
	 * @param token      user's token from the authorization header received during
	 *                   login
	 * @throws InvalidTokenException     in case authentication token expired or is
	 *                                   invalid
	 * @throws ResourceNotFoundException in case project with the given id was not
	 *                                   found in the database
	 * @throws FileSystemException       in case there was an error deleting the
	 *                                   project off the file system
	 * @throws PermissionException       in case user doesn't have permissions for
	 *                                   this operation
	 */
	@Transactional
	public void deleteProject(Long project_id, String token) {
		String email = jwtUtil.extractUsername(token);
		// Check permissions
		permissionService.checkPermission(Operation.WRITE, token, project_id);

		// Delete from database
		ProjectEntity existingProject = projectRepository.findById(project_id).get();

		List<PermissionEntity> existingPermissions = permissionRepository.findByProject(existingProject);
		permissionRepository.deleteAll(existingPermissions);
		projectRepository.deleteById(existingProject.getId());

		// Delete from fileSystem
		try {
			FileSystemUtils.deleteRecursively(Paths.get(uploadPath + File.separator
					+ existingProject.getOwner().getEmail() + File.separator + existingProject.getId()));
		} catch (IOException e) {
			throw new FileSystemException(email, "Could not delete project " + existingProject.getProjectName(), e);
		}

		try {
			publisher.sendDeleteProject(project_id);
		} catch (IOException e) {
			throw new InterserviceCommunicationException(email, "Unable to establish communication with publisher", e);
		}
	}

	/**
	 * Finds all projects a user owns
	 * 
	 * @param token user's token from the authorization header received during login
	 * @return list of user's projects
	 * @throws InvalidTokenException in case authentication token expired or is
	 *                               invalid
	 */
	public List<ProjectDTO> getMyProjects(String token) {

		String email = jwtUtil.extractUsername(token);
		RegisteredUserEntity existingUser = registeredUserRepository.findByEmail(email).get();

		// Get all projects where this email is owner
		return projectRepository.findByOwner(existingUser).stream().map(element -> {
			ProjectDTO project = projectMapper.toDTO(element);
			return project;
		}).collect(Collectors.toList());

	}

	/**
	 * Finds all projects shared with the user
	 * 
	 * @param token user's token from the authorization header received during login
	 * @return list of projects shared with the user
	 * @throws InvalidTokenException in case authentication token expired or is
	 *                               invalid
	 */
	public List<PermissionDTO> getSharedProjects(String token) {
		String email = jwtUtil.extractUsername(token);
		Optional<RegisteredUserEntity> user = registeredUserRepository.findByEmail(email);
		if (user.isEmpty()) {
			throw new InvalidTokenException("User does not exist.");
		}

		// Get all projects for this user where he is not owner but has read permission
		return permissionRepository.findByUser(user.get()).stream().map(element -> permissionMapper.toDTO(element))
				.collect(Collectors.toList());
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	/**
	 * Finds all projects a user owns split into pages
	 * 
	 * @param token user's token from the authorization header received during login
	 * @param page  number of the page being returned and number of projects per
	 *              page
	 * @return list of user's projects on the requested page
	 * @throws InvalidTokenException in case authentication token expired or is
	 *                               invalid
	 */
	public Page<ProjectDTO> getMyProjects(String token, Pageable page) {
		String email = jwtUtil.extractUsername(token);
		Optional<RegisteredUserEntity> existingUser = registeredUserRepository.findByEmail(email);
		if (existingUser.isEmpty()) {
			throw new InvalidTokenException("User does not exist.");
		}

		// Get all projects where this email is owner
		List<ProjectDTO> list = projectRepository.findAllByOwner(existingUser.get(), page).stream().map(element -> {
			ProjectDTO project = projectMapper.toDTO(element);
			return project;
		}).collect(Collectors.toList());
		return new PageImpl<ProjectDTO>(list);

	}

	/**
	 * Finds all projects shared with the user split into pages
	 * 
	 * @param token user's token from the authorization header received during login
	 * @param page  number of the page being returned and number of projects per
	 *              page
	 * @return list of projects shared with the user on the requested page
	 * @throws InvalidTokenException in case authentication token expired or is
	 *                               invalid
	 */
	public Page<PermissionDTO> getSharedProjects(String token, Pageable page) {
		String email = jwtUtil.extractUsername(token);
		Optional<RegisteredUserEntity> existingUser = registeredUserRepository.findByEmail(email);
		if (existingUser.isEmpty()) {
			throw new InvalidTokenException("User does not exist.");
		}
		// Get all projects for this user where he is not owner but has read permission
		List<PermissionDTO> list = permissionRepository.findByUser(existingUser.get(), page).stream()
				.map(element -> permissionMapper.toDTO(element)).collect(Collectors.toList());

		return new PageImpl<PermissionDTO>(list);
	}

}
