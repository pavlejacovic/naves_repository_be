package com.htec.naves.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.naves.constants.UserType;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.exception.ForbiddenException;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.JwtUtil;

@Service
public class AdminPermissionService {

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private RegisteredUserRepository registeredUserRepository;

	/**
	 * Checks if the admin is present and if it really is adminEntity.
	 * 
	 * @param email email extracted from the token
	 * @param admin adminentity
	 */
	public void checkAdminPermission(String token) {
		String email = jwtUtil.extractUsername(token);
		Optional<RegisteredUserEntity> admin = registeredUserRepository.findByEmail(email);
		if (admin.isEmpty() || admin.get().getType() != UserType.ADMIN) {
			throw new ForbiddenException("User is not allowed to access this information. ", email);
		}
	}
}
