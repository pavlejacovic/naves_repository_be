package com.htec.naves.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.StreamUtils;

/**
 * 
 * Currently not in use, possibly needed
 *
 */
public class ZippingService {

	public static void pack(List<String> paths, String zipFilePath) throws IOException {

		Path p = Files.createFile(Paths.get(zipFilePath));
		try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
			for (String sourceDirPath : paths) {
				Path pp = Paths.get(sourceDirPath);
				File file = new File(pp.toString());
				if (file.isDirectory()) {
					Files.walk(pp).filter(path -> !Files.isDirectory(path)).forEach(path -> {
						ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
						try {
							zs.putNextEntry(zipEntry);
							Files.copy(path, zs);
							zs.closeEntry();
						} catch (IOException e) {
							System.err.println(e);
						}
					});
				} else {

					ZipEntry zipEntry = new ZipEntry(pp.getFileName().toString());
					try {
						zs.putNextEntry(zipEntry);

						Resource resource = new UrlResource(pp.toUri());

						StreamUtils.copy(resource.getInputStream(), zs);
//						Files.copy(pp, zs);
						zs.closeEntry();
					} catch (IOException e) {
						System.err.println(e);
					}
				}
			}

		}
	}
}
