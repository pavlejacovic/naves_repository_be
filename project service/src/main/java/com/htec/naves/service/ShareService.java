package com.htec.naves.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.naves.dto.InsertPermissionDTO;
import com.htec.naves.dto.PermissionDTO;
import com.htec.naves.dto.PermissionResponseDTO;
import com.htec.naves.entity.PermissionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.enums.Permission;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.mapper.PermissionEntityDTOMapper;
import com.htec.naves.repository.PermissionRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.JwtUtil;

/**
 * Service containing methods for project sharing
 * 
 * @author Uros Stokovic
 * @author Ivan Kukrkic
 *
 */
@Service
public class ShareService {

	@Autowired
	private RegisteredUserRepository registeredUserRepository;

	@Autowired
	private PermissionRepository permissionRepository;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private PermissionEntityDTOMapper permissionMapper;

	@Autowired
	private JwtUtil jwtUtil;

	/**
	 * Changes the access rights of a user to a given project
	 * 
	 * @param projectId id of project for which the permissions are being modified
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @param user      DTO containing user email and read and write permissions
	 * @return list of messages describing the success of each operation
	 */
	public List<PermissionResponseDTO> shareProject(Long projectId, String token, InsertPermissionDTO[] permissions) {
		String mail = jwtUtil.extractUsername(token);
		ProjectEntity project = permissionService.getProjectIfOwner(token, projectId);

		List<PermissionResponseDTO> response = new LinkedList<>();

		for (InsertPermissionDTO permission : permissions) {
			try {
				addPermission(permission, project, mail);
				response.add(new PermissionResponseDTO(true, "Modified permissions for user " + permission.getEmail()));
			} catch (Exception e) {
				response.add(new PermissionResponseDTO(false, e.getMessage()));
			}
		}
		return response;
	}

	private void addPermission(InsertPermissionDTO user, ProjectEntity project, String mail) {
		user.setEmail(user.getEmail().trim());
		Optional<RegisteredUserEntity> checkedUser = registeredUserRepository.findById(user.getEmail());
		if (checkedUser.isEmpty()) {
			throw new ResourceNotFoundException(mail, "User " + user.getEmail() + " does not exist");
		}

		if (mail.equals(user.getEmail())) {
			throw new ResourceAlreadyExistsException(mail,
					"User " + user.getEmail() + " is already an owner for this project");
		}

		Optional<PermissionEntity> permissionEntity = permissionRepository.findByUserAndProject(checkedUser.get(),
				project);
		if (permissionEntity.isPresent()) {
			if (user.getPermission() == Permission.DELETE) {
				permissionRepository.deleteById(permissionEntity.get().getId());
			} else {
				permissionEntity.get().setWritePermission(user.getPermission() == Permission.READ_ONLY ? false : true);
				permissionRepository.save(permissionEntity.get());
			}
		} else {
			if (user.getPermission() == Permission.DELETE) {
				throw new InvalidArgumentException(mail,
						"User " + user.getEmail() + " did not have permissions for this project");
			}

			PermissionEntity permission = new PermissionEntity();
			permission.setUser(checkedUser.get());
			permission.setProject(project);
			permission.setWritePermission(user.getPermission() == Permission.READ_ONLY ? false : true);
			permissionRepository.save(permission);
		}

	}

	/**
	 * Returns a list of all permissions for a project
	 * 
	 * @param token     user's token from the authorization header received during
	 *                  login
	 * @param projectId id of a project for which the permissions are being returned
	 * @return list of DTOs containing the permissions
	 */
	public List<PermissionDTO> getAllPermissions(String token, Long projectId) {
		permissionService.getProjectIfOwner(token, projectId);
		Optional<ProjectEntity> project = projectRepository.findById(projectId);
		if (project.isEmpty()) {
			throw new ResourceNotFoundException(jwtUtil.extractUsername(token), "Project does not exist");
		}
		return permissionRepository.findByProject(project.get()).stream()
				.map(element -> permissionMapper.toDTO(element)).collect(Collectors.toList());

	}

}
