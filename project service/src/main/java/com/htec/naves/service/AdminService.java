package com.htec.naves.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.htec.naves.dto.PermissionDTO;
import com.htec.naves.dto.ProjectDashboardDTO;
import com.htec.naves.entity.PermissionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.mapper.PermissionEntityDTOMapper;
import com.htec.naves.mapper.ProjectDashboarProjectDTOMapper;
import com.htec.naves.repository.PermissionRepository;
import com.htec.naves.repository.ProjectRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Service containing methods for admin.
 * 
 * @author Jovan Popovic
 * @author Mia Pecelj
 * 
 *
 */
@Service
@Transactional
@Slf4j
@Configuration
public class AdminService {
	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private PermissionRepository permissionRepository;
	@Autowired
	private PermissionEntityDTOMapper permissionMapper;
	@Autowired
	private ProjectDashboarProjectDTOMapper dashboardMapper;
	@Autowired
	private AdminPermissionService adminPermissionService;

	/**
	 * Returns number of all projects in our system.
	 * 
	 * @param token user token
	 */
	public Long getNumberOfProjects(String token) {
		adminPermissionService.checkAdminPermission(token);
		return projectRepository.count();
	}

	/**
	 * Returns information about all projects(owner, shared with, size...)
	 * 
	 * @param token user token
	 */
	public List<ProjectDashboardDTO> getProjectsInformation(String token) {
		adminPermissionService.checkAdminPermission(token);
		List<ProjectEntity> projects = projectRepository.findAll();
		List<ProjectDashboardDTO> dashboardProject = mapProjects(projects);
		return dashboardProject;
	}

	/**
	 * Returns information about all projects(owner, shared with, size...) by page.
	 * 
	 * @param token user token
	 * @param page  contains info about number and size of page.
	 */
	public List<ProjectDashboardDTO> getProjectsInformation(String token, Pageable page) {
		adminPermissionService.checkAdminPermission(token);
		Page<ProjectEntity> projects = projectRepository.findAll(page);
		List<ProjectDashboardDTO> dashboardProject = mapProjects(projects.getContent());
		return dashboardProject;
	}

	/**
	 * Returns a list of ProjectDashboardDTOs mapped from a list of ProjectEntitys.
	 * 
	 * @param projects list of ProjectEntitys
	 */
	private List<ProjectDashboardDTO> mapProjects(List<ProjectEntity> projects) {
		return projects.stream().map(dashboardMapper::toDashboardDTO).collect(Collectors.toList());
	}

	public List<PermissionDTO> getSharedWith(String token, Long projectId) {
		adminPermissionService.checkAdminPermission(token);
		List<PermissionEntity> permissions = permissionRepository.findByProjectId(projectId);
		List<PermissionDTO> pemssionDtos = permissions.stream().map(permissionMapper::toDTO)
				.collect(Collectors.toList());
		return pemssionDtos;
	}

}
