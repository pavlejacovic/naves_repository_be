package com.htec.naves.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTO {

	private Long id;

	private RegisteredUserDTO owner;

	@NotNull
	@NotBlank
	@Size(max = 255)
	private String projectName;

	private Long numberOfFiles;

	private Long projectSize;

}
