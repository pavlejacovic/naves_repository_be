package com.htec.naves.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileDTO {

	private String filePath;

	private Long fileSize;

	private ProjectDTO parentProject;

	private String uploadedBy;

	private String fileName;

	private LocalDateTime createdDate = LocalDateTime.now();

}
