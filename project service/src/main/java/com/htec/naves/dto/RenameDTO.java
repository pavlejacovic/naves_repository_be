package com.htec.naves.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RenameDTO {
	@NotNull
	private String path;
	@NotNull
	private String newName;
}
