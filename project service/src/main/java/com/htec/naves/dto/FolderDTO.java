package com.htec.naves.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FolderDTO {

	private String folderPath;
	private Long folderSize;
	private String folderName;

}
