package com.htec.naves.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDashboardDTO {

	private ProjectDTO project;
	private Long numberOfShares;
}
