package com.htec.naves.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.htec.naves.enums.Permission;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InsertPermissionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotBlank
	private String email;

	@NotNull
	private Permission permission;

}
