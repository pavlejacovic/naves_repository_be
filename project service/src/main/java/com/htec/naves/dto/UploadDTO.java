package com.htec.naves.dto;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UploadDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private MultipartFile[] files;
	private String path;

}
