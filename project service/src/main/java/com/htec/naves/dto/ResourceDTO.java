package com.htec.naves.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceDTO {

	private List<FileDTO> files;
	private List<FolderDTO> folders;
	private ProjectDTO project;
	private boolean writePermission;
}
