package com.htec.naves.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeleteResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private boolean successful;
	private String message;
	private String path;
}
