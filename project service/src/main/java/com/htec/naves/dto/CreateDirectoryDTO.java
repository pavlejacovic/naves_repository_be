package com.htec.naves.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateDirectoryDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	@NotBlank
	private String path;
	
	@NotBlank
	private String folderName;
	
}
