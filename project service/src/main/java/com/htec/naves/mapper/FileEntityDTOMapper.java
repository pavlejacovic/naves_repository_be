package com.htec.naves.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import com.htec.naves.dto.FileDTO;
import com.htec.naves.entity.FileEntity;

@Mapper(componentModel = "spring", uses = {
		ProjectEntityDTOMapper.class }, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface FileEntityDTOMapper {
	
	FileDTO toDTO(FileEntity entity);

	FileEntity toEntity(FileDTO file);
}
