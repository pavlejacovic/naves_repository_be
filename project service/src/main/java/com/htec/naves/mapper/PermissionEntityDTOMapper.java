package com.htec.naves.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import com.htec.naves.dto.PermissionDTO;
import com.htec.naves.entity.PermissionEntity;

@Mapper(componentModel = "spring", uses = { RegisteredUserEntityDTOMapper.class,
		ProjectEntityDTOMapper.class }, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PermissionEntityDTOMapper {

	PermissionDTO toDTO(PermissionEntity entity);

	PermissionEntity toEntity(PermissionDTO project);

}
