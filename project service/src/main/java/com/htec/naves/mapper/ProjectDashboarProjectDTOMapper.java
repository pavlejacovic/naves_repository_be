package com.htec.naves.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htec.naves.dto.ProjectDashboardDTO;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.repository.PermissionRepository;

@Component
public class ProjectDashboarProjectDTOMapper {

	@Autowired
	ProjectEntityDTOMapper projectEntityDTOMapper;

	@Autowired
	PermissionRepository permissionRepository;

	public ProjectDashboardDTO toDashboardDTO(ProjectEntity entity) {

		ProjectDashboardDTO dto = new ProjectDashboardDTO();
		dto.setNumberOfShares(Long.valueOf(permissionRepository.findByProject(entity).size()));
		dto.setProject(projectEntityDTOMapper.toDTO(entity));

		return dto;
	}
}
