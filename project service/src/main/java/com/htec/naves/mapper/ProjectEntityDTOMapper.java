package com.htec.naves.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import com.htec.naves.dto.ProjectDTO;
import com.htec.naves.entity.ProjectEntity;

@Mapper(componentModel = "spring", uses = {
		RegisteredUserEntityDTOMapper.class }, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ProjectEntityDTOMapper {

	ProjectDTO toDTO(ProjectEntity entity);

	ProjectEntity toEntity(ProjectDTO project);

}
