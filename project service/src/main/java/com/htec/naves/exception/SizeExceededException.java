package com.htec.naves.exception;

import lombok.Getter;

/**
 * Thrown when the user attempts to upload a file which exceeds current size
 * limit.
 * 
 * @author Ivan Kukrkic
 *
 */
@Getter
public class SizeExceededException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String user;

	public SizeExceededException(String user, String message) {
		super(message);
		this.user = user;
	}

	public SizeExceededException(String user, String message, Throwable cause) {
		super(message, cause);
		this.user = user;
	}
}
