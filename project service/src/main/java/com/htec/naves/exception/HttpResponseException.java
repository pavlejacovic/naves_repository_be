package com.htec.naves.exception;

import org.springframework.http.HttpStatus;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class HttpResponseException extends Exception {

	private static final long serialVersionUID = 1L;
	private HttpStatus httpStatus;
	private String message;

}