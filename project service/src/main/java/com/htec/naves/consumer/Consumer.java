package com.htec.naves.consumer;

import java.io.ByteArrayInputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.HashMap;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.repository.RegisteredUserRepository;

/**
 * Class that is used to catch messages via @RabbitListener from publisher
 * (queues = "Verified-users") -> queue name from RabbitMq dashboard
 * 
 * 
 * @author Pavle Jacovic
 * 
 */

@Service
public class Consumer {

	@Autowired
	private RegisteredUserRepository registeredUserRepository;

	/**
	 * Every time publisher sends the message, the class that is annotated
	 * with @RabbitListener catches those messages and deserializes them so data can
	 * be accessible and written in database like here if needed
	 * 
	 * @param message array of bytes received from the publisher
	 */
	@RabbitListener(queues = "Verified-users")
	public void getMessage(byte[] message) {
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(message);
			ObjectInput in = new ObjectInputStream(bis);
			HashMap<String, String> data = (HashMap<String, String>) in.readObject();
			in.close();
			bis.close();

			registeredUserRepository
					.save(new RegisteredUserEntity(data.get("email"), Long.valueOf(data.get("type").toString())));

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}

}
