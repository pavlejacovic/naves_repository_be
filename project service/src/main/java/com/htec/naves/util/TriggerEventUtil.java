package com.htec.naves.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.naves.dto.PathDTO;
import com.htec.naves.enums.Trigger;
import com.htec.naves.exception.InterserviceCommunicationException;
import com.htec.naves.publisher.Publisher;

/**
 * Class containing method that organizes the data that needs to be send and
 * calls publishers method for sending data
 * 
 * @author Mia Pecelj
 * @author Jovan Popovic
 * 
 */
@Service
public class TriggerEventUtil {
	@Autowired
	Publisher publisher;
	@Autowired
	JwtUtil jwtUtil;

	/**
	 * Sends data for triggers in lambda service in a form of HashMap.
	 * 
	 * @param params    paths to the files affected by the action that is used as a
	 *                  trigger
	 * @param projectId id of a project in which the action was performed
	 * @param email     email of the user
	 * @param action    action which was performed (eg. update, delete, upload)
	 */
	public void sendTrigger(List<PathDTO> params, Long projectId, String token, Trigger action) {
		HashMap<String, Object> data = new HashMap<>();
		data.put("params", params);
		data.put("projectId", projectId);
		data.put("token", token);
		data.put("action", action);
		String email = jwtUtil.extractUsername(token);
		try {
			publisher.sendTriggerData(data);
		} catch (IOException e) {
			throw new InterserviceCommunicationException(email, "Unable to execute lambda", e);
		}

	}

}
