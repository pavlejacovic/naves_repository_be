package com.htec.naves.util;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.htec.naves.exception.InvalidArgumentException;

@Service
public class PathUtil {

	// Same value as the variable in application.properties
	@Value("${upload.path}")
	private String uploadPath;

	@Autowired
	private JwtUtil jwtUtil;

	public void checkPath(String token, String owner, Long id, String path) {
		String mail = jwtUtil.extractUsername(token);
		try {
			Path wanted = Paths.get(uploadPath + File.separator + owner + File.separator + id
					+ File.separator + path).normalize();
			Path regular = Paths.get(uploadPath + File.separator + owner + File.separator + id)
					.normalize();
			if (!wanted.startsWith(regular))
				throw new InvalidArgumentException(mail, "Path is invalid");
		} catch (InvalidPathException e) {
			throw new InvalidArgumentException(mail, "Path is invalid", e);
		}
	}

}
