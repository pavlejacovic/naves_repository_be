package com.htec.naves.util;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.htec.naves.exception.InvalidArgumentException;

/**
 * Utility service that checks if name for file or folder is valid
 * 
 * @author Uros
 *
 */
@Service
public class NameValidator {
	public static final List<String> INVALID_RESOURCE_CHARACTERS = Stream
			.of("\\", "/", ":", "*", "?", "\"", "<", ">", "|").collect(Collectors.toList());

	public static final List<String> INVALID_RESOURCE_BASENAMES = Stream
			.of("aux", "com1", "com2", "com3", "com4", "com5", "com6", "com7", "com8", "com9", "con", "lpt1", "lpt2",
					"lpt3", "lpt4", "lpt5", "lpt6", "lpt7", "lpt8", "lpt9", "nul", "prn")
			.collect(Collectors.toList());

	/**
	 * Checks if given name is valid for file or folder
	 * 
	 * @param email email of user that requested operation
	 * 
	 * @param name  name that is being validated
	 */
	public void checkName(String email, String name) {
		if (name == null || name.trim().length() == 0) {
			throw new InvalidArgumentException(email, "Invalid name");
		}

		boolean matched = false;

		matched = INVALID_RESOURCE_BASENAMES.stream().anyMatch(s -> name.toLowerCase().equals(s));
		if (matched) {
			throw new InvalidArgumentException(email, "Invalid name");
		}
		matched = INVALID_RESOURCE_CHARACTERS.stream().anyMatch(s -> name.contains(s));
		if (matched) {
			throw new InvalidArgumentException(email, "Invalid name");
		}
		if (name.toLowerCase().matches("^\\.+$")) {
			throw new InvalidArgumentException(email, "Invalid name");
		}
	}

}
