package com.htec.naves.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.naves.entity.FileEntity;
import com.htec.naves.entity.ProjectEntity;

@Repository
@Transactional
public interface FileRepository extends JpaRepository<FileEntity, String> {

	public void findByParentProject(ProjectEntity parentProject);

	List<FileEntity> findByFilePathStartsWith(String filePath);

	Optional<FileEntity> findByFilePath(String filePath);

}
