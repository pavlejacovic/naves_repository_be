package com.htec.naves.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;

@Repository
@Transactional
public interface ProjectRepository extends JpaRepository<ProjectEntity, Long> {

	@Query(value = "SELECT project_name FROM project WHERE owner_email=?1 AND project_name=?2", nativeQuery = true)
	public Optional<String> findProjectForUser(String userEmail, String projectName);

	public List<ProjectEntity> findByOwner(RegisteredUserEntity owner);

	public List<ProjectEntity> findAllByOwner(RegisteredUserEntity owner, Pageable page);

	Optional<ProjectEntity> findById(Long id);

	@Modifying(flushAutomatically = true)
	@Query(value = "UPDATE project SET project_size=project_size-?2, number_of_files=number_of_files-1 WHERE id=?1", nativeQuery = true)
	public void free(Long projectId, Long size);

	@Modifying(flushAutomatically = true)
	@Query(value = "UPDATE project\r\n"
			+ "    SET project_size=project_size + ?2, number_of_files=number_of_files+1\r\n"
			+ "    WHERE id = ?1 AND project_size + ?2 <= ?3", nativeQuery = true)
	public int allocate(Long projectId, Long size, Long maxProjectSize);

}