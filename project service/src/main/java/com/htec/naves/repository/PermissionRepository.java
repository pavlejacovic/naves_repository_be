package com.htec.naves.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.naves.entity.PermissionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;

@Repository
public interface PermissionRepository extends JpaRepository<PermissionEntity, Long> {

	public void deleteById(Long id);

	public void deleteByProject(ProjectEntity project);

	public void deleteAll(Iterable<? extends PermissionEntity> entities);

	List<PermissionEntity> findByProject(ProjectEntity project);

	List<PermissionEntity> findByProjectId(Long projectId);

	List<PermissionEntity> findByUser(RegisteredUserEntity user);

	List<PermissionEntity> findByUser(RegisteredUserEntity user, Pageable pageable);

	public Optional<PermissionEntity> findByUserAndProject(RegisteredUserEntity user, ProjectEntity project);

}
