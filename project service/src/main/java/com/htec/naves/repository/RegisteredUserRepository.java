package com.htec.naves.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.naves.entity.RegisteredUserEntity;

@Repository
public interface RegisteredUserRepository extends JpaRepository<RegisteredUserEntity, String> {
	Optional<RegisteredUserEntity> findByEmail(String email);
}
