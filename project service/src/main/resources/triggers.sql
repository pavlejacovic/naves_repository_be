DROP TRIGGER IF EXISTS after_file_delete;

CREATE TRIGGER after_file_delete
AFTER DELETE 
ON file
FOR EACH ROW 
 UPDATE project
 SET number_of_files=number_of_files-1,
	project_size=project_size-OLD.file_size
WHERE project.id=OLD.parent_project;

DROP TRIGGER IF EXISTS after_file_update;

CREATE TRIGGER after_file_update
AFTER UPDATE
ON file
FOR EACH ROW 
 UPDATE project
 SET project_size=project_size - OLD.file_size + NEW.file_size
WHERE project.id=OLD.parent_project;
