package com.htec.naves.service;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import com.htec.naves.ProjectServiceApplication;
import com.htec.naves.dto.FileDTO;
import com.htec.naves.dto.ProjectSizeDTO;
import com.htec.naves.dto.ResponseDTO;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.HttpResponseException;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.exception.PermissionException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.exception.SizeExceededException;
import com.htec.naves.mapper.FileEntityDTOMapper;
import com.htec.naves.mapper.ProjectEntityDTOMapper;
import com.htec.naves.repository.FileRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.util.JwtUtil;
import com.htec.naves.util.NameValidator;
import com.htec.naves.util.PathUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectServiceApplication.class)
public class FileUploadServiceTest {

	class MultipartFileImpl implements MultipartFile {
		private final byte[] imgContent;

		private String name;

		public MultipartFileImpl(byte[] imgContent) {
			this.imgContent = imgContent;
		}

		public void setOriginalFilename(String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public String getOriginalFilename() {
			return name;
		}

		@Override
		public String getContentType() {
			return null;
		}

		@Override
		public boolean isEmpty() {
			return imgContent == null || imgContent.length == 0;
		}

		@Override
		public long getSize() {
			return imgContent.length;
		}

		@Override
		public byte[] getBytes() throws IOException {
			return imgContent;
		}

		@Override
		public InputStream getInputStream() throws IOException {
			return new ByteArrayInputStream(imgContent);
		}

		@Override
		public void transferTo(File dest) throws IOException, IllegalStateException {
			new FileOutputStream(dest).write(imgContent);
		}
	}

	@Value("${upload.path}")
	private String uploadPath;

	// FOR NOW IT IS ONLY 50MB, BECAUSE OF QA TESTING
	private static final long MAX_PROJECT_SIZE = 52428800l;

	// MAX NUMBER OF FILES THAT CAN BE UPLOADED IN A SINGLE REQUEST
	private static final int MAX_FILES = 10;

	@InjectMocks
	private FileUploadService testee;

	@Mock
	private ProjectRepository projectRepository;

	@Mock
	private FileRepository fileRepository;

	@Mock
	private ProjectEntityDTOMapper projectMapper;

	@Mock
	private FileEntityDTOMapper fileMapper;

	@Mock
	private PermissionService permissionService;

	@Mock
	private JwtUtil jwtUtil;

	@Mock
	private PathUtil pathUtil;

	@Mock
	private NameValidator nameValidator;

	private final String test = "TEST";
	private final String testTXT = "TEST.txt";

	private ProjectEntity projectEntity;
	private RegisteredUserEntity registeredUser;
	private MultipartFileImpl multipartFile;
	private String email = "tempEmail@gmail.com";
	private FileDTO fileDTO;

	private Path root;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);

		projectEntity = new ProjectEntity();

		registeredUser = new RegisteredUserEntity();
		registeredUser.setEmail(email);
		projectEntity.setOwner(registeredUser);

		byte[] data = new byte[] { 1, 2, 3, 4 };
		multipartFile = new MultipartFileImpl(data);
		multipartFile.setOriginalFilename(test);
		root = Paths.get(uploadPath + File.separator + email + File.separator + 5l + File.separator + "");
		fileDTO = new FileDTO();
	}

	@Test
	public void checkValidRequestSuccess() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {
			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
			filesMock.when(() -> Files.exists(any())).thenReturn(true);
			assertEquals(projectEntity, testee.checkValidRequest(null, null, null));
		}
	}

	@Test
	public void checkValidRequestInvalidPath() {
		when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
		Mockito.doThrow(InvalidArgumentException.class).when(pathUtil).checkPath(any(), any(), any(), any());
		assertThrows(InvalidArgumentException.class, () -> {
			testee.checkValidRequest(null, null, null);
		});
	}

	@Test
	public void checkValidRequestNoPermission() {
		Mockito.doThrow(PermissionException.class).when(permissionService).checkPermission(any(), any(), any());
		assertThrows(PermissionException.class, () -> {
			testee.checkValidRequest(null, null, null);
		});
	}

	@Test
	public void checkValidRequestNoDirectory() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {
			filesMock.when(() -> Files.exists(any())).thenReturn(false);
			when(jwtUtil.extractUsername(any())).thenReturn(test);
			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
			assertThrows(ResourceNotFoundException.class, () -> {
				testee.checkValidRequest(null, null, test);
			});
		}
	}

	@Test
	public void checkProjectSizeTest() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {
			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
			filesMock.when(() -> Files.exists(any())).thenReturn(true);
			assertEquals(new ProjectSizeDTO(projectEntity.getProjectSize(), MAX_PROJECT_SIZE),
					testee.checkProjectSize(null, null, null));
		}
	}

	@Test
	public void saveInvalidArgument() {
		assertThrows(InvalidArgumentException.class, () -> {
			testee.save(null, null, null, null);
		});
	}

	@Test
	public void saveNumberOfFilesExceeded() {
		MultipartFile[] inputArray = new MultipartFile[MAX_FILES + 1];
		for (int i = 0; i < MAX_FILES + 1; i++)
			inputArray[i] = multipartFile;

		assertThrows(SizeExceededException.class, () -> {
			testee.save(inputArray, null, null, null);
		});
	}

	@Test
	public void saveFilenameContainsSlashes() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {
			filesMock.when(() -> Files.exists(any())).thenReturn(true);
			multipartFile.setOriginalFilename("Dummy \\file");
			Mockito.doThrow(new InvalidArgumentException(null,"Invalid name")).when(nameValidator).checkName(any(), any());
			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
			List<ResponseDTO> uploadResponseList = new LinkedList<>();
			uploadResponseList.add(new ResponseDTO(false, "Invalid name", null));
			assertEquals(uploadResponseList, testee.save(new MultipartFile[] { multipartFile }, null, null, null));
		}
	}

	@Test
	public void saveNoSpace() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {
			filesMock.when(() -> Files.exists(any())).thenReturn(true);
			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));

			List<ResponseDTO> uploadResponseList = new LinkedList<>();
			uploadResponseList.add(new ResponseDTO(false, "Not enough space", null));
			assertEquals(uploadResponseList, testee.save(new MultipartFile[] { multipartFile }, null, null, null));
		}
	}

	@Test
	public void saveNoDirectory() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {
			filesMock.when(() -> Files.exists(any())).thenReturn(true);
			filesMock.when(() -> Files.copy(any(InputStream.class), any(Path.class)))
					.thenThrow(NoSuchFileException.class);
			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
			when(projectRepository.allocate(any(), any(), any())).thenReturn(1);

			List<ResponseDTO> uploadResponseList = new LinkedList<>();
			uploadResponseList.add(new ResponseDTO(false, "Directory " + test + " does not exist", null));

			assertEquals(uploadResponseList, testee.save(new MultipartFile[] { multipartFile }, null, null, test));
		}
	}

	@Test
	public void saveFileAlreadyExists() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {
			filesMock.when(() -> Files.exists(any())).thenReturn(true);
			filesMock.when(() -> Files.copy(any(InputStream.class), any(Path.class))).thenThrow(RuntimeException.class)
					.thenThrow(NoSuchFileException.class);
			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
			when(projectRepository.allocate(any(), any(), any())).thenReturn(1);

			List<ResponseDTO> uploadResponseList = new LinkedList<>();
			uploadResponseList.add(new ResponseDTO(false, "Directory " + test + " does not exist", null));

			assertEquals(uploadResponseList, testee.save(new MultipartFile[] { multipartFile }, null, null, test));
		}
	}

	@Test
	public void saveFileAlreadyExistsNotEnoughSuffixes() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);
				MockedStatic<Paths> pathsMock = Mockito.mockStatic(Paths.class);) {
			filesMock.when(() -> Files.exists(any())).thenReturn(true);
			filesMock.when(() -> Files.copy(any(InputStream.class), any(Path.class))).thenThrow(RuntimeException.class)
					.thenThrow(RuntimeException.class);

			testee.setUploadPath(uploadPath);

			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
			when(projectRepository.allocate(any(), any(), any())).thenReturn(1);
			pathsMock.when(() -> Paths.get(any(String.class))).thenReturn(root);

			List<ResponseDTO> uploadResponseList = new LinkedList<>();
			uploadResponseList.add(new ResponseDTO(false, "Can not create more instances of the same file", null));
			assertEquals(uploadResponseList, testee.save(new MultipartFile[] { multipartFile }, null, null, null));
		}
	}

	@Test
	public void saveFileAlreadyExistsSuffixPresentSuccessfulSave() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);
				MockedStatic<Paths> pathsMock = Mockito.mockStatic(Paths.class);) {
			filesMock.when(() -> Files.exists(any())).thenReturn(true);
			filesMock.when(() -> Files.copy(any(InputStream.class), any(Path.class))).thenThrow(RuntimeException.class)
					.thenThrow(RuntimeException.class).thenReturn(1l);

			multipartFile.setOriginalFilename(testTXT);
			testee.setUploadPath(uploadPath);

			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
			when(projectRepository.allocate(any(), any(), any())).thenReturn(1);
			pathsMock.when(() -> Paths.get(any(String.class))).thenReturn(root);
			when(fileMapper.toDTO(any())).thenReturn(fileDTO);

			List<ResponseDTO> uploadResponseList = new LinkedList<>();
			uploadResponseList.add(
					new ResponseDTO(true, "File upload successful: " + multipartFile.getOriginalFilename(), fileDTO));
			assertEquals(uploadResponseList, testee.save(new MultipartFile[] { multipartFile }, null, null, null));
		}
	}

	@Test
	public void saveFileNotPresentSuccessfulSave() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);
				MockedStatic<Paths> pathsMock = Mockito.mockStatic(Paths.class);) {
			filesMock.when(() -> Files.exists(any())).thenReturn(true);

			testee.setUploadPath(uploadPath);

			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
			when(projectRepository.allocate(any(), any(), any())).thenReturn(1);
			pathsMock.when(() -> Paths.get(any(String.class))).thenReturn(root);

			when(fileMapper.toDTO(any())).thenReturn(fileDTO);

			List<ResponseDTO> uploadResponseList = new LinkedList<>();
			uploadResponseList.add(
					new ResponseDTO(true, "File upload successful: " + multipartFile.getOriginalFilename(), fileDTO));
			assertEquals(uploadResponseList, testee.save(new MultipartFile[] { multipartFile }, null, null, null));
		}
	}

	@Test
	public void saveFileAlreadyExistsSuffixPresentErrorWithDatabase() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);
				MockedStatic<Paths> pathsMock = Mockito.mockStatic(Paths.class);) {
			filesMock.when(() -> Files.exists(any())).thenReturn(true);
			filesMock.when(() -> Files.copy(any(InputStream.class), any(Path.class))).thenThrow(RuntimeException.class)
					.thenThrow(RuntimeException.class).thenReturn(1l);

			testee.setUploadPath(uploadPath);

			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
			when(projectRepository.allocate(any(), any(), any())).thenReturn(1);
			pathsMock.when(() -> Paths.get(any(String.class))).thenReturn(root);
			when(fileRepository.save(any())).thenThrow(RuntimeException.class);

			List<ResponseDTO> uploadResponseList = new LinkedList<>();
			uploadResponseList.add(new ResponseDTO(false, "Error while trying to save file info to database", null));
			assertEquals(uploadResponseList, testee.save(new MultipartFile[] { multipartFile }, null, null, null));
		}
	}

	@Test
	public void createDirectoryInvalidPath() {
		when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
		Mockito.doThrow(InvalidArgumentException.class).when(pathUtil).checkPath(any(), any(), any(), any());
		assertThrows(InvalidArgumentException.class, () -> {
			testee.createDirectory(null, null, null, "");
		});
	}

	@Test
	public void createDirectoryNoProject() {
		when(projectRepository.findById(any())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> {
			testee.createDirectory(null, null, null, null);
		});
	}

	@Test
	public void createDirectoryNoPermissions() throws HttpResponseException {
		when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
		Mockito.doThrow(PermissionException.class).when(permissionService).checkPermission(any(), any(), any());
		assertThrows(PermissionException.class, () -> {
			testee.createDirectory(null, null, null, "");
		});
	}

	@Test
	public void createDirectoryFolderExists() {
		try (MockedStatic<Paths> pathsMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));

			pathsMock.when(() -> Paths.get(any(String.class))).thenReturn(root);
			filesMock.when(() -> Files.exists(any())).thenReturn(true);

			assertThrows(ResourceAlreadyExistsException.class, () -> {
				testee.createDirectory(null, null, null, "");
			});
		}
	}

	@Test
	public void getUploadPathTest() {
		assertNull(testee.getUploadPath());
	}

	@Test
	public void createDirectorySuccessfullyCreated() throws IOException {
		try (MockedStatic<Paths> pathsMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));

			pathsMock.when(() -> Paths.get(any(String.class))).thenReturn(root);
			filesMock.when(() -> Files.isDirectory(any())).thenReturn(false);

			testee.createDirectory(null, null, null, "");
		}
	}

	@Test
	public void createDirectoryError() throws IOException {
		try (MockedStatic<Paths> pathsMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));

			pathsMock.when(() -> Paths.get(any(String.class))).thenReturn(root);
			filesMock.when(() -> Files.isDirectory(any())).thenReturn(false);
			filesMock.when(() -> Files.createDirectories(any(Path.class)))
					.thenThrow(java.nio.file.FileSystemException.class);

			assertThrows(FileSystemException.class, () -> {
				testee.createDirectory(null, null, null, "");
			});
		}
	}

	@Test
	public void createDirectoryNameInvalid() throws IOException {
		try (MockedStatic<Paths> pathsMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));

			pathsMock.when(() -> Paths.get(any(String.class))).thenReturn(root);
			filesMock.when(() -> Files.isDirectory(any())).thenReturn(false);
			filesMock.when(() -> Files.createDirectories(any(Path.class)))
					.thenThrow(java.nio.file.FileSystemException.class);
			Mockito.doThrow(InvalidArgumentException.class).when(nameValidator).checkName(any(), any());

			assertThrows(InvalidArgumentException.class, () -> {
				testee.createDirectory(null, null, null, "temp/temp2");
			});
		}
	}
}