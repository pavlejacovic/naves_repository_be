package com.htec.naves.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htec.naves.ProjectServiceApplication;
import com.htec.naves.dto.InsertPermissionDTO;
import com.htec.naves.dto.PermissionDTO;
import com.htec.naves.dto.PermissionResponseDTO;
import com.htec.naves.dto.ProjectDTO;
import com.htec.naves.dto.RegisteredUserDTO;
import com.htec.naves.entity.PermissionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.enums.Permission;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.mapper.PermissionEntityDTOMapper;
import com.htec.naves.mapper.ProjectEntityDTOMapper;
import com.htec.naves.mapper.RegisteredUserEntityDTOMapper;
import com.htec.naves.repository.PermissionRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.JwtUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectServiceApplication.class)
public class ShareServiceTest {

	@InjectMocks
	private ShareService testee;

	@Mock
	private RegisteredUserRepository registeredUserRepository;

	@Mock
	private PermissionRepository permissionRepository;

	@Mock
	private PermissionService permissionService;

	@Mock
	private PermissionEntityDTOMapper permissionMapper;

	@Mock
	private RegisteredUserEntityDTOMapper userMapper;

	@Mock
	private ProjectEntityDTOMapper projectMapper;

	@Mock
	private ProjectRepository projectRepository;

	@Mock
	private JwtUtil jwtUtil;

	private static final String TEST_MAIL = "testEmail@gmail.com";
	private static final String OTHER_MAIL = "otherEmail@gmail.com";

	private ProjectEntity project;

	private InsertPermissionDTO permissionRequest;

	private RegisteredUserEntity regUser;

	private List<PermissionResponseDTO> response;

	private PermissionEntity permissionEntity;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);

		when(jwtUtil.extractUsername(any())).thenReturn(OTHER_MAIL);

		project = new ProjectEntity();
		regUser = new RegisteredUserEntity();
		regUser.setEmail(TEST_MAIL);
		project.setOwner(regUser);

		when(permissionService.getProjectIfOwner(any(), any())).thenReturn(project);

		permissionRequest = new InsertPermissionDTO();
		permissionRequest.setEmail(TEST_MAIL);

		response = new LinkedList<>();

		when(registeredUserRepository.findById(any())).thenReturn(Optional.of(regUser));
		when(projectRepository.findById(any())).thenReturn(Optional.of(project));

		permissionEntity = new PermissionEntity();

		when(permissionRepository.findByUserAndProject(any(), any())).thenReturn(Optional.of(permissionEntity));

		when(permissionRepository.findById(any())).thenReturn(Optional.of(permissionEntity));
	}

	@Test
	public void shareNoProject() {
		when(jwtUtil.extractUsername(any())).thenReturn(TEST_MAIL);
		when(registeredUserRepository.findById(any())).thenReturn(Optional.empty());
		response.add(new PermissionResponseDTO(false, "User " + permissionRequest.getEmail() + " does not exist"));
		assertEquals(response, testee.shareProject(null, null, ArrayUtils.toArray(permissionRequest)));
	}

	@Test
	public void shareUserIsOwner() {
		when(jwtUtil.extractUsername(any())).thenReturn(TEST_MAIL);
		response.add(new PermissionResponseDTO(false,
				"User " + regUser.getEmail() + " is already an owner for this project"));
		assertEquals(response, testee.shareProject(project.getId(), "", ArrayUtils.toArray(permissionRequest)));
	}

	@Test
	public void shareNoUserAndDeleteRequested() {
		when(permissionRepository.findByUserAndProject(any(), any())).thenReturn(Optional.empty());
		when(permissionRepository.findById(any())).thenReturn(Optional.empty());
		response.add(new PermissionResponseDTO(false,
				"User " + regUser.getEmail() + " did not have permissions for this project"));
		permissionRequest.setPermission(Permission.DELETE);
		assertEquals(response, testee.shareProject(project.getId(), "", ArrayUtils.toArray(permissionRequest)));
	}

	@Test
	public void shareNoUserAndInsertRequested() {
		when(permissionRepository.findByUserAndProject(any(), any())).thenReturn(Optional.empty());
		permissionRequest.setPermission(Permission.WRITE);
		when(permissionRepository.findById(any())).thenReturn(Optional.empty());
		response.add(new PermissionResponseDTO(true, "Modified permissions for user " + regUser.getEmail()));
		assertEquals(response, testee.shareProject(project.getId(), "", ArrayUtils.toArray(permissionRequest)));
	}

	@Test
	public void shareUserPresentAndDeleteRequested() {
		response.add(new PermissionResponseDTO(true, "Modified permissions for user " + regUser.getEmail()));
		assertEquals(response, testee.shareProject(project.getId(), "", ArrayUtils.toArray(permissionRequest)));
	}

	@Test
	public void shareUserPresentAndUpdateRequested() {
		permissionRequest.setPermission(Permission.READ_ONLY);
		response.add(new PermissionResponseDTO(true, "Modified permissions for user " + regUser.getEmail()));
		assertEquals(response, testee.shareProject(project.getId(), "", ArrayUtils.toArray(permissionRequest)));
	}

	@Test
	public void getPermissionsResourceNotFoundException() {
		when(permissionService.getProjectIfOwner(any(), any())).thenThrow(ResourceNotFoundException.class);
		assertThrows(ResourceNotFoundException.class, () -> {
			testee.getAllPermissions(null, null);
		});
	}

	@Test
	public void getPermissionsSuccess() {
		PermissionEntity permission = new PermissionEntity();
		permission.setUser(new RegisteredUserEntity(TEST_MAIL, Long.valueOf(1)));
		permission.setProject(new ProjectEntity());
		permission.setWritePermission(false);
		LinkedList<PermissionEntity> retList = new LinkedList<PermissionEntity>();
		retList.add(permission);
		when(permissionRepository.findByProject(any())).thenReturn(retList);

		PermissionDTO expectedPermission = new PermissionDTO();
		expectedPermission.setUser(new RegisteredUserDTO(TEST_MAIL, Long.valueOf(1)));
		expectedPermission.setProject(new ProjectDTO());
		expectedPermission.setWritePermission(false);
		when(permissionMapper.toDTO(any())).thenReturn(expectedPermission);

		List<PermissionDTO> returnList = testee.getAllPermissions(null, null);
		assertEquals(1, returnList.size());
		assertEquals(expectedPermission, returnList.get(0));

	}
}
