package com.htec.naves.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htec.naves.ProjectServiceApplication;
import com.htec.naves.dto.DeleteResponseDTO;
import com.htec.naves.dto.PathDTO;
import com.htec.naves.entity.FileEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.InvalidArgumentException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.repository.FileRepository;
import com.htec.naves.repository.PermissionRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.util.JwtUtil;
import com.htec.naves.util.NameValidator;
import com.htec.naves.util.PathUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectServiceApplication.class)
public class FileServiceTest {

	private final static String TEST_STRING = "TEST";
	private final static String INVALID_TEST_STRING = "/TEST";
	private final static String DOT_TEST_STRING = "TEST.txt";
	private static List<PathDTO> TEST_STRING_ARRAY;
	private final static Long TEST_ID = Long.valueOf(1);
	private final static ProjectEntity TEST_PROJECT = new ProjectEntity(TEST_ID, null, TEST_STRING, null, null, null);
	private FileEntity file;
	private RegisteredUserEntity TEST_OWNER;

	@Mock
	private ProjectRepository projectRepository;

	@Mock
	private PermissionRepository permissionRepository;

	@Mock
	private PermissionService permisionService;

	@Mock
	private FileRepository fileRepository;

	@Mock
	private JwtUtil jwtUtil;

	@Mock
	private PathUtil pathUtil;
	
	@Mock
	private NameValidator nameValidator;

	@InjectMocks
	private FileService testee = new FileService("\\fileSystem");

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);

		TEST_STRING_ARRAY = new ArrayList<>();
		TEST_STRING_ARRAY.add(new PathDTO("TEST"));
		file = new FileEntity();

		TEST_OWNER = new RegisteredUserEntity();
		TEST_OWNER.setEmail(TEST_STRING);
		TEST_PROJECT.setOwner(TEST_OWNER);
	}

	@After
	public void after() {
		testee.setFileStoragePath(Paths.get("\\fileSystem"));
	}

	@Test
	public void deleteNoProjectTest() {
		Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
		Mockito.doReturn(Optional.empty()).when(projectRepository).findById(TEST_ID);
		LinkedList<DeleteResponseDTO> response = new LinkedList<>();
		response.add(new DeleteResponseDTO(false, "Project does not exist", TEST_STRING));
		assertEquals(response, testee.deleteFiles(TEST_STRING_ARRAY, TEST_ID, TEST_STRING));
	}

	@Test
	public void deleteBadPathTest() {
		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class)) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			InvalidPathException ex = new InvalidPathException("", "");
			staticMock.when(() -> Paths.get(any(String.class))).thenThrow(ex);
			LinkedList<DeleteResponseDTO> response = new LinkedList<>();
			response.add(new DeleteResponseDTO(false, "Path is invalid", TEST_STRING));
			assertEquals(response, testee.deleteFiles(TEST_STRING_ARRAY, TEST_ID, TEST_STRING));
		}
	}

	@Test
	public void rootDeleteTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class)) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath());
			LinkedList<DeleteResponseDTO> response = new LinkedList<>();
			response.add(new DeleteResponseDTO(false, "Can't delete root directory", TEST_STRING));
			assertEquals(response, testee.deleteFiles(TEST_STRING_ARRAY, TEST_ID, TEST_STRING));
		}
	}

	@Test
	public void deleteDirectoryTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath())
					.thenReturn(testee.getFileStoragePath().getParent());
			filesMock.when(() -> Files.isDirectory(any(Path.class))).thenReturn(true);
			LinkedList<DeleteResponseDTO> response = new LinkedList<>();
			response.add(new DeleteResponseDTO(true, "Successfully deleted " + TEST_STRING, TEST_STRING));
			assertEquals(response, testee.deleteFiles(TEST_STRING_ARRAY, TEST_ID, TEST_STRING));
		}
	}

	@Test
	public void deleteFileDoesntExistTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath())
					.thenReturn(testee.getFileStoragePath().getParent());
			filesMock.when(() -> Files.exists(any(Path.class))).thenReturn(true);
			filesMock.when(() -> Files.isDirectory(any(Path.class))).thenReturn(false);
			filesMock.when(() -> Files.isRegularFile(any(Path.class))).thenReturn(false);
			LinkedList<DeleteResponseDTO> response = new LinkedList<>();
			response.add(new DeleteResponseDTO(false, "File doesn't exist", TEST_STRING));
			assertEquals(response, testee.deleteFiles(TEST_STRING_ARRAY, TEST_ID, TEST_STRING));
		}
	}

	@Test
	public void cantDeleteTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath())
					.thenReturn(testee.getFileStoragePath().getParent());
			filesMock.when(() -> Files.exists(any(Path.class))).thenReturn(true);
			filesMock.when(() -> Files.isDirectory(any(Path.class))).thenReturn(false);
			filesMock.when(() -> Files.isRegularFile(any(Path.class))).thenReturn(true);
			filesMock.when(() -> Files.deleteIfExists(any(Path.class))).thenThrow(new IOException());
			LinkedList<DeleteResponseDTO> response = new LinkedList<>();
			response.add(new DeleteResponseDTO(false, "Error deleting file", TEST_STRING));
			assertEquals(response, testee.deleteFiles(TEST_STRING_ARRAY, TEST_ID, TEST_STRING));
		}
	}

	@Test
	public void deleteFileTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath())
					.thenReturn(testee.getFileStoragePath().getParent());
			filesMock.when(() -> Files.exists(any(Path.class))).thenReturn(true);
			filesMock.when(() -> Files.isDirectory(any(Path.class))).thenReturn(false);
			filesMock.when(() -> Files.isRegularFile(any(Path.class))).thenReturn(true);
			LinkedList<DeleteResponseDTO> response = new LinkedList<>();
			response.add(new DeleteResponseDTO(true, "Successfully deleted " + TEST_STRING, TEST_STRING));
			assertEquals(response, testee.deleteFiles(TEST_STRING_ARRAY, TEST_ID, TEST_STRING));
		}
	}

	@Test
	public void getSetTest() {
		testee.setFileStorageLocation(TEST_STRING);
		assertEquals(testee.getFileStorageLocation(), TEST_STRING);
		testee.setFileStoragePath(Path.of(TEST_STRING));
		assertEquals(testee.getFileStoragePath(), Path.of(TEST_STRING));
	}

	@Test
	public void renameBadArgumentTest() {
		Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
		Mockito.doThrow(InvalidArgumentException.class).when(nameValidator).checkName(any(), any());
		assertThrows(InvalidArgumentException.class, () -> {
			testee.renameFile(TEST_STRING, INVALID_TEST_STRING, TEST_ID, TEST_STRING);
		});
	}

	@Test
	public void renameBadPathTest() {
		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class)) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			InvalidPathException ex = new InvalidPathException("", "");
			staticMock.when(() -> Paths.get(any(String.class))).thenThrow(ex);
			assertThrows(InvalidArgumentException.class, () -> {
				testee.renameFile(TEST_STRING, TEST_STRING, TEST_ID, TEST_STRING);
			});
		}
	}

	@Test
	public void rootRenameTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class)) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath());
			assertThrows(InvalidArgumentException.class, () -> {
				testee.renameFile(TEST_STRING, TEST_STRING, TEST_ID, TEST_STRING);
			});
		}
	}

	@Test
	public void renameAlreadyExistsTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath())
					.thenReturn(testee.getFileStoragePath().getParent());
			filesMock.when(() -> Files.isDirectory(any(Path.class))).thenReturn(true);
			FileAlreadyExistsException ex = new FileAlreadyExistsException("");
			filesMock.when(() -> Files.move(any(Path.class), any(Path.class))).thenThrow(ex);
			assertThrows(ResourceAlreadyExistsException.class, () -> {
				testee.renameFile(TEST_STRING, TEST_STRING, TEST_ID, TEST_STRING);
			});
		}
	}

	@Test
	public void renameDirectoryTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath())
					.thenReturn(testee.getFileStoragePath().getParent());
			filesMock.when(() -> Files.isDirectory(any(Path.class))).thenReturn(true);
			ArrayList<FileEntity> list = new ArrayList<FileEntity>();
			list.add(file);
			Mockito.doReturn(list).when(fileRepository).findByFilePathStartsWith(any(String.class));
			assertDoesNotThrow(() -> {
				testee.renameFile(TEST_STRING, TEST_STRING, TEST_ID, TEST_STRING);
			});
		}
	}

	@Test
	public void renameFileDoesntExistTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath())
					.thenReturn(testee.getFileStoragePath().getParent());
			filesMock.when(() -> Files.isDirectory(any(Path.class))).thenReturn(false);
			filesMock.when(() -> Files.isRegularFile(any(Path.class))).thenReturn(false);
			assertThrows(ResourceNotFoundException.class, () -> {
				testee.renameFile(TEST_STRING, TEST_STRING, TEST_ID, TEST_STRING);
			});
		}
	}

	@Test
	public void renameFileExtensionTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath())
					.thenReturn(testee.getFileStoragePath().getParent());
			filesMock.when(() -> Files.isDirectory(any(Path.class))).thenReturn(false);
			filesMock.when(() -> Files.isRegularFile(any(Path.class))).thenReturn(true);
			assertThrows(InvalidArgumentException.class, () -> {
				testee.renameFile(DOT_TEST_STRING, TEST_STRING, TEST_ID, TEST_STRING);
			});
		}
	}

	@Test
	public void renameFileTest() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath())
					.thenReturn(testee.getFileStoragePath().getParent());
			filesMock.when(() -> Files.isDirectory(any(Path.class))).thenReturn(false);
			filesMock.when(() -> Files.isRegularFile(any(Path.class))).thenReturn(true);
			Mockito.doReturn(Optional.of(file)).when(fileRepository).findById(any(String.class));
			assertDoesNotThrow(() -> {
				testee.renameFile(DOT_TEST_STRING, DOT_TEST_STRING, TEST_ID, TEST_STRING);
			});
		}
	}

	@Test
	public void renameFileFileSystemException() {

		try (MockedStatic<Paths> staticMock = Mockito.mockStatic(Paths.class);
				MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class);) {

			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(TEST_PROJECT)).when(projectRepository).findById(TEST_ID);
			staticMock.when(() -> Paths.get(any(String.class))).thenReturn(testee.getFileStoragePath())
					.thenReturn(testee.getFileStoragePath().getParent());
			filesMock.when(() -> Files.isDirectory(any(Path.class))).thenReturn(false);
			filesMock.when(() -> Files.isRegularFile(any(Path.class))).thenReturn(true);
			filesMock.when(() -> Files.move(any(Path.class), any(Path.class)))
					.thenThrow(DirectoryNotEmptyException.class);
			Mockito.doReturn(Optional.of(file)).when(fileRepository).findById(any(String.class));
			assertThrows(FileSystemException.class, () -> {
				testee.renameFile(DOT_TEST_STRING, DOT_TEST_STRING, TEST_ID, TEST_STRING);
			});
		}
	}

}