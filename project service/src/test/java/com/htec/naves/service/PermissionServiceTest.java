package com.htec.naves.service;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htec.naves.ProjectServiceApplication;
import com.htec.naves.entity.PermissionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.enums.Operation;
import com.htec.naves.enums.Permission;
import com.htec.naves.exception.InvalidTokenException;
import com.htec.naves.exception.PermissionException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.repository.PermissionRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.JwtUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectServiceApplication.class)
public class PermissionServiceTest {
	private final static String TEST_STRING = "TEST";
	@InjectMocks
	private PermissionService testee;

	@Mock
	private ProjectRepository projectRepository;

	@Mock
	private PermissionRepository permissionRepository;

	@Mock
	private JwtUtil jwtUtil;

	@Mock
	private RegisteredUserRepository userRepository;

	private ProjectEntity project;
	private RegisteredUserEntity registeredUser;
	private PermissionEntity permissionEntity;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);

		project = new ProjectEntity();
		registeredUser = new RegisteredUserEntity();
		registeredUser.setEmail("testMail@gmail.com");
		project.setOwner(registeredUser);

		permissionEntity = new PermissionEntity();

		when(projectRepository.findById(any())).thenReturn(Optional.of(project));

		when(jwtUtil.extractUsername(any())).thenReturn("anotherMail@gmail.com");

		when(permissionRepository.findByUserAndProject(any(), any())).thenReturn(Optional.of(permissionEntity));

		when(userRepository.findByEmail(any())).thenReturn(Optional.of(registeredUser));
	}

	@Test
	public void noSuchProject() {
		when(projectRepository.findById(any())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> {
			testee.checkPermission(null, null, null);
		});
	}

	@Test
	public void errorWithToken() {
		when(jwtUtil.extractUsername(any())).thenThrow(new InvalidTokenException(TEST_STRING));
		assertThrows(InvalidTokenException.class, () -> {
			testee.checkPermission(null, null, null);
		});
	}

	@Test
	public void heIsAnOwner() {
		when(jwtUtil.extractUsername(any())).thenReturn("testMail@gmail.com");
		testee.checkPermission(null, null, null);
	}

	@Test
	public void notAnOwner() {
		when(permissionRepository.findByUserAndProject(any(), any())).thenReturn(Optional.empty());
		assertThrows(PermissionException.class, () -> {
			testee.checkPermission(null, null, null);
		});
	}

	@Test
	public void doesNotHaveWritePermission() {
		PermissionEntity permissionEntity = new PermissionEntity();

		when(permissionRepository.findByUserAndProject(any(), any())).thenReturn(Optional.of(permissionEntity));

		assertThrows(PermissionException.class, () -> {
			testee.checkPermission(Operation.WRITE, null, null);
		});
	}

	@Test
	public void hasReadPermission() {
		permissionEntity.setWritePermission(false);
		when(permissionRepository.findByUserAndProject(any(), any())).thenReturn(Optional.of(permissionEntity));
		testee.checkPermission(Operation.READ, null, null);
	}

	@Test
	public void hasWritePermission() {
		permissionEntity.setWritePermission(true);

		when(permissionRepository.findByUserAndProject(any(), any())).thenReturn(Optional.of(permissionEntity));
		testee.checkPermission(Operation.WRITE, null, null);
	}

	@Test
	public void ownerNoProjectPresent() {
		when(projectRepository.findById(any())).thenReturn(Optional.empty());

		assertThrows(ResourceNotFoundException.class, () -> {
			testee.getProjectIfOwner(null, null);
		});
	}

	@Test
	public void errorWithTokenForOwner() {
		ProjectEntity project = new ProjectEntity();
		when(projectRepository.findById(any())).thenReturn(Optional.of(project));
		when(jwtUtil.extractUsername(any())).thenThrow(new InvalidTokenException(TEST_STRING));

		assertThrows(InvalidTokenException.class, () -> {
			testee.getProjectIfOwner(null, null);
		});
	}

	@Test
	public void heIsAnOwnerCheck() {
		when(jwtUtil.extractUsername(any())).thenReturn("testMail@gmail.com");
		assertEquals(project, testee.getProjectIfOwner(null, null));
	}

	@Test
	public void notAnOwnerCheck() {
		when(permissionRepository.findByUserAndProject(any(), any())).thenReturn(Optional.empty());

		assertThrows(PermissionException.class, () -> {
			testee.getProjectIfOwner(null, null);
		});

	}

}
