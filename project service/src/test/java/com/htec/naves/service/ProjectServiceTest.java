package com.htec.naves.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileSystemUtils;

import com.htec.naves.ProjectServiceApplication;
import com.htec.naves.dto.PermissionDTO;
import com.htec.naves.dto.ProjectDTO;
import com.htec.naves.dto.RegisteredUserDTO;
import com.htec.naves.entity.PermissionEntity;
import com.htec.naves.entity.ProjectEntity;
import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.exception.FileSystemException;
import com.htec.naves.exception.HttpResponseException;
import com.htec.naves.exception.InterserviceCommunicationException;
import com.htec.naves.exception.InvalidTokenException;
import com.htec.naves.exception.ResourceAlreadyExistsException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.mapper.PermissionEntityDTOMapper;
import com.htec.naves.mapper.ProjectEntityDTOMapper;
import com.htec.naves.mapper.RegisteredUserEntityDTOMapper;
import com.htec.naves.publisher.Publisher;
import com.htec.naves.repository.FileRepository;
import com.htec.naves.repository.PermissionRepository;
import com.htec.naves.repository.ProjectRepository;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.JwtUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectServiceApplication.class)
public class ProjectServiceTest {
	private final static String TEST_STRING = "TEST";
	private final static Long TEST_ID = Long.valueOf(1);
	private RegisteredUserEntity registeredUserEntity;
	private RegisteredUserDTO registeredUserDTO;
	private ProjectDTO projectDTO;
	private ProjectEntity projectEntity;

	@Mock
	private JwtUtil jwtUtil;

	@Mock
	private RegisteredUserRepository registeredUserRepository;

	@Mock
	private ProjectRepository projectRepository;

	@Mock
	private FileRepository fileRepository;

	@Mock
	private PermissionRepository permissionRepository;

	@Mock
	private ProjectEntityDTOMapper projectmapper;

	@Mock
	private RegisteredUserEntityDTOMapper registeredUserMapper;

	@Mock
	private PermissionService permissionService;

	@Mock
	private Publisher publisher;
	@Mock
	private PermissionEntityDTOMapper permissionMapper;

	@InjectMocks
	private ProjectService testee;

	private RegisteredUserEntity registeredUser;
	private PermissionDTO permissionDTO;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);

		registeredUserDTO = new RegisteredUserDTO(TEST_STRING, Long.valueOf(1));
		projectDTO = new ProjectDTO(TEST_ID, registeredUserDTO, TEST_STRING, null, null);
		registeredUserEntity = new RegisteredUserEntity(TEST_STRING, Long.valueOf(1));
		projectEntity = new ProjectEntity(TEST_ID, registeredUserEntity, TEST_STRING, null, null, null);
		registeredUser = new RegisteredUserEntity();
		registeredUser.setEmail(TEST_STRING);
		projectEntity.setOwner(registeredUser);
		permissionDTO = new PermissionDTO();
		permissionDTO.setProject(projectDTO);
		when(permissionMapper.toDTO(any())).thenReturn(permissionDTO);
		when(projectRepository.findById(any())).thenReturn(Optional.of(projectEntity));
	}

	@After
	public void after() {
		testee.setUploadPath("\\fileSystem");
	}

	@Test
	public void createBadTokenTest() {
		InvalidTokenException ex = new InvalidTokenException(TEST_STRING);
		Mockito.doThrow(ex).when(jwtUtil).extractUsername(TEST_STRING);
		assertThrows(InvalidTokenException.class, () -> {
			testee.createProject(projectDTO, TEST_STRING);
		});
	}

	@Test
	public void createProjectNameExistsTest() {
		Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
		Mockito.doReturn(Optional.of(TEST_STRING)).when(registeredUserRepository).findByEmail(TEST_STRING);
		Mockito.doReturn(Optional.of(TEST_STRING)).when(projectRepository).findProjectForUser(TEST_STRING, TEST_STRING);
		assertThrows(ResourceAlreadyExistsException.class, () -> {
			testee.createProject(projectDTO, TEST_STRING);
		});
	}

	@Test
	public void createErrorTest() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class)) {
			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(registeredUserEntity)).when(registeredUserRepository).findByEmail(TEST_STRING);
			Mockito.doReturn(Optional.empty()).when(projectRepository).findProjectForUser(TEST_STRING, TEST_STRING);
			Mockito.doReturn(projectEntity).when(projectRepository).save(any());
			filesMock.when(() -> Files.exists(any(Path.class))).thenReturn(false);
			IOException ex = new IOException();
			filesMock.when(() -> Files.createDirectories(any(Path.class))).thenThrow(ex);
			assertThrows(FileSystemException.class, () -> {
				testee.createProject(projectDTO, TEST_STRING);
			});
		}
	}

	@Test
	public void createSuccessTest() {
		try (MockedStatic<Files> filesMock = Mockito.mockStatic(Files.class)) {
			Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
			Mockito.doReturn(Optional.of(registeredUserEntity)).when(registeredUserRepository).findByEmail(TEST_STRING);
			Mockito.doReturn(Optional.empty()).when(projectRepository).findProjectForUser(TEST_STRING, TEST_STRING);
			Mockito.doReturn(projectEntity).when(projectRepository).save(any());
			filesMock.when(() -> Files.exists(any(Path.class))).thenReturn(false);
			assertDoesNotThrow(() -> {
				testee.createProject(projectDTO, TEST_STRING);
			});
		}
	}

	@Test
	public void renameBadTokenTest() {
		InvalidTokenException ex = new InvalidTokenException(TEST_STRING);
		Mockito.doThrow(ex).when(jwtUtil).extractUsername(TEST_STRING);
		assertThrows(InvalidTokenException.class, () -> {
			testee.renameProject(projectDTO, TEST_STRING);
		});
	}

	@Test
	public void renameProjectNameExistsTest() {
		Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
		Mockito.doReturn(Optional.of(TEST_STRING)).when(projectRepository).findProjectForUser(any(), any());
		assertThrows(ResourceAlreadyExistsException.class, () -> {
			testee.renameProject(projectDTO, TEST_STRING);
		});
	}

	@Test
	public void renameSuccessTest() {
		Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
		Mockito.doReturn(Optional.empty()).when(projectRepository).findProjectForUser(TEST_STRING, TEST_STRING);
		Mockito.doReturn(Optional.of(projectEntity)).when(projectRepository).findById(TEST_ID);
		assertDoesNotThrow(() -> {
			testee.renameProject(projectDTO, TEST_STRING);
		});
	}

	@Test
	public void deleteBadTokenTest() {
		Mockito.doReturn(Optional.of(projectEntity)).when(projectRepository).findById(TEST_ID);
		InvalidTokenException ex = new InvalidTokenException(TEST_STRING);
		Mockito.doThrow(ex).when(jwtUtil).extractUsername(TEST_STRING);
		assertThrows(InvalidTokenException.class, () -> {
			testee.deleteProject(TEST_ID, TEST_STRING);
		});
	}

	@Test
	public void deleteNoProjectErrorTest() {
		try (MockedStatic<FileSystemUtils> fileUtilsMock = Mockito.mockStatic(FileSystemUtils.class)) {
			Mockito.doThrow(ResourceNotFoundException.class).when(permissionService).checkPermission(any(), any(),
					any());
			assertThrows(ResourceNotFoundException.class, () -> {
				testee.deleteProject(TEST_ID, TEST_STRING);
			});
		}
	}

	@Test
	public void deleteFileSystemErrorTest() {
		try (MockedStatic<FileSystemUtils> fileUtilsMock = Mockito.mockStatic(FileSystemUtils.class)) {
			Mockito.doReturn(Optional.of(projectEntity)).when(projectRepository).findById(TEST_ID);
			IOException ex = new IOException();
			fileUtilsMock.when(() -> FileSystemUtils.deleteRecursively(any(Path.class))).thenThrow(ex);
			assertThrows(FileSystemException.class, () -> {
				testee.deleteProject(TEST_ID, TEST_STRING);
			});
		}
	}

	@Test
	public void deleteRabbitError() throws IOException {
		Mockito.doReturn(Optional.of(projectEntity)).when(projectRepository).findById(TEST_ID);
		Mockito.doThrow(IOException.class).when(publisher).sendDeleteProject(any());
		assertThrows(InterserviceCommunicationException.class, () -> {
			testee.deleteProject(TEST_ID, TEST_STRING);
		});
	}

	@Test
	public void deleteSuccessTest() {
		Mockito.doReturn(Optional.of(projectEntity)).when(projectRepository).findById(TEST_ID);
		assertDoesNotThrow(() -> {
			testee.deleteProject(TEST_ID, TEST_STRING);
		});
	}

	@Test
	public void getMyProjectsBadTokenTest() {
		Mockito.doThrow(new InvalidTokenException(TEST_STRING)).when(jwtUtil).extractUsername(TEST_STRING);
		assertThrows(InvalidTokenException.class, () -> {
			testee.getMyProjects(TEST_STRING);
		});
	}

	@Test
	public void getMyProjectsSuccessTest() {
		Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
		Mockito.doReturn(Optional.of(registeredUserEntity)).when(registeredUserRepository).findByEmail(TEST_STRING);
		List<ProjectEntity> projectList = new ArrayList<>();
		projectList.add(projectEntity);
		Mockito.doReturn(projectList).when(projectRepository).findByOwner(any());
		Mockito.doReturn(projectDTO).when(projectmapper).toDTO(projectEntity);
		assertDoesNotThrow(() -> {
			testee.getMyProjects(TEST_STRING);
		});
	}

	@Test
	public void getSharedProjectsBadTokenTest() {
		InvalidTokenException ex = new InvalidTokenException(TEST_STRING);
		Mockito.doThrow(ex).when(jwtUtil).extractUsername(TEST_STRING);
		assertThrows(InvalidTokenException.class, () -> {
			testee.getSharedProjects(TEST_STRING);
		});
	}

	@Test
	public void getSharedProjectsSuccessTest() {
		Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(TEST_STRING);
		List<ProjectEntity> projectList = new ArrayList<>();
		projectList.add(projectEntity);
		PermissionEntity permission = new PermissionEntity();
		permission.setProject(projectEntity);
		List<PermissionEntity> permissions = new ArrayList<>();
		permissions.add(permission);
		Mockito.doReturn(Optional.of(registeredUser)).when(registeredUserRepository).findByEmail(any());
		Mockito.doReturn(permissions).when(permissionRepository).findByUser(any());
		Mockito.doReturn(projectDTO).when(projectmapper).toDTO(projectEntity);
		assertDoesNotThrow(() -> {
			testee.getSharedProjects(TEST_STRING);
		});
	}

	@Test
	public void getMyProjectsPagableTest() {
		Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(any());
		Mockito.doReturn(Optional.of(registeredUserEntity)).when(registeredUserRepository).findByEmail(any());
		List<ProjectEntity> list = new ArrayList<>();
		list.add(projectEntity);

		List<ProjectDTO> listDTO = new ArrayList<>();
		listDTO.add(projectDTO);

		Mockito.doReturn(list).when(projectRepository).findAllByOwner(any(), any());
		Mockito.doReturn(projectDTO).when(projectmapper).toDTO(any());
		assertEquals(new PageImpl<ProjectDTO>(listDTO), testee.getMyProjects(TEST_STRING, null));
	}

	@Test
	public void getMyProjectsPagableInvalidTokenTest() {
		Mockito.doThrow(new InvalidTokenException(TEST_STRING)).when(jwtUtil).extractUsername(any());
		assertThrows(InvalidTokenException.class, () -> {
			testee.getMyProjects(TEST_STRING, null);
		});
	}

	@Test
	public void getSharedProjectsPagableTest() {
		Mockito.doReturn(TEST_STRING).when(jwtUtil).extractUsername(any());
		List<ProjectEntity> projectList = new ArrayList<>();
		projectList.add(projectEntity);
		PermissionEntity permission = new PermissionEntity();
		permission.setProject(projectEntity);
		List<PermissionEntity> permissions = new ArrayList<>();
		permissions.add(permission);
		List<PermissionDTO> listDTO = new ArrayList<>();
		listDTO.add(permissionDTO);
		Mockito.doReturn(Optional.of(registeredUser)).when(registeredUserRepository).findByEmail(any());
		Mockito.doReturn(permissions).when(permissionRepository).findByUser(any(), any());
		Mockito.doReturn(projectDTO).when(projectmapper).toDTO(any());
		assertEquals(new PageImpl<PermissionDTO>(listDTO), testee.getSharedProjects(TEST_STRING, null));
	}

	@Test
	public void getSharedProjectsPageableInvalidTokenTest() throws HttpResponseException {
		Mockito.doReturn(Optional.empty()).when(registeredUserRepository).findByEmail(any());
		assertThrows(InvalidTokenException.class, () -> {
			testee.getSharedProjects(TEST_STRING, null);
		});
	}

	@Test
	public void checkValidUploadPath() throws HttpResponseException {
		testee.setUploadPath(TEST_STRING);
		assertEquals(TEST_STRING, testee.getUploadPath());
	}

}
