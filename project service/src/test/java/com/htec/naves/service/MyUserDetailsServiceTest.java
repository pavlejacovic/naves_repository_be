package com.htec.naves.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import com.htec.naves.entity.RegisteredUserEntity;
import com.htec.naves.repository.RegisteredUserRepository;
import com.htec.naves.util.MyUser;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyUserDetailsServiceTest {

	private final static String TEST = "TEST";

	@Mock
	private RegisteredUserRepository registeredUserRepository;

	@InjectMocks
	private MyUserDetailsService testee;
	private MyUser myUser;
	private Optional<RegisteredUserEntity> registeredUserEntityOptional;

	@Before
	public void setup() {
		MockitoAnnotations.openMocks(this);
		registeredUserEntityOptional = Optional.empty();
		myUser = new MyUser(null, 1);
	}

	@Test
	public void loadUserByUsernameTest1() {
		String id = "1";
		when(registeredUserRepository.findById(id)).thenReturn(registeredUserEntityOptional);
		assertThrows(UsernameNotFoundException.class, () -> {

			testee.loadUserByUsername(id);
		});
	}

	@Test
	public void loadUserByUsernameTest2() {
		User user = new User(TEST, TEST, new ArrayList<>());
		myUser = new MyUser(user, 1);
		RegisteredUserEntity userEntity = new RegisteredUserEntity(TEST, Long.valueOf(1));
		registeredUserEntityOptional = Optional.of(userEntity);
		String id = "1";
		when(registeredUserRepository.findById(id)).thenReturn(registeredUserEntityOptional);
		assertEquals(myUser, testee.loadUserByUsername(id));
	}

}
