package com.htec.naves.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import com.htec.naves.dto.ClientDTO;
import com.htec.naves.entity.AdminEntity;
import com.htec.naves.entity.ClientEntity;
import com.htec.naves.entity.UserEntity;
import com.htec.naves.entity.enums.Provider;
import com.htec.naves.mapper.ClientEntityDTOMapper;
import com.htec.naves.repository.UserRepository;
import com.htec.naves.util.MyUser;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyUserDetailsServiceTest {

	@InjectMocks
	private MyUserDetailsService myUserDetailsService;

	@Mock
	private UserRepository<UserEntity> userRepository;

	@Mock
	ClientService clientService;

	@Mock
	ClientEntityDTOMapper clientMapper;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void userNotFount() {
		String email = "uros.stokovic@htecgroup.com";

		when(userRepository.findByEmail(email)).thenReturn(Optional.empty());
		assertThrows(UsernameNotFoundException.class, () -> {
			myUserDetailsService.loadUserByUsername(email);
		});
	}

	@Test
	public void foundAdmin() {
		String email = "uros.stokovic@htecgroup.com";

		AdminEntity ue = new AdminEntity();
		ue.setEmail(email);
		ue.setPassword("sifra");
		ue.setName("Uros");
		ue.setSurname("Stokovic");
		ue.setUserId(Long.valueOf(1));

		when(userRepository.findByEmail(email)).thenReturn(Optional.of(ue));

		MyUser myUser = new MyUser(new User(email, "sifra", new ArrayList<>()), 2);

		UserDetails ud = myUserDetailsService.loadUserByUsername(email);

		assertTrue(ud instanceof MyUser);

		assertTrue(myUser.equals(ud));
	}

	@Test
	public void foundRegularVerifiedUser() {
		String email = "uros.stokovic@htecgroup.com";

		ClientEntity ue = new ClientEntity();
		ue.setEmail(email);
		ue.setPassword("sifra");
		ue.setUserId(Long.valueOf(1));
		ue.setEnabled(true);
		ue.setProvider(Provider.LOCAL);

		when(userRepository.findByEmail(email)).thenReturn(Optional.of(ue));

		MyUser myUser = new MyUser(new User(email, "sifra", new ArrayList<>()), 1);

		UserDetails ud = myUserDetailsService.loadUserByUsername(email);

		assertTrue(ud instanceof MyUser);

		assertTrue(myUser.equals(myUserDetailsService.loadUserByUsername(email)));
	}

	@Test
	public void foundGoogleUser() {
		String email = "uros.stokovic@htecgroup.com";

		ClientEntity ue = new ClientEntity();
		ue.setEmail(email);
		ue.setPassword("sifra");
		ue.setUserId(Long.valueOf(1));
		ue.setEnabled(true);
		ue.setProvider(Provider.GOOGLE);

		when(userRepository.findByEmail(email)).thenReturn(Optional.of(ue));

		assertEquals(new MyUser(new User(ue.getEmail().toLowerCase(), "", new ArrayList<>()), 1),
				myUserDetailsService.loadUserByUsername(email));

	}

	@Test
	public void foundRegularNonverifiedUserException() throws Exception {
		String email = "uros.stokovic@htecgroup.com";

		ClientEntity ue = new ClientEntity();
		ue.setEmail(email);
		ue.setPassword("sifra");
		ue.setUserId(Long.valueOf(1));
		ue.setEnabled(false);
		ue.setProvider(Provider.LOCAL);

		ClientDTO clientDTO = new ClientDTO();
		clientDTO.setEmail(email);
		clientDTO.setPassword("sifra");
		clientDTO.setUserId(Long.valueOf(1));
		clientDTO.setEnabled(false);
		clientDTO.setProvider(Provider.LOCAL);

		when(userRepository.findByEmail(email)).thenReturn(Optional.of(ue));
		Mockito.doThrow(RuntimeException.class).when(clientService).register(any());
		assertThrows(UsernameNotFoundException.class, () -> {
			myUserDetailsService.loadUserByUsername(email);
		});

	}

}
