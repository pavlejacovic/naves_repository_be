package com.htec.naves.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.htec.naves.dto.ClientDTO;
import com.htec.naves.dto.ForgotPasswordDTO;
import com.htec.naves.dto.ResetPasswordDTO;
import com.htec.naves.entity.ClientEntity;
import com.htec.naves.entity.enums.Provider;
import com.htec.naves.exception.EmailTakenException;
import com.htec.naves.exception.InterserviceCommunicationException;
import com.htec.naves.exception.InvalidTokenException;
import com.htec.naves.mapper.ClientEntityDTOMapper;
import com.htec.naves.publisher.Publisher;
import com.htec.naves.repository.ClientRepository;
import com.htec.naves.util.JwtUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientServiceTest {
	private final static String TEST = "TEST";
	@Mock
	private ClientRepository clientRepository;
	@Mock
	private ClientEntityDTOMapper clientMapper;
	@Mock
	private EmailSenderService emailSenderService;
	@Mock
	private SimpleMailMessage simpleMailMessage;
	@Mock
	private PasswordEncoder passwordEncoder;
	@Mock
	private JwtUtil jwtUtil;
	@Mock
	private ClientService clientService;
	@Mock
	private Publisher publisher;
	@InjectMocks
	private ClientService testee;
	private ClientDTO clientDTO;
	private ClientEntity clientEntity;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		clientDTO = new ClientDTO();
		clientDTO.setUserId(1L);
		clientDTO.setEmail(TEST);
		clientDTO.setName(TEST);
		clientDTO.setPassword(TEST);
		clientDTO.setProvider(Provider.LOCAL);
		clientDTO.setSubscribed(true);
		clientDTO.setSurname(TEST);
		clientDTO.setEnabled(false);
		clientEntity = new ClientEntity();
		clientEntity.setEnabled(true);
		clientEntity.setEmail(TEST);
		clientEntity.setName(TEST);
		clientEntity.setPassword(TEST);
		clientEntity.setProvider(Provider.LOCAL);
		clientEntity.setSubscribed(true);
		clientEntity.setSurname(TEST);
		clientEntity.setUserId(1L);
	}

	@Test
	public void registerTest() {
		when(clientRepository.findByEmail(any())).thenReturn(clientEntity);
		assertThrows(EmailTakenException.class, () -> {
			testee.register(clientDTO);
		});
	}

	@Test
	public void registerTest2() {
		clientEntity.setEnabled(false);
		when(clientRepository.findByEmail(any())).thenReturn(clientEntity);
		testee.register(clientDTO);
	}

	@Test
	public void registerTest3() {
		when(clientMapper.toEntity(any())).thenReturn(clientEntity);
		clientEntity.setEnabled(false);
		when(clientRepository.findByEmail(any())).thenReturn(null);
		testee.register(clientDTO);
	}

	@Test
	public void registerTest4() throws UnsupportedEncodingException, MessagingException {
		when(clientMapper.toEntity(any())).thenReturn(clientEntity);
		Mockito.doThrow(RuntimeException.class).when(emailSenderService).sendConfirmationEmail(any(), any());
		assertThrows(IllegalArgumentException.class, () -> {
			testee.register(clientDTO);
		});
	}

	@Test
	public void confirmTest() throws UnsupportedEncodingException, MessagingException {
		Mockito.doThrow(IllegalArgumentException.class).when(emailSenderService).sendConfirmationEmail(any(), any());
		assertThrows(InvalidTokenException.class, () -> {
			testee.confirm(TEST);
		});
	}

	@Test
	public void confirmTest2() {
		when(clientRepository.findByConfirmationToken(any())).thenReturn(clientEntity);
		testee.confirm(TEST);
	}

	@Test
	public void confirmTest3() throws Exception {
		when(jwtUtil.isTokenExpired(TEST)).thenReturn(true);
		assertThrows(InvalidTokenException.class, () -> {
			testee.confirm(TEST);
		});
	}

	@Test
	public void confirmTest4() throws Exception {
		assertThrows(InvalidTokenException.class, () -> {
			testee.confirm(null);
		});
	}

	@Test
	public void confirmTest5() throws Exception {
		when(clientRepository.findByConfirmationToken(any())).thenReturn(clientEntity);
		when(jwtUtil.isTokenExpired(TEST)).thenThrow(InvalidTokenException.class);
		assertThrows(InvalidTokenException.class, () -> {
			testee.confirm(TEST);
		});
	}

	@Test
	public void confirmTest6() throws Exception {
		when(clientRepository.findByConfirmationToken(any())).thenReturn(null);
		assertThrows(InvalidTokenException.class, () -> {
			testee.confirm(TEST);
		});
	}

	@Test
	public void confirmTest7() throws Exception {
		when(clientRepository.findByConfirmationToken(any())).thenReturn(clientEntity);
		Mockito.doThrow(IOException.class).when(publisher).sendVerifiedUserData(any());
		assertThrows(InterserviceCommunicationException.class, () -> {
			testee.confirm(TEST);
		});
	}

	@Test
	public void forgetPasswordTest() {
		when(clientRepository.findByEmail(any())).thenReturn(clientEntity);
		when(jwtUtil.generateTokenFromString(any())).thenReturn(TEST);
		testee.forgotPassword(new ForgotPasswordDTO(TEST));
	}

	@Test
	public void forgetPasswordTest2() throws IOException {
		clientEntity.setEnabled(false);
		when(clientRepository.findByEmail(any())).thenReturn(clientEntity);
		assertThrows(IllegalArgumentException.class, () -> {
			testee.forgotPassword(new ForgotPasswordDTO(TEST));
		});

	}

	@Test
	public void forgetPasswordTest3() throws IOException {
		clientEntity.setProvider(Provider.GOOGLE);
		when(clientRepository.findByEmail(any())).thenReturn(clientEntity);
		assertThrows(IllegalArgumentException.class, () -> {
			testee.forgotPassword(new ForgotPasswordDTO(TEST));
		});
	}

	@Test
	public void forgetPasswordTest4() throws UnsupportedEncodingException, MessagingException {
		when(clientRepository.findByEmail(any())).thenReturn(clientEntity);
		when(jwtUtil.generateTokenFromString(any())).thenReturn(TEST);

		Mockito.doThrow(UnsupportedEncodingException.class).when(emailSenderService).sendResetPasswordEmail(any(),
				any());

		assertThrows(IllegalArgumentException.class, () -> {
			testee.forgotPassword(new ForgotPasswordDTO(TEST));
		});
	}

	@Test
	public void sendEmailTest() throws UnsupportedEncodingException, MessagingException {
		testee.sendEmail(TEST, TEST);
	}

	@Test
	public void processResetPasswordTest1() throws Exception {
		when(jwtUtil.isTokenExpired(TEST)).thenReturn(false);
		when(clientRepository.findByResetPasswordToken(any())).thenReturn(clientEntity);
		testee.processResetPassword(new ResetPasswordDTO(TEST, TEST));
	}

	@Test
	public void processResetPasswordTest2() throws Exception {
		when(jwtUtil.isTokenExpired(TEST)).thenReturn(true);
		when(clientRepository.findByResetPasswordToken(any())).thenReturn(null);

		assertThrows(InvalidTokenException.class, () -> {
			testee.processResetPassword(new ResetPasswordDTO(TEST, TEST));
		});

	}
}
