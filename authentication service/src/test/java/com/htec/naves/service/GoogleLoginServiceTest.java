package com.htec.naves.service;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htec.naves.dto.ClientDTO;
import com.htec.naves.entity.AdminEntity;
import com.htec.naves.entity.ClientEntity;
import com.htec.naves.entity.UserEntity;
import com.htec.naves.entity.enums.Provider;
import com.htec.naves.mapper.ClientEntityDTOMapper;
import com.htec.naves.publisher.Publisher;
import com.htec.naves.repository.ClientRepository;
import com.htec.naves.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GoogleLoginServiceTest {
	private static final String TEST = "test";
	@InjectMocks
	private GoogleLoginService testee;
	@Mock
	private ClientRepository clientRepository;
	@Mock
	private UserRepository<UserEntity> userRepository;
	@Mock
	private ClientEntityDTOMapper clientMapper;
	@Mock
	private Publisher publisher;

	private ClientEntity clientEntity;
	private ClientDTO clientDto;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		clientDto = new ClientDTO();
		clientDto.setEmail(TEST);
		clientDto.setProvider(Provider.GOOGLE);
		clientDto.setSubscribed(false);
		clientDto.setEnabled(true);
		clientDto.setName(TEST);
		clientDto.setSurname(TEST);
		clientEntity = new ClientEntity();
		clientEntity.setEmail(TEST);
		clientEntity.setProvider(Provider.GOOGLE);
		clientEntity.setSubscribed(false);
		clientEntity.setEnabled(true);
		clientEntity.setName(TEST);
		clientEntity.setSurname(TEST);

	}

	@Test
	public void processOAuthPostLoginTest() throws IOException {
		when(userRepository.findByEmail(any(String.class))).thenReturn(Optional.empty());
		when(clientRepository.save(any())).thenReturn(clientEntity);
		when(clientMapper.toDTO(any())).thenReturn(clientDto);
		Mockito.doThrow(IOException.class).when(publisher).sendVerifiedUserData(any());
		assertNull(testee.processOAuthPostLogin(TEST, TEST, TEST));
	}

	@Test
	public void processOAuthPostLoginTest1() {
		when(userRepository.findByEmail(any(String.class))).thenReturn(Optional.empty());
		when(clientRepository.save(any())).thenThrow(org.springframework.dao.DataIntegrityViolationException.class);
		assertNull(testee.processOAuthPostLogin(TEST, TEST, TEST));
	}

	@Test
	public void processOAuthPostLoginTest4() {
		when(userRepository.findByEmail(any(String.class))).thenReturn(Optional.empty());
		when(clientRepository.save(any())).thenReturn(null);
		when(clientMapper.toDTO(any())).thenReturn(clientDto);
		assertEquals(clientDto, testee.processOAuthPostLogin(TEST, TEST, TEST));
	}

	@Test
	public void processOAuthPostLoginTest2() {

		when(clientMapper.toDTO(any())).thenReturn(clientDto);
		when(userRepository.findByEmail(any(String.class))).thenReturn(Optional.of(clientEntity));
		assertEquals(clientDto, testee.processOAuthPostLogin(TEST, TEST, TEST));
		clientEntity.setEnabled(false);
		assertEquals(clientDto, testee.processOAuthPostLogin(TEST, TEST, TEST));
	}

	@Test
	public void processOAuthPostLoginTest3() {
		when(userRepository.findByEmail(any(String.class))).thenReturn(Optional.of(new AdminEntity()));
		assertNull(testee.processOAuthPostLogin(TEST, TEST, TEST));
	}

}