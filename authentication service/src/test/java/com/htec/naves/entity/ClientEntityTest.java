package com.htec.naves.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientEntityTest {
	private static final String TEST="test";
	private ClientEntity testee;

	@Before
	public void setUp() {
		testee = new ClientEntity();
	}

	@Test
	public void getterSetterTest() {
		testee.setEmail(TEST);
		assertEquals(TEST, testee.getEmail());
	}
}
