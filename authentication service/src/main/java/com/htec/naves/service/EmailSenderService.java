package com.htec.naves.service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.common.io.Files;

import lombok.extern.slf4j.Slf4j;

@Service("emailSenderService")
@Slf4j
public class EmailSenderService {
	private JavaMailSender javaMailSender;

	@Autowired
	public EmailSenderService(JavaMailSender javaMailSender) {
		super();
		this.javaMailSender = javaMailSender;
	}

	@Async
	public void sendConfirmationEmail(String recipientEmail, String link)
			throws UnsupportedEncodingException, MessagingException {
		MimeMessage message = javaMailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(message);
		helper.setFrom("navesteam26@gmail.com", "Naves");

		helper.setTo(recipientEmail);

		String subject = "Verify your account";

		String content = "";
		String htmlString = "";

		try {
			content = Files.asCharSource(new File("." + File.separator + "src" + File.separator + "main"
					+ File.separator + "resources" + File.separator + "confirmAccount.html"), StandardCharsets.UTF_8)
					.read();

			htmlString = content.replace("$confirmAccountLink", link);

		} catch (IOException e) {
			log.info("Email for account confirmation could not be sent." + e.getMessage());
		}

		helper.setSubject(subject);
		helper.setText(htmlString, true);
		javaMailSender.send(message);

	}

	public void sendResetPasswordEmail(String recipientEmail, String link)
			throws UnsupportedEncodingException, MessagingException {
		MimeMessage message = javaMailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(message);
		helper.setFrom("navesteam26@gmail.com", "Naves");

		helper.setTo(recipientEmail);

		String subject = "Reset your password";

		String content = "";
		String htmlString = "";

		try {
			content = Files
					.asCharSource(new File("." + File.separator + "src" + File.separator + "main" + File.separator
							+ "resources" + File.separator + "resetPasswordEmail.html"), StandardCharsets.UTF_8)
					.read();

			htmlString = content.replace("$passwordResetLink", link);

		} catch (IOException e) {
			log.info("Email for password reset could not be sent." + e.getMessage());
		}

		helper.setSubject(subject);
		helper.setText(htmlString, true);
		javaMailSender.send(message);
	}

}
