package com.htec.naves.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.htec.naves.entity.ClientEntity;
import com.htec.naves.entity.UserEntity;
import com.htec.naves.entity.enums.Provider;
import com.htec.naves.mapper.ClientEntityDTOMapper;
import com.htec.naves.repository.UserRepository;
import com.htec.naves.util.MyUser;

/**
 * Implementation of the UserDetailsService from Spring Security.
 * 
 * @author Ivan Kukrkic
 *
 */
@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository<UserEntity> userRepository;

	@Autowired
	ClientService clientService;

	@Autowired
	ClientEntityDTOMapper clientMapper;

	/**
	 * Returns UserDetails of the user with a given name
	 * 
	 * @param email user's email
	 * @return UserDetails with the user's information
	 * @throws UsernameNotFoundException Thrown if an UserDetailsService
	 *                                   implementation cannot locate a User by its
	 *                                   email.
	 */
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		Optional<UserEntity> user = userRepository.findByEmail(email);

		if (user.isEmpty()) {
			throw new UsernameNotFoundException("User " + email + " does not exist");
		}

		if (user.get() instanceof ClientEntity) {
			if (((ClientEntity) user.get()).getProvider() == Provider.LOCAL
					&& ((ClientEntity) user.get()).getEnabled() == true) {
				return new MyUser(
						new User(user.get().getEmail().toLowerCase(), user.get().getPassword(), new ArrayList<>()), 1);
			} else if (((ClientEntity) user.get()).getProvider() == Provider.GOOGLE) {
				return new MyUser(new User(user.get().getEmail().toLowerCase(), "", new ArrayList<>()), 1);
			}
		} else {
			return new MyUser(new User(user.get().getEmail(), user.get().getPassword(), new ArrayList<>()), 2);
		}
		throw new UsernameNotFoundException("User " + email + " does not exist");

	}

}