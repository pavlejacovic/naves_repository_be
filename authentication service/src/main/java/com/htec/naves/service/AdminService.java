package com.htec.naves.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.htec.naves.entity.AdminEntity;
import com.htec.naves.exception.ForbiddenException;
import com.htec.naves.repository.AdminRepository;
import com.htec.naves.repository.ClientRepository;
import com.htec.naves.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Service containing methods for admin.
 * 
 * @author Jovan Popovic
 * @author Mia Pecelj
 * 
 *
 */
@Service
@Transactional
@Slf4j
@Configuration
public class AdminService {
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private AdminRepository adminRepository;
	@Autowired
	private JwtUtil jwtUtil;

	/**
	 * Returns number of clients that are registered on our system.
	 * 
	 * @param token token containing user email
	 * @return
	 */
	public Long getNumberOfClients(String token) {
		String email = jwtUtil.extractUsername(token);
		Optional<AdminEntity> admin = adminRepository.findByEmail(email);
		if (admin.isEmpty()) {
			throw new ForbiddenException("User is not allowed to access this information. ", email);
		}
		return clientRepository.count();
	}

}
