package com.htec.naves.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.htec.naves.dto.ClientDTO;
import com.htec.naves.dto.ForgotPasswordDTO;
import com.htec.naves.dto.PasswordChangeDTO;
import com.htec.naves.dto.ResetPasswordDTO;
import com.htec.naves.dto.UpdateClientDTO;
import com.htec.naves.entity.ClientEntity;
import com.htec.naves.entity.enums.Provider;
import com.htec.naves.exception.EmailTakenException;
import com.htec.naves.exception.InterserviceCommunicationException;
import com.htec.naves.exception.InvalidTokenException;
import com.htec.naves.exception.PasswordChangeException;
import com.htec.naves.exception.ResourceNotFoundException;
import com.htec.naves.mapper.ClientEntityDTOMapper;
import com.htec.naves.publisher.Publisher;
import com.htec.naves.repository.ClientRepository;
import com.htec.naves.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Service containing methods for client authentication.
 * 
 * @author Jovan Popovic
 * @author Mia Pecelj
 * @author Ognjen Pejcic
 * @author Pavle Jacovic
 * @author Uros Stokovic
 * 
 *
 */
@Service
@Transactional
@Slf4j
@Configuration
public class ClientService {

	private ClientRepository clientRepository;
	private ClientEntityDTOMapper clientMapper;
	private EmailSenderService emailSenderService;
	private PasswordEncoder passwordEncoder;
	private JwtUtil jwtUtil;
	private Publisher publisher;

	@Value("${port}")
	private String port;
	@Value("${host}")
	private String host;
	@Value("${protocol}")
	private String protocol;

	@Autowired
	public ClientService(ClientRepository clientRepository, ClientEntityDTOMapper clientMapper,
			EmailSenderService emailSenderService, PasswordEncoder passwordEncoder, JwtUtil jwtUtil,
			Publisher publisher) {
		super();
		this.clientRepository = clientRepository;
		this.clientMapper = clientMapper;
		this.emailSenderService = emailSenderService;
		this.passwordEncoder = passwordEncoder;
		this.jwtUtil = jwtUtil;
		this.publisher = publisher;
	}

	/**
	 * Attempts to register the user if the email is available and sends the user
	 * verification link via email.
	 * 
	 * @param client a DTO containing client information
	 * @throws EmailTakenException      when user with the same email is already in
	 *                                  the database
	 * @throws IllegalArgumentException when email format is invalid
	 */
	public void register(ClientDTO client) {
		ClientEntity existingUser = clientRepository.findByEmail(client.getEmail());
		if (existingUser != null && existingUser.getEnabled() == true) {
			throw new EmailTakenException("Email is already taken", client.getEmail());
		} else {
			String confirmationToken = jwtUtil.generateTokenFromString(client.getEmail().toLowerCase());
			try {
				emailSenderService.sendConfirmationEmail(client.getEmail(),
						protocol + host + port + "/confirm-account?token=" + confirmationToken);
			} catch (Exception e) {
				throw new IllegalArgumentException("Invalid email format", e);
			}
			if (existingUser == null) {
				client.setEnabled(false);
				client.setPassword(passwordEncoder.encode(client.getPassword()));
				ClientEntity clientEntity = clientMapper.toEntity(client);
				clientEntity.setConfirmationToken(confirmationToken);
				clientRepository.save(clientEntity);
			} else {
				existingUser.setName(client.getName());
				existingUser.setSurname(client.getSurname());
				existingUser.setPassword(passwordEncoder.encode(client.getPassword()));
				existingUser.setEnabled(false);
				existingUser.setSubscribed(client.getSubscribed());
				existingUser.setConfirmationToken(confirmationToken);
				clientRepository.save(existingUser);
			}
		}
	}

	/**
	 * Attempts to confirm the users account if the confirmation token matches.
	 * 
	 * @param confirmationToken user's confirmation token provided to the user via
	 *                          email
	 * @throws InvalidTokenException              when there is no such user for
	 *                                            specified token or if token is
	 *                                            null
	 * @throws InterserviceCommunicationException when service can't connect to
	 *                                            RabbitMQ broker
	 */
	public void confirm(String confirmationToken) {
		if (confirmationToken != null) {
			ClientEntity existingClient = clientRepository.findByConfirmationToken(confirmationToken);
			if (existingClient == null) {
				throw new InvalidTokenException("Invalid verification link");
			}

			jwtUtil.isTokenExpired(confirmationToken);

			existingClient.setEnabled(true);
			existingClient.setConfirmationToken(null);
			existingClient.setProvider(Provider.LOCAL);

			HashMap<String, String> clientData = new HashMap<>();

			clientData.put("email", existingClient.getEmail());
			clientData.put("userId", existingClient.getUserId().toString());
			clientData.put("type", "1");
			clientData.put("subscribed", existingClient.getSubscribed().toString());

			try {
				publisher.sendVerifiedUserData(clientData);
			} catch (IOException e) {
				throw new InterserviceCommunicationException("Unable to establish communication with publisher", e);
			}
			clientRepository.save(existingClient);
			log.info("User " + existingClient.getEmail() + " successfully logged in");
		} else {
			throw new InvalidTokenException("Invalid verification link");
		}
	}

	/**
	 * Sends the email with a password reset token if the user exists
	 * 
	 * @param request DTO containing the user's email
	 * @throws IllegalArgumentException when email does not have valid format
	 */
	public void forgotPassword(ForgotPasswordDTO request) {
		ClientEntity client = clientRepository.findByEmail(request.getEmail());

		if (client != null && client.getProvider() == Provider.LOCAL && client.getEnabled() == true) {
			String email = request.getEmail();
			String token = jwtUtil.generateTokenFromString(email);

			try {
				client.setResetPasswordToken(token);
				clientRepository.save(client);
				String resetPasswordLink = protocol + host + port + "/reset_password?token=" + token;
				sendEmail(email, resetPasswordLink);

			} catch (UnsupportedEncodingException | MessagingException ex) {
				throw new IllegalArgumentException(
						"Invalid email format. Could not reset password for user " + request.getEmail(), ex);
			}
		}
	}

	public void sendEmail(String recipientEmail, String link) throws MessagingException, UnsupportedEncodingException {
		emailSenderService.sendResetPasswordEmail(recipientEmail, link);
	}

	/**
	 * Attempts to change the user's password
	 * 
	 * @param request a DTO containing the password reset token and a new password
	 * @throws InvalidTokenException when there is no such user for specified token
	 */
	public void processResetPassword(ResetPasswordDTO request) {
		String token = request.getPasswordResetToken();
		String password = request.getPassword();

		ClientEntity customer = clientRepository.findByResetPasswordToken(token);

		if (customer == null) {
			throw new InvalidTokenException("Invalid password reset link");
		} else {
			jwtUtil.isTokenExpired(token);
			String encodedPassword = passwordEncoder.encode(password);
			customer.setPassword(encodedPassword);

			customer.setResetPasswordToken(null);
			clientRepository.save(customer);
		}
	}

	/**
	 * Attempts to update user
	 * 
	 * @param client client information for update
	 * @param id     client id
	 */
	public void updateUser(UpdateClientDTO client, String token) {
		ClientEntity existingClient = clientRepository.findByEmail(jwtUtil.extractUsername(token));
		if (existingClient == null) {
			throw new ResourceNotFoundException("User not found");
		}
		existingClient.setName(client.getName());
		existingClient.setSurname(client.getSurname());
		existingClient.setSubscribed(client.isSubscribed());

		clientRepository.save(existingClient);

	}

	/**
	 * Attempts to get user by ID
	 * 
	 * @param id client Id
	 * @return
	 */
	public ClientDTO getUserById(Long id) {
		Optional<ClientEntity> client = clientRepository.findById(id);
		if (client.isEmpty()) {
			throw new ResourceNotFoundException("User not found");
		}
		return clientMapper.toDTO(client.get());

	}

	/**
	 * Attempts to get user by token
	 * 
	 * @param token client token
	 * @return
	 */
	public ClientDTO getUserByToken(String token) {
		String email = jwtUtil.extractUsername(token);
		ClientEntity client = clientRepository.findByEmail(email);
		if (client == null) {
			throw new ResourceNotFoundException("User not found");
		}

		return clientMapper.toDTO(client);
	}

	/**
	 * Attempts to change clients password
	 * 
	 * @param passwordChangeDto dto containing old and new password that user has
	 *                          sent
	 * @param token             token containing user's email
	 */
	public void changePassword(PasswordChangeDTO passwordChangeDto, String token) {
		String email = jwtUtil.extractUsername(token);
		ClientEntity client = clientRepository.findByEmail(email);
		if (client == null) {
			throw new ResourceNotFoundException("User not found.");
		}
		if (client.getProvider().equals(Provider.GOOGLE)) {
			throw new PasswordChangeException("Google Sign-in: Password can not be changed.");
		}
		if (!passwordEncoder.matches(passwordChangeDto.getOldPassword(), client.getPassword())) {
			throw new PasswordChangeException("The old password you have entered is incorrect.");
		}
		if (passwordEncoder.matches(passwordChangeDto.getNewPassword(), client.getPassword())) {
			throw new PasswordChangeException("Your new password must be different from your previous password.");
		}
		client.setPassword(passwordEncoder.encode(passwordChangeDto.getNewPassword()));
		clientRepository.save(client);

	}
}
