package com.htec.naves.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.naves.dto.ClientDTO;
import com.htec.naves.entity.ClientEntity;
import com.htec.naves.entity.UserEntity;
import com.htec.naves.entity.enums.Provider;
import com.htec.naves.mapper.ClientEntityDTOMapper;
import com.htec.naves.publisher.Publisher;
import com.htec.naves.repository.ClientRepository;
import com.htec.naves.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GoogleLoginService {

	@Autowired
	private UserRepository<UserEntity> userRepository;

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private ClientEntityDTOMapper clientMapper;

	@Autowired
	private Publisher publisher;

	public synchronized ClientDTO processOAuthPostLogin(String email, String name, String last_name)
			throws org.springframework.dao.DataIntegrityViolationException {

		synchronized (GoogleLoginService.class) {

			Optional<UserEntity> existingUser = userRepository.findByEmail(email);

			if (existingUser.isEmpty()) {
				ClientDTO cd = new ClientDTO();
				cd.setEmail(email);
				cd.setProvider(Provider.GOOGLE);
				cd.setSubscribed(true);
				cd.setEnabled(true);
				cd.setName(name);
				cd.setSurname(last_name);

				try {
					ClientEntity client = clientRepository.save(clientMapper.toEntity(cd));

					HashMap<String, String> clientData = new HashMap<>();

					clientData.put("email", email);
					clientData.put("userId", client.getUserId().toString());
					clientData.put("type", "1");
					clientData.put("subscribed", client.getSubscribed().toString());
					publisher.sendVerifiedUserData(clientData);
					return clientMapper.toDTO(client);
				} catch (org.springframework.dao.DataIntegrityViolationException e) {
					log.info("User " + email + " already exists in databate but thread tried to add it again");
					return null;
				} catch (IOException e) {
					log.error("User not able to sign in with his google account. RabbitMQ not working!");
					return null;
				}
			} else {
				ClientEntity client;
				if (existingUser.get() instanceof ClientEntity) {
					client = (ClientEntity) existingUser.get();
					if (client.getEnabled() == false) {
						client.setProvider(Provider.GOOGLE);
						client.setEnabled(true);
						client.setConfirmationToken(null);
						clientRepository.save(client);
					}
					return clientMapper.toDTO(client);
				} else {
					log.error("Got AdminEntity instead of ClientEntity at processOAuthPostLogin");
					return null;
				}

			}

		}

	}
}
