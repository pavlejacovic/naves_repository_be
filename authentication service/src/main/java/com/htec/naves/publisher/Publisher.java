package com.htec.naves.publisher;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Class containing method that sends information about verified user to all of
 * the subscriber classes in different projects.
 * 
 * @author Pavle Jacovic
 * 
 */
@Service
public class Publisher {

	@Autowired
	RabbitTemplate rabbitTemplate;

	/**
	 * Sends serializable data to all of the queues using fanout exchange principle
	 * 
	 * @param userData When user confirms his account this data in form of hash map
	 *                 is beeing sent to queues of other services
	 * @throws IOException
	 */
	public void sendVerifiedUserData(HashMap<String, String> userData) throws IOException {

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = new ObjectOutputStream(bos);
		out.writeObject(userData);
		out.flush();
		out.close();

		byte[] byteMessage = bos.toByteArray();
		bos.close();

		Message message = MessageBuilder.withBody(byteMessage).build();

		rabbitTemplate.convertAndSend("Fanout-Exchange", "", message);
	}

}
