package com.htec.naves.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.htec.naves.entity.enums.Provider;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@ToString(callSuper = true, includeFieldNames = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class ClientDTO extends UserDTO {
	private static final long serialVersionUID = 1L;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Provider provider;
	private Boolean subscribed;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Boolean enabled;
}