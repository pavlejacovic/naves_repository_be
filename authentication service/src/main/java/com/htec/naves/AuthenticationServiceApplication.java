package com.htec.naves;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class AuthenticationServiceApplication {

	public static void main(String[] args) {

		try {
			File f = new File("./logs/auth.log");
			if (!f.delete()) {
				log.info("Failed to delete log file");
			} else {
				log.info("Log file deleted successfully");
			}
		} catch (Exception e) {
			log.error("Error while trying to delete log file! " + e.getMessage());
			return;
		}

		SpringApplication.run(AuthenticationServiceApplication.class, args);
	}

}
