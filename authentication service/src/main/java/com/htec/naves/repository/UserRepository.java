package com.htec.naves.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.naves.entity.UserEntity;

@Repository
public interface UserRepository<T extends UserEntity> extends JpaRepository<T, Long> {

	Optional<UserEntity> findByEmail(String email);

}
