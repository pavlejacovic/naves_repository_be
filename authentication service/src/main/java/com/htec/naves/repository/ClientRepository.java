package com.htec.naves.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.naves.entity.ClientEntity;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {
	public ClientEntity findByEmail(String email);

	public ClientEntity findByConfirmationToken(String confirmationToken);

	public ClientEntity findByResetPasswordToken(String token);

}
