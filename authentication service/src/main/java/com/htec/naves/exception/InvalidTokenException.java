package com.htec.naves.exception;

/**
 * Thrown when there is an error while validating token that user has provided.
 * 
 * @author Uros
 *
 */
public class InvalidTokenException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidTokenException(String message) {
		super(message);
	}

}