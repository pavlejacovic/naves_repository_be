package com.htec.naves.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice(basePackages = "com.htec.naves.controller")
public class GlobalExceptionHandler {

	@ExceptionHandler(value = { EmailTakenException.class })
	public ResponseEntity<?> handleEmailTakenException(EmailTakenException ex) {
		log.info("EmailTakenException for user " + ex.getUserEmail() + " : " + ex.getMessage());
		return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());
	}

	@ExceptionHandler(value = { InterserviceCommunicationException.class })
	public ResponseEntity<?> handleInterserviceCommunicationException(InterserviceCommunicationException ex) {
		log.info("InterserviceCommunicationException : " + ex.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
	}

	@ExceptionHandler(value = { IllegalArgumentException.class })
	public ResponseEntity<?> handleIllegalArgumentException(IllegalArgumentException ex) {
		log.info("IllegalArgumentException : " + ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
	}

	@ExceptionHandler(value = { InvalidTokenException.class })
	public ResponseEntity<?> handleVerificationLinkException(InvalidTokenException ex) {
		log.info("InvalidTokenException: ", ex.getMessage());
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
	}

	@ExceptionHandler(value = { ResourceNotFoundException.class })
	public ResponseEntity<?> handleResourceNotFoundkException(ResourceNotFoundException ex) {
		log.info("ResourceNotFoundException: ", ex.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
	}

	@ExceptionHandler(value = { PasswordChangeException.class })
	public ResponseEntity<?> handlePasswordChangeException(PasswordChangeException ex) {
		log.info("PasswordChangeException: ", ex.getMessage());
		return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());
	}

	@ExceptionHandler(value = { ForbiddenException.class })
	public ResponseEntity<?> handleForbiddenException(ForbiddenException ex) {
		log.info("ForbiddenException: ", ex.getMessage());
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(ex.getMessage());
	}
}
