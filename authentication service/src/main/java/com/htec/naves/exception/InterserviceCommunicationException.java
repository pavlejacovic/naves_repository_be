package com.htec.naves.exception;

/**
 * Thrown when the system is unable to establish communication with RabbitMQ
 * broker.
 * 
 * @author Uros
 *
 */
public class InterserviceCommunicationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InterserviceCommunicationException(String message) {
		super(message);
	}

	public InterserviceCommunicationException(String message, Throwable cause) {
		super(message, cause);
	}

}