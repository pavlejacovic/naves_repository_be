package com.htec.naves.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import com.htec.naves.dto.ClientDTO;
import com.htec.naves.entity.ClientEntity;

@Mapper(componentModel = "spring", uses = {
		UserEntityDTOMapper.class }, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ClientEntityDTOMapper {

	ClientDTO toDTO(ClientEntity client);

	ClientEntity toEntity(ClientDTO client);
}
