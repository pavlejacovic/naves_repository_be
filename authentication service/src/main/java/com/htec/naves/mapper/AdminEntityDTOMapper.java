package com.htec.naves.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import com.htec.naves.dto.AdminDTO;
import com.htec.naves.entity.AdminEntity;

@Mapper(componentModel = "spring", uses = {
		UserEntityDTOMapper.class }, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface AdminEntityDTOMapper {

	AdminDTO toDTO(AdminEntity admin);

	AdminEntity toEntity(AdminDTO admin);
}
