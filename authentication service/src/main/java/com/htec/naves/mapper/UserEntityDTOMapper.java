package com.htec.naves.mapper;

import org.mapstruct.Mapper;

import com.htec.naves.dto.UserDTO;
import com.htec.naves.entity.UserEntity;

@Mapper(componentModel = "spring")
public interface UserEntityDTOMapper {

	UserDTO toDTO(UserEntity entity);

	UserEntity toEntity(UserDTO user);

}
