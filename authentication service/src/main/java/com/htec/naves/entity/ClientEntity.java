package com.htec.naves.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.htec.naves.entity.enums.Provider;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Entity
@DiscriminatorValue("1")
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@ToString(callSuper = true, includeFieldNames = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class ClientEntity extends UserEntity {

	@Enumerated(EnumType.STRING)
	private Provider provider;
	private Boolean subscribed;
	private Boolean enabled;

	@Column(name = "reset_password_token")
	private String resetPasswordToken;

	@Column(name = "confirmation_token")
	private String confirmationToken;

}
