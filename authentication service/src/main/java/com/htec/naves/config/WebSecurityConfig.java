package com.htec.naves.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.htec.naves.dto.ClientDTO;
import com.htec.naves.filter.JwtRequestFilter;
import com.htec.naves.google.CustomOAuth2User;
import com.htec.naves.service.CustomOAuth2UserService;
import com.htec.naves.service.GoogleLoginService;
import com.htec.naves.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableWebSecurity
@Slf4j
class WebSecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {
	@Autowired
	private UserDetailsService myUserDetailsService;
	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Autowired
	private JwtUtil jwtTokenUtil;

	@Autowired
	private GoogleLoginService googleService;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(myUserDetailsService);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Autowired
	private CustomOAuth2UserService oauthUserService;

	/*
	 * 
	 * For Google go to http://localhost:8080/auth/oauth2/authorization/google
	 * 
	 * 
	 */

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().authorizeRequests()
				.antMatchers("/authenticate", "/oauth2/**", "/register", "/confirm-account", "/forgot-password",
						"/reset-password", "/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html")
				.permitAll().anyRequest().authenticated().and().formLogin().permitAll().loginProcessingUrl("/login")
				.successHandler((req, res, auth) -> res.setStatus(HttpStatus.NO_CONTENT.value()))
				.failureHandler(new SimpleUrlAuthenticationFailureHandler()).and().exceptionHandling()
				.authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED)).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().oauth2Login().loginPage("/authenticate")
				.userInfoEndpoint().userService(oauthUserService).and()
				.successHandler(new AuthenticationSuccessHandler() {

					@Override
					public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
							Authentication authentication) throws IOException, ServletException {

						CustomOAuth2User oauthUser = (CustomOAuth2User) authentication.getPrincipal();

						if (oauthUser == null) {
							log.error("Google didn't provide prinicipal");
							response.setStatus(HttpStatus.NOT_FOUND.value());
							response.setContentType("application/json");
							response.setCharacterEncoding("UTF-8");
							return;
						}

						ClientDTO client = null;

						String email = (String) oauthUser.getAttributes().get("email");
						String name = (String) oauthUser.getAttributes().get("given_name");
						String last_name = (String) oauthUser.getAttributes().get("family_name");

						try {
							client = googleService.processOAuthPostLogin(email, name, last_name);
						} catch (org.springframework.dao.DataIntegrityViolationException e) {
							log.warn("Multiple google registrations with same email");
							client = googleService.processOAuthPostLogin(email, name, last_name);
						}

						if (client == null) {
							log.error("User not able to sign in with his google account. RabbitMQ not working!");
							response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
							response.setContentType("application/json");
							response.setCharacterEncoding("UTF-8");
							return;
						}

						final String jwt = jwtTokenUtil.generateTokenFromString(client.getEmail());

						response.setStatus(HttpStatus.OK.value());
						response.setContentType("application/json");
						response.setCharacterEncoding("UTF-8");

						response.sendRedirect("http://localhost:3000/oauth?token=" + jwt);

						log.info("User " + oauthUser.getEmail()
								+ " has logged in successfully using his google account. At onAuthenticationSuccess");

					}
				});

		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

	}

}