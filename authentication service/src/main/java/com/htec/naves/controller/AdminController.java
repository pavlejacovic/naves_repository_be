package com.htec.naves.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.service.AdminService;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller for admin dashboard
 * 
 * 
 * @author Jovan Popovic
 * @author Mia Pecelj
 *
 */
@RestController
@Slf4j
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private AdminService adminService;

	/**
	 * Returns number of clients.
	 * 
	 * @param token admin token
	 * @return
	 */
	@GetMapping
	public ResponseEntity<Object> getNumberOfClients(@RequestHeader(name = "Authorization") String token) {
		Long numberOfUsers = adminService.getNumberOfClients(token);
		return ResponseEntity.status(HttpStatus.OK).body(numberOfUsers);

	}

}
