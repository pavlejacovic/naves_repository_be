package com.htec.naves.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.dto.AuthenticationRequestDTO;
import com.htec.naves.dto.AuthenticationResponseDTO;
import com.htec.naves.dto.RoleDTO;
import com.htec.naves.service.MyUserDetailsService;
import com.htec.naves.util.JwtUtil;
import com.htec.naves.util.MyUser;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller for user login API calls
 * 
 * @author Ivan Kukrkic
 *
 */
@RestController
@Slf4j
public class UserRestController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtTokenUtil;

	@Autowired
	private MyUserDetailsService userDetailsService;

	/**
	 * Attempts to log in the user and returns the authentication token and user
	 * type if successful
	 * 
	 * @param authenticationRequest a DTO containing user's email and password
	 * @return HTTP response with a status and DTO with a token which will be used
	 *         for authorization and user's type (client or admin) if successful
	 */
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequestDTO authenticationRequest) {

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(),
					authenticationRequest.getPassword()));
		} catch (Exception e) {
			log.info("Incorrect email or password in POST request /authenticate");
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Incorrect email or password");

		}
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());

		final String jwt = jwtTokenUtil.generateToken(userDetails);

		log.info("User " + userDetails.getUsername() + " has successfully logged in. At POST request /authenticate");
		return ResponseEntity.ok(new AuthenticationResponseDTO(jwt, ((MyUser) userDetails).getType()));
	}

	@RequestMapping(value = "/role", method = RequestMethod.GET)
	public ResponseEntity<?> getRole(@RequestHeader(name = "Authorization") String token) {
		String email = jwtTokenUtil.extractUsername(token);

		final UserDetails userDetails = userDetailsService.loadUserByUsername(email);

		return ResponseEntity.ok(new RoleDTO(((MyUser) userDetails).getType()));
	}

}
