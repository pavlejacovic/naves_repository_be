package com.htec.naves.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.htec.naves.dto.ClientDTO;
import com.htec.naves.dto.ForgotPasswordDTO;
import com.htec.naves.dto.PasswordChangeDTO;
import com.htec.naves.dto.ResetPasswordDTO;
import com.htec.naves.dto.TokenDTO;
import com.htec.naves.dto.UpdateClientDTO;
import com.htec.naves.service.ClientService;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller for user registration and password changing API calls
 * 
 * 
 * @author Jovan Popovic
 * @author Mia Pecelj
 * @author Ognjen Pejcic
 * @author Pavle Jacovic
 * @author Uros Stokovic
 *
 */
@RestController
@Slf4j
public class ClientRestController {
	@Autowired
	private ClientService clientService;

	/**
	 * Attempts to register the user if the email is available and sends the user
	 * verification link via email.
	 * 
	 * @param client        a DTO containing user information
	 * @param bindingResult format validation
	 * @return HTTP response with a status and a message for the client
	 */
	@PostMapping(value = "/register")
	public ResponseEntity<Object> registerUser(@RequestBody @Valid ClientDTO client, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			log.info("Validation error for user " + client.getEmail() + " in POST request /register");
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach((error) -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error registering client " + errors);
		} else {
			clientService.register(client);
			log.info("Verification mail was sent for user " + client.getEmail());
			return ResponseEntity.status(HttpStatus.OK).body("Verification mail was sent");
		}
	}

	/**
	 * Attempts to confirm the users account if the confirmation token matches.
	 * 
	 * @param confirmationToken user's confirmation token provided to the user via
	 *                          email
	 * @return HTTP response with a status and a message for the client
	 */
	@RequestMapping(value = "/confirm-account", method = { RequestMethod.POST })
	public ResponseEntity<Object> confirmUserAccount(@RequestBody TokenDTO confirmationToken) {
		clientService.confirm(confirmationToken.getToken());
		return ResponseEntity.status(HttpStatus.OK).body("You have successfully verified your email");
	}

	/**
	 * Sends the email with a password reset token if the user exists
	 * 
	 * @param request a DTO containing the user's email
	 * @return HTTP response with a status and a message for the client
	 */
	@PostMapping("/forgot-password")
	public ResponseEntity<Object> processForgotPassword(@RequestBody @Valid ForgotPasswordDTO request,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			log.info("Validation error in POST request /forgot-password");
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach((error) -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error in forgot password: " + errors);
		} else {
			clientService.forgotPassword(request);
			log.info("Password reset email was sent in POST request /forgot-password for user " + request.getEmail());
			return ResponseEntity.status(HttpStatus.OK).body("Check your email for a link to reset your password.");
		}

	}

	/**
	 * Attempts to change the user's password
	 * 
	 * @param request       a DTO containing the password reset token and a new
	 *                      password
	 * @param bindingResult format validation
	 * @return HTTP response with a status and a message for the client
	 */
	@PostMapping("/reset-password")
	public ResponseEntity<Object> processResetPassword(@RequestBody @Valid ResetPasswordDTO request,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			log.info("Validation error in POST request /reset-password");
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach((error) -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error reseting password " + errors);
		} else {
			clientService.processResetPassword(request);
			log.info("Client successfully changed his password in POST request /reset-password");
			return ResponseEntity.status(HttpStatus.OK).body("You have successfully changed your password.");
		}
	}

	/**
	 * Attempts to update user's information
	 * 
	 * @param id            client id
	 * @param client        client information for update
	 * @param bindingResult
	 * @return
	 */
	@PutMapping("/profile")
	public ResponseEntity<Object> updateUser(@RequestHeader(name = "Authorization") String token,
			@RequestBody @Valid UpdateClientDTO client, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			log.info("Validation error for user in PUT request /update");
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach((error) -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error updating client " + errors);
		} else {
			clientService.updateUser(client, token);
			log.info("Successfully updated profile");
			return ResponseEntity.status(HttpStatus.OK).body("Successfully updated profile.");
		}
	}

	/**
	 * Gets user information by email.
	 * 
	 * @param token token containing user's email
	 * @return
	 */
	@GetMapping("/profile")
	public ResponseEntity<Object> getUser(@RequestHeader(name = "Authorization") String token) {
		return ResponseEntity.status(HttpStatus.OK).body(clientService.getUserByToken(token));
	}

	/**
	 * Attempts to change clients password
	 * 
	 * @param token             token containing user's email
	 * @param passwordChangeDto dto containing old and new password that client has
	 *                          sent
	 * @param bindingResult
	 * @return
	 */
	@PutMapping("/change-password")
	public ResponseEntity<Object> changePassword(@RequestHeader(name = "Authorization") String token,
			@RequestBody @Valid PasswordChangeDTO passwordChangeDto, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			log.info("Validation error for user in PUT request /change password");
			Map<String, String> errors = new HashMap<>();
			bindingResult.getAllErrors().forEach((error) -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error changing password " + errors);
		} else {
			clientService.changePassword(passwordChangeDto, token);
			log.info("Successfully changed password");
			return ResponseEntity.status(HttpStatus.OK).body("Successfully changed password.");
		}
	}
}
