﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace NavesLib
{
    /// <summary>
    /// Reads input parameters from input.json and other files provided when the function is called
    /// </summary>
    public class NavesReader
    {
        private JObject inputJson;

        public NavesReader()
        {
            string jsonAsString = File.ReadAllText(@"myfolder/Input/input.json");
            inputJson = (JObject)JsonConvert.DeserializeObject(jsonAsString);
        }
        /// <summary>
        /// Returns the provided body input as an instance of a provided class
        /// </summary>
        /// <typeparam name="T">
        /// Type as which the json from body is parsed
        /// </typeparam>
        /// <param name="type">
        /// type the class of T
        /// </param>
        /// <returns>
        /// an object of the specified type deserialized from the body json
        /// </returns>
        public T GetBody<T>(Type type)
        {
            JToken requestBody = inputJson["requestBody"];
            return (T)JsonConvert.DeserializeObject(requestBody.ToString(), type);
        }
        /// <summary>
        /// Returns a map of request parameters
        /// </summary>
        /// <returns>
        /// Dictionary containing a pair of name and value for all the parameters that have been passed to user function
        /// </returns>
        public Dictionary<String, Object> getParams()
        {
            JObject requestParams = inputJson["requestParams"].Value<JObject>(); ;
            return requestParams.ToObject<Dictionary<string, object>>();
        }
        /// <summary>
        /// Returns a value of a specific request parameter
        /// </summary>
        /// <param name="param">
        /// Name of requested parameter
        /// </param>
        /// <returns>
        /// Value of specified parameter
        /// </returns>
        public String getParam(String param)
        {
            return inputJson["requestParams"][param].ToString();
        }
        /// <summary>
        /// Returns a file representation of the requested body parameter
        /// </summary>
        /// <param name="file">
        /// file name of field in the input body representing a file
        /// </param>
        /// <returns>
        /// FileStream for the file with specified name
        /// </returns>
        public FileStream getFile(String file)
        {
            return new FileStream("myfolder/Input/" + inputJson["requestBody"][file], FileMode.Open, FileAccess.Read);
        }
    }
}
