﻿using Newtonsoft.Json.Linq;
using System.IO;

namespace NavesLib
{
    /// <summary>
    /// Creates an output.json file with headers, status and body which will be returned as a response when this function is called
    /// </summary>
    public class NavesWriter
    {
        private JObject headersJson;
        private JObject bodyJson;
        private JObject statusJson;

        public NavesWriter()
        {
            headersJson = new JObject();
            bodyJson = new JObject();
            statusJson = new JObject();
            statusJson.Add("code", 200);
        }
        /// <summary>
        /// Sets a response header with the given name and value
        /// </summary>
        /// <param name="header">
        /// the name of the header
        /// </param>
        /// <param name="value">
        /// the new value of the header
        /// </param>
        /// <returns>
        /// true if the operation was successful
        /// </returns>
        public bool setHeader(string header, string value)
        {
            headersJson[header] = value;
            return true;
        }

        /// <summary>
        /// Sets the response status to a given code
        /// </summary>
        /// <param name="status">
        /// code the status is being set to
        /// </param>
        /// <returns>
        /// true if the operation was successful
        /// </returns>
        public bool setStatus(int status)
        {
            statusJson["code"] = status;
            return true;
        }
        /// <summary>
        /// Sets the response body content to a given string
        /// </summary>
        /// <param name="content">
        /// Content of the body as string
        /// </param>
        /// <returns>
        /// true if the operation was successful
        /// </returns>
        public bool setBody(string content)
        {
            bodyJson["content"] = content;
            bodyJson["type"] = "string";
            return true;
        }
        /// <summary>
        /// Sets the response body to be a given file
        /// </summary>
        /// <param name="file">
        /// file being sent in the body
        /// </param>
        /// <returns>
        /// true if the operation was successful
        /// </returns>
        public bool setBodyFile(FileStream file)
        {
            bodyJson["content"] = Path.GetFileName(file.Name);
            bodyJson["type"] = "file";
            return true;
        }
        /// <summary>
        /// Returns the path to the current output folder
        /// </summary>
        /// <returns>
        /// string represeting the output folder path
        /// </returns>
        public string getOutput()
        {
            return "myfolder/Output";
        }
        /// <summary>
        /// Creates an output.json file which will be returned as a response when the function is called.Must be called before the end of the execution if a response is being returned
        /// </summary>
        /// <returns>
        /// true if the operation was successful
        /// </returns>
        public bool writeOutput()
        {
            JObject outputJson = new JObject();
            outputJson.Add("headers", headersJson);
            outputJson.Add("body", bodyJson);
            outputJson.Add("status", statusJson);

            File.WriteAllText(@"myfolder/Output/output.json", outputJson.ToString().Replace("\\/", "/"));
            return true;
        }

    }
}
