﻿using System.Diagnostics;

namespace NavesLib
{
    /// <summary>
    /// Allows users to easily execute specified command along with arguments that are provided
    /// </summary>
    public class NavesConsole
    {
        /// <summary>
        /// Executes user command
        /// </summary>
        /// <param name="Command">
        /// Command that user wants to execute
        /// </param>
        /// <param name="Argument">
        /// Arguments that will be passed to command
        /// </param>
        public static void ExecuteCommand(string Command, string Argument)
        {
            ProcessStartInfo ProcessInfo;
            Process Process;

            ProcessInfo = new ProcessStartInfo(Command, Argument)
            {
                CreateNoWindow = true,
                UseShellExecute = true
            };

            Process = Process.Start(ProcessInfo);
            Process.WaitForExit();
        }
    }
}
