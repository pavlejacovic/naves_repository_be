package com.htec.naves.gateway;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallBackMethodController {

	@RequestMapping(value = "/fall-back", method = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
			RequestMethod.DELETE })
	public ResponseEntity<String> authServiceFallBackMethod() {
		return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
				.body("Authentication service is taking longer than expected." + " Please try again later.");
	}

	@RequestMapping(value = "/fall-back-box", method = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
			RequestMethod.DELETE })
	public ResponseEntity<String> boxServiceFallBackMethod() {
		return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
				.body("Box service is taking longer than expected." + " Please try again later.");
	}

	@RequestMapping(value = "/fall-back-lambda", method = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
			RequestMethod.DELETE })
	public ResponseEntity<String> lambdaServiceFallBackMethod() {
		return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
				.body("Lambda service is taking longer than expected." + " Please try again later.");
	}
}
