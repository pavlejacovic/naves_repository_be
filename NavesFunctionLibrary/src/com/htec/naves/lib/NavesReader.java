package com.htec.naves.lib;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;

import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

/**
 * Reads input parameters from input.json and other files provided when the
 * function is called.
 * 
 * @author Ivan Kukrkic
 *
 */
public class NavesReader {

	private JSONObject inputJson;

	public NavesReader() throws ParseException, FileNotFoundException {
		File file = new File("myfolder/Input/input.json");
		FileReader fr = new FileReader(file);
		JSONParser parser = new JSONParser(fr);
		inputJson = new JSONObject(parser.parse().toString());

	}

	/**
	 * Returns the provided body input as an instance of a provided class.
	 * 
	 * @param <T>  Type as which the json from body is parsed
	 * @param type the class of T
	 * @return an object of the specified class deserialized from the body json
	 * @throws JSONException
	 */
	public <T> T getBody(Class<T> type) {
		Gson gson = new Gson();
		return gson.fromJson(inputJson.getString("requestBody"), type);
	}

	/**
	 * Returns a map of request parameters
	 * 
	 * @throws JSONException
	 */
	public Map<String, Object> getParams() {
		return inputJson.getJSONObject("requestParams").toMap();
	}

	/**
	 * Returns a value of a specific request parameter
	 * 
	 * @param param name of the request parameter
	 * @throws JSONException
	 */
	public String getParam(String param) {
		return inputJson.getJSONObject("requestParams").getString(param);
	}

	/**
	 * Returns a file representation of the requested body parameter
	 * 
	 * @param file name of field in the input body representing a file
	 */
	public File getFile(String file) {
		return new File("myfolder/Input/" + inputJson.getJSONObject("requestBody").getString(file));
	}

}
