package com.htec.naves.lib;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Creates an output.json file with headers, status and body which will be
 * returned as a response when this function is called.
 * 
 * @author Ivan Kukrkic
 *
 */
public class NavesWriter {

	private JSONObject headersJson;
	private JSONObject bodyJson;
	private JSONObject statusJson;

	public NavesWriter() {
		headersJson = new JSONObject();
		bodyJson = new JSONObject();
		statusJson = new JSONObject();
		statusJson.put("code", 200);
	}

	/**
	 * Sets a response header with the given name and value
	 * 
	 * @param header the name of the header
	 * @param value  the new value of the header
	 * @return true if the operation was a success
	 * @throws JSONException
	 */
	public boolean setHeader(String header, String value) {
		headersJson.put(header, value);
		return true;
	}

	/**
	 * Sets the response status to a given code
	 * 
	 * @param status code the status is being set to
	 * @return true if the operation was a success
	 * @throws JSONException
	 */
	public boolean setStatus(int status) {
		statusJson.put("code", status);
		return true;
	}

	/**
	 * Sets the response body content to a given string
	 * 
	 * @param content
	 * @return true if the operation was a success
	 * @throws JSONException
	 */
	public boolean setBody(String content) {
		bodyJson.put("content", content);
		bodyJson.put("type", "string");
		return true;
	}

	/**
	 * Sets the response body to be a given file
	 * 
	 * @param file file being sent in the body
	 * @return true if the operation was a success
	 * @throws JSONException
	 */
	public boolean setBodyFile(File file) {
		bodyJson.put("content", file.getName());
		bodyJson.put("type", "file");
		return true;
	}

	/**
	 * Returns the path to the current output folder.
	 * 
	 * @return string represeting the output folder path
	 */
	public String getOutput() {
		return "myfolder/Output";
	}

	/**
	 * Creates an output.json file which will be returned as a response when the
	 * function is called. Must be called before the end of the execution if a
	 * response is being returned.
	 * 
	 * @return true if the operation was a success
	 * @throws IOException
	 * @throws JSONException
	 */
	public boolean writeOutput() throws IOException {
		JSONObject outputJson = new JSONObject();
		outputJson.put("headers", headersJson);
		outputJson.put("body", bodyJson);
		outputJson.put("status", statusJson);
		File file = new File("myfolder/Output/output.json");
		Writer output = new BufferedWriter(new FileWriter(file));
		output.write(outputJson.toString().replace("\\/", "/"));
		output.close();
		return true;
	}
}
